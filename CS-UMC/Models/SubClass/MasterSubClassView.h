//
//  MasterSubClassView.h
//  CS-UMC
//
//  Created by SCIT on 7/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterSubClassView : UIView

@property (nonatomic, strong) MasterSubClassView *customView;

@end
