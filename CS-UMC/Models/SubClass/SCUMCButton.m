//
//  SCUMCButton.m
//  CS-UMC
//
//  Created by Lakshitha on 8/6/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "SCUMCButton.h"

@implementation SCUMCButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
      [self buttonStyles];
    }
    return self;
}

//added custum properities to button
-(id)initWithCoder:(NSCoder *)aDecoder
{
//    NSLog(@"initWithCoder");
    self = [super initWithCoder: aDecoder];
    if (self) {
        // Initialization code
        [self buttonStyles];
    }
    return self;
}
-(void)buttonStyles{
    self.layer.cornerRadius = 3.0f;
    self.layer.masksToBounds = true;
}
//selected method of uibutton
-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if(selected)
    {
        
    }
    else{
        
    }
}
@end
