//
//  MasterSubClassView.m
//  CS-UMC
//
//  Created by SCIT on 7/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "MasterSubClassView.h"

@implementation MasterSubClassView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSString *className = NSStringFromClass([self class]);
        _customView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        _customView.frame = self.bounds;
        if(CGRectIsEmpty(frame)) {
            self.bounds = _customView.bounds;
        }
        [self addSubview:_customView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        NSString *className = NSStringFromClass([self class]);
        _customView = [[[NSBundle mainBundle] loadNibNamed:className owner:self options:nil] firstObject];
        _customView.frame = self.bounds;
        [self addSubview:_customView];
    }
    return self;
}

@end
