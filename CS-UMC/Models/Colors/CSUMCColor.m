//
//  CSUMCColor.m
//  CS-UMC
//
//  Created by SCIT on 7/26/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "CSUMCColor.h"

@implementation CSUMCColor

+ (UIColor *)mySensorColor {//Red
    return [UIColor colorWithRed:1.00 green:0.00 blue:0.00 alpha:1.0];
}
+ (UIColor *)stationOneSensorColor {//Blue
    return [UIColor colorWithRed:0.38 green:0.81 blue:0.96 alpha:1.0];
}
+ (UIColor *)stationTwoSensorColor {//Yello
    return [UIColor colorWithRed:1.00 green:0.97 blue:0.00 alpha:1.0];
}
+ (NSArray *)getColor_firstRange{
    int firstIndex = 0;
    return [self getColorArray:firstIndex];;
}
+ (NSArray *)getColor_SecondRange{
    int firstIndex = 1;
    return [self getColorArray:firstIndex];
}
+ (NSArray *)getColor_ThiredRange{
    int firstIndex = 2;
    return [self getColorArray:firstIndex];
}
+ (NSArray *)getColor_FourthRange{
    int firstIndex = 3;
    return [self getColorArray:firstIndex];
}
+ (NSArray *)getColor_FifthRange{
    int firstIndex = 4;
    return [self getColorArray:firstIndex];
}
+ (NSArray *)getColor_SixthRange{
    int firstIndex = 5;
    return [self getColorArray:firstIndex];
}
+ (NSArray *)getColorArray :(int)colorSchema {
    switch (colorSchema) {
        case 0:
            return [[NSArray alloc]initWithObjects:[UIColor colorWithRed:0.55 green:0.87 blue:1.00 alpha:1.0],  //low
                    [UIColor colorWithRed:0.42 green:0.81 blue:0.96 alpha:1.0],
                    [UIColor colorWithRed:0.28 green:0.74 blue:0.92 alpha:1.0],
                    [UIColor colorWithRed:0.00 green:0.60 blue:0.84 alpha:1.0],
                    [UIColor colorWithRed:0.00 green:0.60 blue:0.84 alpha:1.0], nil];//high
            break;
        case 1:
            return [[NSArray alloc]initWithObjects:[UIColor colorWithRed:0.55 green:0.87 blue:1.00 alpha:1.0],
                    [UIColor colorWithRed:0.55 green:0.80 blue:0.84 alpha:1.0],
                    [UIColor colorWithRed:0.60 green:0.71 blue:0.51 alpha:1.0],
                    [UIColor colorWithRed:0.64 green:0.76 blue:0.54 alpha:1.0],
                    [UIColor colorWithRed:0.53 green:0.62 blue:0.46 alpha:1.0], nil];//Green
            break;
        case 2:
            return [[NSArray alloc]initWithObjects:[UIColor colorWithRed:0.89 green:0.89 blue:0.52 alpha:1.0],
                    [UIColor colorWithRed:0.99 green:0.95 blue:0.51 alpha:1.0],
                    [UIColor colorWithRed:0.99 green:0.95 blue:0.51 alpha:1.0],
                    [UIColor colorWithRed:1.00 green:0.89 blue:0.38 alpha:1.0],
                    [UIColor colorWithRed:1.00 green:0.85 blue:0.32 alpha:1.0], nil];//Yellow
            break;
        case 3:
            return [[NSArray alloc]initWithObjects:[UIColor colorWithRed:1.00 green:0.85 blue:0.32 alpha:1.0],
                    [UIColor colorWithRed:1.00 green:0.81 blue:0.24 alpha:1.0],
                    [UIColor colorWithRed:0.99 green:0.74 blue:0.11 alpha:1.0],
                    [UIColor colorWithRed:1.00 green:0.47 blue:0.09 alpha:1.0],
                    [UIColor colorWithRed:1.00 green:0.47 blue:0.09 alpha:1.0], nil]; //Orange
            break;
        case 4:
            return [[NSArray alloc]initWithObjects:[UIColor colorWithRed:1.00 green:0.47 blue:0.09 alpha:1.0],
                    [UIColor colorWithRed:0.97 green:0.44 blue:0.13 alpha:1.0],
                    [UIColor colorWithRed:0.97 green:0.44 blue:0.13 alpha:1.0],
                    [UIColor colorWithRed:0.79 green:0.13 blue:0.14 alpha:1.0],
                    [UIColor colorWithRed:0.79 green:0.13 blue:0.14 alpha:1.0], nil]; //Dam
            break;
        case 5:
            return [[NSArray alloc]initWithObjects:[UIColor colorWithRed:1.00 green:0.00 blue:0.18 alpha:1.0],
                    [UIColor colorWithRed:1.00 green:0.00 blue:0.18 alpha:1.0],
                    [UIColor colorWithRed:1.00 green:0.33 blue:0.15 alpha:1.0],
                    [UIColor colorWithRed:0.89 green:0.23 blue:0.15 alpha:1.0],
                    [UIColor colorWithRed:0.79 green:0.13 blue:0.14 alpha:1.0], nil]; //Red
            break;
            
        default:
            break;
    }
    
    
    return nil;
}
@end
