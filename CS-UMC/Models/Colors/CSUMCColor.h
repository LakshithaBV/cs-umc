;//
//  CSUMCColor.h
//  CS-UMC
//
//  Created by SCIT on 7/26/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUserDefaults.h"

@interface CSUMCColor : UIColor

+ (UIColor *)mySensorColor ; //Current connected sensor graph line color
+ (UIColor *)stationOneSensorColor ; //Station one connected sensor graph line color
+ (UIColor *)stationTwoSensorColor ; //Station two connected sensor graph line color

+ (NSArray *)getColor_firstRange;
+ (NSArray *)getColor_SecondRange;
+ (NSArray *)getColor_ThiredRange;
+ (NSArray *)getColor_FourthRange;
+ (NSArray *)getColor_FifthRange;
+ (NSArray *)getColor_SixthRange;
@end
