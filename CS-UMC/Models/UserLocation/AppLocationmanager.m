//
//  AppLocationmanager.m
//  CS-UMC
//
//  Created by SCIT on 1/11/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "AppLocationmanager.h"

@implementation AppLocationmanager
+ (AppLocationmanager *)sharedInstance
{
    static AppLocationmanager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AppLocationmanager alloc] init];
    });
    return sharedInstance;
}
-(void)startUpdateLocation {
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    CLLocation *currentLocation = newLocation;
    if (currentLocation != nil) {
//        NSLog(@"Lat %f Long %f",currentLocation.coordinate.latitude,currentLocation.coordinate.longitude);
        [self.delegate didSelectLocation:currentLocation];
    }
}

@end
