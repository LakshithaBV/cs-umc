//
//  AppLocationmanager.h
//  CS-UMC
//
//  Created by SCIT on 1/11/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@protocol LocationManagerDelegate <NSObject>

-(void) didSelectLocation :(CLLocation *)pickedLocation;

@end

@interface AppLocationmanager : NSObject <CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
}


+ (AppLocationmanager *)sharedInstance;
-(void)startUpdateLocation ;
@property id<LocationManagerDelegate> delegate;

@end
