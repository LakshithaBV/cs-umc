//
//  ListCell.h
//  HHDropDownList
//
//  Occupancy Evalution
//
//  Created by SCIT on 2/21/18.
//  Copyright © 2018 SCIT. All rights reserved.
//
//

#import <UIKit/UIKit.h>

@interface ListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *centerLabel;

@end
