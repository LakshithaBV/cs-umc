//
//  HHDropDownList.h
//  HHDropDownList
//
//  Occupancy Evalution
//
//  Created by SCIT on 2/21/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HHDropDownList;

@protocol HHDropDownListDataSource <NSObject>

@required
- (NSArray *)listDataForDropDownList:(HHDropDownList *)dropDownList;

@optional

@end


@protocol HHDropDownListDelegate <NSObject>

@optional
- (void)dropDownList:(HHDropDownList *)dropDownList didSelectItemName:(NSString *)itemName atIndex:(NSInteger)index;

@end


@interface HHDropDownList : UIView <UITableViewDataSource, UITableViewDelegate>

@property (assign, nonatomic) BOOL haveBorderLine;

@property (strong, nonatomic) UIColor *highlightColor;

@property (strong, nonatomic) NSString *topicString;

@property (assign, nonatomic) BOOL isExclusive;        

@property (weak, nonatomic) id<HHDropDownListDelegate> delegate;

@property (weak, nonatomic) id<HHDropDownListDataSource> dataSource;

- (void)reloadListData;

- (void)dropDown;   

- (void)pullBack;

@end

@interface CALayer (HHAddAnimationAndValue)

- (void)addAnimation:(CAAnimation *)anim andValue:(NSValue *)value forKeyPath:(NSString *)keyPath;

@end
