//
//  ChatManager.h
//  CS-UMC
//
//  Created by SCIT on 6/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AAChartKit.h"
#import "DBManager.h"
#import "LMLineGraphView.h"
#import "Constants.h"
#import "CSUMCColor.h"
#import "ChartDataProcess.h"

@interface ChatManager : NSObject <AAChartViewDidFinishLoadDelegate,LMLineGraphViewDelegate>{
    AAChartView * temperatureChartView;
    AAChartView * tAHChartView;
    AAChartModel *aaChartModel;
    DBManager * dbManager;
    ChartDataProcess * chartProcess;
    
    //Main ChartView
    CGRect frameView;
    LMLineGraphView *graphView;
    
    CGFloat maxValue;
    CGFloat minValue;
    
    NSString * symbol;
    
    NSArray * xAxisCordinates;
    NSArray * sortedObjects;
    NSMutableArray * timeLineArray;
}
+ (instancetype)sharedInstance;
-(AAChartView *)getTemperatureChart ;
-(AAChartView *)addTemperatureChart;
-(AAChartModel *)updateTemperatureChartWithData ;
-(AAChartView *)getHymidityChart ;
-(AAChartModel *)updateHymidityChartWithData ;

- (LMLineGraphView *)getTotalChart :(float)zoomScale Type:(NSString *)parameters Line1:(NSArray *)line1 Line2:(NSArray *)line2 MySensor:(NSArray *)line3;

@property(nonatomic,retain)NSNumber * temperatureDataCount;

@end
