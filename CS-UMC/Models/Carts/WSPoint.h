//
//  WSPoint.h
//  CS-UMC
//
//  Created by SCIT on 8/20/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WSPoint : NSObject

@property (nonatomic,retain)NSNumber * tempreture;
@property (nonatomic,retain)NSNumber * humidity;
@property (nonatomic,retain)NSDate * dateTime;
@property (nonatomic,retain)NSString * wStation;
@property (nonatomic,retain)NSString * dateString;

-(WSPoint *)initWithDataDictionary :(NSDictionary *)dataDic Station:(NSString *)station;
@end
