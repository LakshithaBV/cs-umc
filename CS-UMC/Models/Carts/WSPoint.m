//
//  WSPoint.m
//  CS-UMC
//
//  Created by SCIT on 8/20/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "WSPoint.h"

@implementation WSPoint

@synthesize tempreture;
@synthesize humidity;
@synthesize wStation;
@synthesize dateTime;
@synthesize dateString;

-(WSPoint *)initWithDataDictionary :(NSDictionary *)dataDic Station:(NSString *)station{
    if(self = [super init]){
        tempreture = [NSNumber numberWithFloat:[[dataDic objectForKey:@"air_temperature"] floatValue]];
        humidity = [NSNumber numberWithFloat:[[dataDic objectForKey:@"rel_humidity"] floatValue]];
        wStation = station;
        dateString = [dataDic objectForKey:@"time_local"];
        dateTime = [self stringToDate:[dataDic objectForKey:@"time_local"]];
    }
    return self;
}
-(NSDate *)stringToDate :(NSString *)stringDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat : @"YYYY-MM-dd HH:mm:ss"];
    NSDate *dateTime = [formatter dateFromString:stringDate];
    return dateTime;
}
@end
