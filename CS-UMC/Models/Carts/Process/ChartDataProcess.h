//
//  ChartDataProcess.h
//  CS-UMC
//
//  Created by SCIT on 8/21/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSPoint.h"
@interface ChartDataProcess : NSObject

+ (instancetype)sharedInstance;
-(NSArray *)processStationDataForChart :(NSArray *)station1 Station2:(NSArray *)station2 DeviceData:(NSArray *)deviceData;

-(NSArray *)getTimeLine :(NSArray *)sortedArray;
-(NSArray *)getTimeLineObjects :(NSArray *)sortedArray;

@property (nonatomic,retain)NSMutableArray * stationOneLine;
@property (nonatomic,retain)NSMutableArray * stationTwoLine;
@property (nonatomic,retain)NSMutableArray * deviceDataLine;
@end
