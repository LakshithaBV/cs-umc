//
//  ChartDataProcess.m
//  CS-UMC
//
//  Created by SCIT on 8/21/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "ChartDataProcess.h"

@implementation ChartDataProcess
@synthesize stationOneLine,stationTwoLine,deviceDataLine;
+ (instancetype)sharedInstance
{
    static ChartDataProcess *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ChartDataProcess alloc] init];
    });
    return sharedInstance;
}
-(NSArray *)processStationDataForChart :(NSArray *)station1 Station2:(NSArray *)station2 DeviceData:(NSArray *)deviceData{
    [self cleanMyLines];
 
    NSMutableArray * datesArray = [[NSMutableArray alloc]init];
    [datesArray addObjectsFromArray:station1];
    [datesArray addObjectsFromArray:station2];
    [datesArray addObjectsFromArray:deviceData];
    
    NSArray *arrKeys = [datesArray sortedArrayUsingComparator:^NSComparisonResult(WSPoint * obj1, WSPoint * obj2) {
        NSDate *d1 = obj1.dateTime;
        NSDate *d2 = obj2.dateTime;
        return [d1 compare: d2];
    }];
    
    return arrKeys;
}
-(NSArray *)getTimeLine :(NSArray *)sortedArray{
    NSMutableArray * dateTimeLine = [[NSMutableArray alloc]init];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    for (WSPoint * point in sortedArray) {
        NSString * dateTimeString = point.dateString; //[df stringFromDate:point.dateTime];
        if(![dateTimeLine containsObject:dateTimeString]){
            [dateTimeLine addObject:dateTimeString];
        }
    }
    return dateTimeLine;
}
-(NSArray *)getTimeLineObjects :(NSArray *)sortedArray{
    NSMutableArray * dateTimeLineObjects = [[NSMutableArray alloc]init];
    for (WSPoint * point in sortedArray) {
        if(![dateTimeLineObjects containsObject:point.dateTime]){
            [dateTimeLineObjects addObject:point];
        }
    }
    return dateTimeLineObjects;
}
-(BOOL)compareStartDate :(NSDate *)dateOne isLessThan :(NSDate *)dateTwo{
    
   NSComparisonResult result = [dateOne compare:dateTwo]; // comparing two dates
    
    if(result == NSOrderedAscending){
        return true;
    }else if(result==NSOrderedDescending){
        return false;
    }else{
        return false;
    }
}
-(BOOL)compareEndDate :(NSDate *)dateOne isLessThan :(NSDate *)dateTwo{
    
    NSComparisonResult result = [dateOne compare:dateTwo]; // comparing two dates
    
    if(result == NSOrderedAscending){
        return false;
    }else if(result==NSOrderedDescending){
        return true;
    }else{
        return false;
    }
}
-(void)cleanMyLines{
    if (!stationOneLine)stationOneLine = [[NSMutableArray alloc]init];
    [stationOneLine removeAllObjects];
    
    if (!stationTwoLine)stationTwoLine = [[NSMutableArray alloc]init];
    [stationTwoLine removeAllObjects];
    
    if (!deviceDataLine)deviceDataLine = [[NSMutableArray alloc]init];
    [deviceDataLine removeAllObjects];
}
-(NSString *)dateToString:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    return stringFromDate;
}
@end
