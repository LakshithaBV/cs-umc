//
//  ChatManager.m
//  CS-UMC
//
//  Created by SCIT on 6/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "ChatManager.h"
#import "AAChartKit.h"
#import "AAChartView.h"
#import "Temperature+CoreDataClass.h"
#import "AppUserDefaults.h"

@implementation ChatManager

+ (instancetype)sharedInstance
{
    static ChatManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ChatManager alloc] init];
        // Do any other initialisation stuff here
    });
    return sharedInstance;
}

#pragma mark - Tempreture Charts
-(NSArray *)getTempereture {
    NSMutableArray * tempretureArray;
    if (!dbManager)dbManager = [[DBManager alloc]init];
    NSArray * firstArray = [dbManager getTemperatureData];
    NSArray * storeData = [self sortBydate:firstArray];
    
    if (!tempretureArray)tempretureArray = [[NSMutableArray alloc]init];
    [tempretureArray removeAllObjects];
    
    for (Temperature * tempData in storeData) {
        if ((tempData.dateTime != 0)&&(tempData.dateTime != nil)) {
//            NSLog(@"REfresh -------> Time : %@  temp : %f",tempData.dateTime,tempData.temperature.floatValue);
            [tempretureArray addObject:tempData.temperature];
        }
    }
//    NSLog(@"-----------------END----------------");
    return [self intConvert:tempretureArray];
}

-(AAChartView *)getTemperatureChart {
    
    if (!temperatureChartView)temperatureChartView = [[AAChartView alloc]init];
    temperatureChartView.delegate = self;
    temperatureChartView.scrollEnabled = NO;
    temperatureChartView.isClearBackgroundColor = YES;
//    NSArray * sampleDataArray = [[NSArray alloc] initWithObjects:[NSNumber numberWithDouble:22.43],[NSNumber numberWithDouble:10.23],[NSNumber numberWithDouble:11.23],[NSNumber numberWithDouble:8.23],[NSNumber numberWithDouble:3.23], nil];
    NSArray * temperatureTempArray = [self getTempereture]; //sampleDataArray;
    [AppUserDefaults setRecordSize:temperatureTempArray.count];
    
     aaChartModel= AAObject(AAChartModel)
    .chartTypeSet(AAChartTypeLine)
    .titleSet(@"")
    .subtitleSet(@"")
    .yAxisLineWidthSet(@1)
    .colorsThemeSet(@[@"#f20000"])
    .yAxisTitleSet(@"")
    .tooltipValueSuffixSet(@"℃")
    .backgroundColorSet(@"#f20000")
    .yAxisGridLineWidthSet(@0)
    .seriesSet(@[AAObject(AASeriesElement)
                 .nameSet(@"Temp")
                 .dataSet(temperatureTempArray)
                 .dataLabelsSet(AAObject(AADataLabels)
                                .enabledSet(true)
                                )
                 ]
               );
    
    aaChartModel.yAxisVisible = TRUE;
    aaChartModel.xAxisVisible = TRUE;
    aaChartModel.tooltipEnabled = TRUE;
    aaChartModel.yAxisLabelsEnabled = TRUE;
    aaChartModel.legendEnabled = FALSE;
    aaChartModel.yAxisLabelsFontColor = @"#FFFFFF";
    aaChartModel.pointCount = temperatureTempArray.count;
    aaChartModel.symbol = AAChartSymbolTypeCircle;
    temperatureChartView.chartModel = aaChartModel;
    [temperatureChartView aa_drawChartWithChartModel:aaChartModel];

    return temperatureChartView;
}
-(AAChartModel *)updateTemperatureChartWithData {
    
    //Get Tempreture on DB
    NSArray * temperatureTempArray = [self intConvert:[self getTempereture]] ;
    [AppUserDefaults setRecordSize:temperatureTempArray.count];
    
    //Update Chart
     aaChartModel = AAObject(AAChartModel)
    .chartTypeSet(AAChartTypeLine)
    .titleSet(@"")
    .subtitleSet(@"")
    .yAxisLineWidthSet(@1)
    .colorsThemeSet(@[@"#f20000"])
    .yAxisTitleSet(@"")
    .tooltipValueSuffixSet(@"℃")
    .backgroundColorSet(@"#f20000")  //color or carts
    .yAxisGridLineWidthSet(@0)
    .seriesSet(@[AAObject(AASeriesElement)
                 .nameSet(@"Temp")
                 .dataSet([self getTempereture])
                 .dataLabelsSet(AAObject(AADataLabels)
                                .enabledSet(true)
                                )
                 ]
               );
    
    aaChartModel.pointCount = temperatureTempArray.count;
    aaChartModel.yAxisVisible = TRUE;
    aaChartModel.xAxisVisible = TRUE;
    aaChartModel.tooltipEnabled = TRUE;
    aaChartModel.yAxisLabelsEnabled = TRUE;
    aaChartModel.legendEnabled = FALSE;
    aaChartModel.yAxisLabelsFontColor = @"#FFFFFF";
    aaChartModel.animationType = AAChartAnimationLinear;
    aaChartModel.pointCount = temperatureTempArray.count;
    aaChartModel.symbol = AAChartSymbolTypeCircle;
    return aaChartModel;
    
}
#pragma mark - Humidity Charts
-(NSArray *)getHumidity {
    NSMutableArray * tempretureArray;
    if (!dbManager)dbManager = [[DBManager alloc]init];
    NSArray * firstArray = [dbManager getTemperatureData];
    NSArray * storeData = [self sortBydate:firstArray];
    
    if (!tempretureArray)tempretureArray = [[NSMutableArray alloc]init];
    [tempretureArray removeAllObjects];
    
    for (Temperature * tempData in storeData) {
        if ((tempData.dateTime != 0)&&(tempData.dateTime != nil)) {
            [tempretureArray addObject:tempData.humidity];
        }
        
    }
    return [self intConvert:tempretureArray];
}
-(AAChartView *)getHymidityChart {
    
    if (!temperatureChartView)temperatureChartView = [[AAChartView alloc]init];
    temperatureChartView.delegate = self;
    temperatureChartView.scrollEnabled = NO;
    temperatureChartView.isClearBackgroundColor = YES;
    
    NSArray * temperatureTempArray = [self getHumidity];
    [AppUserDefaults setRecordSize:temperatureTempArray.count];
    
    
    aaChartModel= AAObject(AAChartModel)
    .chartTypeSet(AAChartTypeLine)
    .titleSet(@"")
    .subtitleSet(@"")
    .yAxisLineWidthSet(@1)
    .colorsThemeSet(@[@"#00ffec"])
    .yAxisTitleSet(@"")
    .tooltipValueSuffixSet(@"℃")
    .backgroundColorSet(@"#00ffec")
    .yAxisGridLineWidthSet(@0)
    .seriesSet(@[AAObject(AASeriesElement)
                 .nameSet(@"Hum")
                 .dataSet(temperatureTempArray)
                 .dataLabelsSet(AAObject(AADataLabels)
                                .enabledSet(true)
                                )
                 ]
               );
    
    aaChartModel.yAxisVisible = TRUE;
    aaChartModel.xAxisVisible = TRUE;
    aaChartModel.tooltipEnabled = TRUE;
    aaChartModel.yAxisLabelsEnabled = TRUE;
    aaChartModel.yAxisLabelsFontColor = @"#FFFFFF";
    aaChartModel.yAxisVisible = TRUE;
    aaChartModel.legendEnabled = FALSE;

    aaChartModel.pointCount = temperatureTempArray.count;
    aaChartModel.symbol = AAChartSymbolTypeCircle;
    
    [temperatureChartView aa_drawChartWithChartModel:aaChartModel];
    temperatureChartView.chartModel = aaChartModel;
    return temperatureChartView;
}
-(AAChartModel *)updateHymidityChartWithData {
    
    //Get Tempreture on DB
    NSArray * temperatureTempArray = [self getHumidity];
    [AppUserDefaults setRecordSize:temperatureTempArray.count];
    
    //Update Chart
    aaChartModel = AAObject(AAChartModel)
    .chartTypeSet(AAChartTypeLine)
    .titleSet(@"")
    .subtitleSet(@"")
    .yAxisLineWidthSet(@1)
    .colorsThemeSet(@[@"#00ffec"])
    .yAxisTitleSet(@"")
    .tooltipValueSuffixSet(@"℃")
    .backgroundColorSet(@"#4b2b7f")  //color or carts
    .yAxisGridLineWidthSet(@0)
    .seriesSet(@[AAObject(AASeriesElement)
                 .nameSet(@"Hum")
                 .dataSet(temperatureTempArray)
                 .dataLabelsSet(AAObject(AADataLabels)
                                .enabledSet(true)
                                )
                 ]
               );
    aaChartModel.yAxisVisible = TRUE;
    aaChartModel.xAxisVisible = TRUE;
    aaChartModel.tooltipEnabled = TRUE;
    aaChartModel.yAxisLabelsEnabled = TRUE;
    aaChartModel.yAxisLabelsFontColor = @"#FFFFFF";
    aaChartModel.pointCount = temperatureTempArray.count;
    aaChartModel.legendEnabled = FALSE;
    
    aaChartModel.symbol = AAChartSymbolTypeCircle;
    return aaChartModel;
    
}
-(NSArray *)sortBydate :(NSArray *)datesArray{
    
    NSArray *arrKeys = [datesArray sortedArrayUsingComparator:^NSComparisonResult(Temperature * obj1, Temperature * obj2) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *d1 = [dateFormatter dateFromString:obj1.dateTime];
        NSDate *d2 = [dateFormatter dateFromString:obj2.dateTime];
        return [d2 compare: d1];
    }];
    return arrKeys;
}
//-(NSArray *)floatToDoubleConvert :(NSArray *)dataArray{
//    NSMutableArray * doubleArray = [[NSMutableArray alloc]init];
//    for (NSNumber * value in dataArray) {
//       [doubleArray addObject:<#(nonnull id)#>]
//    }
//}
#pragma mark - Tempreture and Humidity Charts

- (LMLineGraphView *)getTotalChart :(float)zoomScale Type:(NSString *)parameters Line1:(NSArray *)line1 Line2:(NSArray *)line2 MySensor:(NSArray *)line3{
    //Veriables
    LMGraphPlot * nativePlot = nil;
    LMGraphPlot * customPlot_1 = nil;
    LMGraphPlot * customPlot_2 = nil;
    chartProcess = [ChartDataProcess sharedInstance];
  NSArray * sortedRange = [chartProcess processStationDataForChart:line1 Station2:line2 DeviceData:line3];
    
//Max min values
    if ([parameters isEqualToString:TEMPRETURE]) {
        symbol = @"°C";
        maxValue = [self maxValueTempereture:line1 :line2 :line3 :1];
        minValue = [self minValueTempereture:line1 :line2 :line3 :1];
    }else if ([parameters isEqualToString:HUMIDITY]){
        symbol = @"%";
        maxValue = [self maxValueTempereture:line1 :line2 :line3 :2];
        minValue = [self minValueTempereture:line1 :line2 :line3 :2];
    }
 //xAxsis
    timeLineArray = [chartProcess getTimeLine:sortedRange];
    xAxisCordinates =[self shortXAxisValues:timeLineArray] ;
    
    sortedObjects = [chartProcess getTimeLineObjects:sortedRange];
    graphView = [[LMLineGraphView alloc]initWithFrame:CGRectMake(0, 0, 362, 338)];

//Organize lines
    NSMutableArray * plots = [[NSMutableArray alloc]init];
    
    if (line1.count > 0) {
        customPlot_1 = [self plotsFromArray:line1 :[CSUMCColor stationOneSensorColor]];
        [plots addObject:customPlot_1];
    }
    if (line2.count > 0) {
        customPlot_2 = [self plotsFromArray:line2 : [CSUMCColor stationTwoSensorColor]];
        [plots addObject:customPlot_2];
    }
    if (line3.count > 0) {
        nativePlot = [self plotsFromArray:line3 : [CSUMCColor mySensorColor]];
        [plots addObject:nativePlot];
    }
    graphView.graphPlots  = plots;

    // Line Graph View 2
    graphView.layout.xAxisScrollableOnly = YES;
    graphView.layout.drawMovement = YES;
    graphView.xAxisValues     = xAxisCordinates;
//    graphView.yAxisMinValue = 10.0f;
//    graphView.yAxisMaxValue = 50.0f;
    graphView.tintColor = [UIColor redColor];
    graphView.xAxisInterval   = zoomScale;
    graphView.yAxisMaxValue   = maxValue + 10;

    if (minValue != 0) {
        minValue = minValue - 10;
    }
    graphView.yAxisMinValue   = minValue;
    
    graphView.delegate        = self;
    graphView.backgroundColor = [UIColor clearColor];
    
    return graphView;
    
}
- (NSArray *)shortXAxisValues :(NSArray *)timeLineArr
{
    NSMutableArray * dataSetArray = [[NSMutableArray alloc]init];
    for (int i = 0; i < timeLineArr.count ; i++) {
        [dataSetArray addObject:@{[NSNumber numberWithInt:i] :[NSString stringWithFormat:@"%@",[timeLineArr objectAtIndex:i]]}];
    }
    return dataSetArray;
}
- (CGFloat)maxValueTempereture :(NSArray *)line1 :(NSArray *)line2 :(NSArray *)native :(int)typeindex{
    
    NSMutableArray * masterArray = [[NSMutableArray alloc]init];
    [masterArray addObjectsFromArray:native];
    [masterArray addObjectsFromArray:line1];
    [masterArray addObjectsFromArray:line2];
    
    float selectedMax = 0;
    
    for (WSPoint * selectedData in masterArray) {
        float currentValue = 0.0;
        if (typeindex == 1) {
            currentValue = selectedData.tempreture.floatValue;
        }else{
            currentValue = selectedData.humidity.floatValue;
        }
        
        if (currentValue > selectedMax) {
            selectedMax = currentValue;
        }
    }
    return selectedMax;
}
- (CGFloat)minValueTempereture :(NSArray *)line1 :(NSArray *)line2 :(NSArray *)native :(int)typeindex{
    
    NSMutableArray * masterArray = [[NSMutableArray alloc]init];
    [masterArray addObjectsFromArray:native];
    [masterArray addObjectsFromArray:line1];
    [masterArray addObjectsFromArray:line2];
    float selectedMin = -1000;
    
    for (WSPoint * selectedData in masterArray) {
        float currentValue = 0.0;
        if (typeindex == 1) {
            currentValue = selectedData.tempreture.floatValue;
        }else{
            currentValue = selectedData.humidity.floatValue;
        }
        
        if (selectedMin == -1000) {
            selectedMin = currentValue;
        }
        
        if (currentValue < selectedMin) {
            selectedMin = currentValue;
        }
    }
    return selectedMin;
}

- (LMGraphPlot *)plotsFromArray :(NSArray *)lines :(UIColor *)lineColor
{
    NSMutableArray * graphlineTemp = [[NSMutableArray alloc]initWithArray:lines];
    
    LMGraphPlot *plots = [[LMGraphPlot alloc] init];
    plots.strokeColor = lineColor; //[UIColor brownColor];
    plots.fillColor = [UIColor clearColor];
    plots.graphPointColor = lineColor;
    NSMutableArray * graphPoints = [NSMutableArray new];
    for (int i = 0; i < timeLineArray.count ; i++) {
        
        for (WSPoint * point  in graphlineTemp) {
            if ([[timeLineArray objectAtIndex:i] isEqualToString:point.dateString]) {
                float value = 0.0;
                if ([symbol isEqualToString:@"°C"]) {
                    value = point.tempreture.floatValue;
//                    NSLog(@"collectiong %@ ----->%@ T---->  %0.2fC",point.dateString,point.wStation,value);
                }else{
                    value = point.humidity.floatValue;
//                    NSLog(@"collectiong %@ ----->%@  H---->  %0.2f",point.dateString,point.wStation,value);
                }
                NSString *titleString = [NSString stringWithFormat:@"%d %@", i,symbol];
                NSString *valueString = [NSString stringWithFormat:@"%.02f %@", value,symbol];
                LMGraphPoint *graphPoint = LMGraphPointMake(CGPointMake(i, value), titleString, valueString);
                [graphPoints addObject:graphPoint];
                [graphlineTemp removeObject:point];
                break;
            }
        }
        
        
//        WSPoint * point = [sortedObjects objectAtIndex:i];
//        if ([lines containsObject:point]) {
//
//        }
    }
    plots.graphPoints = graphPoints;
    return plots;
}
-(NSArray *)intConvert :(NSArray *)inputArray{
    
    NSMutableArray * processArray = [[NSMutableArray alloc]init];
    for (NSNumber * number in inputArray) {
        
        NSString * dataString = [NSString stringWithFormat:@"%0.2f",number.floatValue];
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *myNumber = [f numberFromString:dataString];
        [processArray addObject:myNumber];
        
    }
    
    return processArray;
}

@end
