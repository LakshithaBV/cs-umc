//
//  AppUserDefaults.m
//  RsturantApp
//
//  Created by Ganidu Ashen on 2/22/13.
//  Copyright (c) 2013 JBMdigital(pvt)Ltd. All rights reserved.
//

#import "AppUserDefaults.h"
#import "Constants.h"
#define USER_DEFAULTS [NSUserDefaults standardUserDefaults]

@implementation AppUserDefaults


+ ( void ) setUserdata:(NSDictionary *) Record
{
    [self setisLogout:NO];
    [USER_DEFAULTS setObject:Record forKey:@"USERDATA"];
    [self setAccessTokan:[Record objectForKey:@"token"]];
    [USER_DEFAULTS synchronize];
}
+ ( NSDictionary *) getUserdata
{
    return [USER_DEFAULTS objectForKey:@"USERDATA"];
}
+ ( NSString *) getAccessTokan
{
    if ([[self getUserdata] objectForKey:@"ACCESSTOKEN"] != nil) {
       return [[self getUserdata] objectForKey:@"ACCESSTOKEN"];
    }else{
        return [USER_DEFAULTS objectForKey:@"ACCESSTOKEN"];
    }
}
+ ( void ) setAccessTokan: (NSString *)Record
{
    [self setisLogout:NO];
    [USER_DEFAULTS setObject:Record forKey:@"ACCESSTOKEN"];
    [USER_DEFAULTS synchronize];
}

+ ( BOOL ) getVarified
{
    BOOL isVerified = [[[self getUserdata] objectForKey:@"verified"] boolValue];
    return isVerified;
}

+ ( NSString *) getUserId
{
    return [[self getUserdata] objectForKey:@"id"];
}
//DEVICE
+ ( BOOL ) setConnectedPeripheral:(CBPeripheral *) Record
{
    NSString * deviceId = [Record.identifier UUIDString];
    NSString * deviceName =  Record.name;
//    [USER_DEFAULTS setObject:[NSKeyedArchiver archivedDataWithRootObject:Record] forKey:@"CONNECTED_PERIPHERAL"];
    if (deviceId != NULL) {
        [USER_DEFAULTS setObject:deviceId forKey:@"PERIPHERAL_ID"];
        [USER_DEFAULTS synchronize];
        if (deviceName != NULL) {
            [USER_DEFAULTS setObject:deviceName forKey:@"PERIPHERAL_NAME"];
            [USER_DEFAULTS synchronize];
        }
        return true;
    }else{
        return false;
    }
}
+ ( CBPeripheral * ) getConnectedPeripheral {
    NSData *data = [USER_DEFAULTS objectForKey:@"CONNECTED_PERIPHERAL"];
   CBPeripheral * periparal = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    return periparal;
}
+ ( void ) setConnectedPeripheralName :(NSString *)periparalName
{
    [USER_DEFAULTS setObject:periparalName forKey:@"PERIPHERAL_NAME"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getConnectedPeripheralName
{
    return [USER_DEFAULTS objectForKey:@"PERIPHERAL_NAME"];
}
+ ( NSString *) getConnectedPeripheralID
{
    return [USER_DEFAULTS objectForKey:@"PERIPHERAL_ID"];
}

+ ( void ) setSelectedLocationCordinate:(NSDictionary *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"SENSERLOCATION_CORDINATE"];
    [USER_DEFAULTS synchronize];
}
+ ( NSDictionary *) getSelectedLocationCordinate
{
    return [USER_DEFAULTS objectForKey:@"SENSERLOCATION_CORDINATE"];
}


+ ( void ) setSelectedLatitude:(CGFloat) Record
{
    [USER_DEFAULTS setFloat:Record forKey:@"SENSERLATITUDE"];
    [USER_DEFAULTS synchronize];
}
+ (CGFloat) getSelectedLatitude
{
    return [USER_DEFAULTS floatForKey:@"SENSERLATITUDE"];
}

+ ( void ) setSelectedLongatude:(CGFloat) Record;
{
    [USER_DEFAULTS setFloat:Record forKey:@"SENSERLONGATUDE"];
    [USER_DEFAULTS synchronize];
}
+ ( void ) setCurrentSecsorPoint {
    
}
+ (CGFloat) getSelectedLongatude;
{
    return [USER_DEFAULTS floatForKey:@"SENSERLONGATUDE"];
}

+ ( void ) setisEnvironmentSetup:(BOOL) Record
{
    if (Record) {
        [USER_DEFAULTS setObject:[NSNumber numberWithInt:1] forKey:@"ISENVIRONMENTSETUP"];
    }else{
        [USER_DEFAULTS setObject:[NSNumber numberWithInt:0] forKey:@"ISENVIRONMENTSETUP"];
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:TIMERRESET_NOTITFICATIONS object:self];  //Change requestTime
    [USER_DEFAULTS synchronize];
}
+ ( BOOL) getisEnvironmentSetup
{
    if ([[USER_DEFAULTS objectForKey:@"ISENVIRONMENTSETUP"] intValue] == 1) {
        return true;
    }else{
        return false;
    }
}
+ ( void ) setENVImage:(NSData *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"ENVIRONMENTIMAGE"];
    [USER_DEFAULTS synchronize];
}
+ ( NSData *) getENVImage
{
    return [USER_DEFAULTS objectForKey:@"ENVIRONMENTIMAGE"];
}
+ ( void ) setENVPhoto:(NSData *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"ENVIRONMENTPHOTO"];
    [USER_DEFAULTS synchronize];
}
+ ( NSData *) getENVPhoto
{
    return [USER_DEFAULTS objectForKey:@"ENVIRONMENTPHOTO"];
}
+ ( void ) setRemovePhoto
{
    [USER_DEFAULTS removeObjectForKey:@"ENVIRONMENTPHOTO"];
}

+ ( void ) setRecordSize:(NSInteger) Record
{
    [USER_DEFAULTS setInteger:Record forKey:@"RECORDCOUNT"];
    [USER_DEFAULTS synchronize];
}
+ ( NSInteger) getRecordSize
{
    return [USER_DEFAULTS integerForKey:@"RECORDCOUNT"];
}

//

+ ( void ) setAttributeArray:(NSArray *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"ATTRIBUTESINDEX"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getAttributeArray
{
    return [USER_DEFAULTS objectForKey:@"ATTRIBUTESINDEX"];
}

+ ( void ) setReviewResultArray:(NSArray *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"REVIEWRESULTS"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getReviewResultArray
{
    return [USER_DEFAULTS objectForKey:@"REVIEWRESULTS"];
}

+ ( void ) setIsRecordingOn:(BOOL) Record
{
    [USER_DEFAULTS setBool:Record forKey:@"ISRECORDING"];
    [USER_DEFAULTS synchronize];
}
+ ( BOOL) getIsRecordingOn
{
    return [USER_DEFAULTS integerForKey:@"ISRECORDING"];
}

//ReviewResult
+ ( void ) setFromDate:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"RRRRRRRRRR"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getFromDate
{
    return [USER_DEFAULTS objectForKey:@"RRRRRRRRRR"];
}
+ ( void ) setToDate:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"TODATE"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getToDate
{
    return [USER_DEFAULTS objectForKey:@"TODATE"];
}
+ ( void ) setDeviceDataArray:(NSArray *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"DEVICEDATA"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getDeviceDataArray
{
    return [USER_DEFAULTS objectForKey:@"DEVICEDATA"];
}
+ ( void ) setStationDataArray:(NSArray *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"DEVICEDATA"];
    [USER_DEFAULTS synchronize];
}
+ ( NSArray *) getStationDataArray
{
    return [USER_DEFAULTS objectForKey:@"DEVICEDATA"];
}
//Map color selected.
+ ( void )setColorRanges :(NSArray *) Record{
    [USER_DEFAULTS setObject:Record forKey:@"COLORRANGES"];
    [USER_DEFAULTS synchronize];
}
+ (NSArray *)getColorRanges {
    return [USER_DEFAULTS objectForKey:@"COLORRANGES"];
}
+ ( void )setFirstAnotationRange :(NSNumber *) Record{
    
    [USER_DEFAULTS setObject:Record forKey:@"FIRSTCOLORRANGE"];
    [USER_DEFAULTS synchronize];
}
+ (NSNumber *)getFirstAnotationRange {
    return [USER_DEFAULTS objectForKey:@"FIRSTCOLORRANGE"];
}
+ ( void )setSecondAnotationRange :(NSNumber *) Record{
    
    [USER_DEFAULTS setObject:Record forKey:@"SECONDCOLORRANGE"];
    [USER_DEFAULTS synchronize];
}
+ (NSNumber *)getSecondAnotationRange {
    return [USER_DEFAULTS objectForKey:@"SECONDCOLORRANGE"];
}
+ ( void )setAnotationRange :(NSNumber *) Record{
    
    [USER_DEFAULTS setObject:Record forKey:@"SECONDCOLORRANGE"];
    [USER_DEFAULTS synchronize];
}
+ (NSNumber *)getAnotationRange {
    return [USER_DEFAULTS objectForKey:@"SECONDCOLORRANGE"];
}
+ (BOOL)removeCurrentDevice {

    [USER_DEFAULTS removeObjectForKey:@"SENSERLOCATION_CORDINATE"];
    [USER_DEFAULTS removeObjectForKey:@"SENSERLATITUDE"];
    [USER_DEFAULTS removeObjectForKey:@"SENSERLONGATUDE"];
    [USER_DEFAULTS removeObjectForKey:@"ENVIRONMENTIMAGE"];
    [USER_DEFAULTS removeObjectForKey:@"ENVIRONMENTPHOTO"];
    [USER_DEFAULTS removeObjectForKey:@"RECORDCOUNT"];
    [USER_DEFAULTS removeObjectForKey:@"ATTRIBUTESINDEX"];
    [USER_DEFAULTS removeObjectForKey:@"REVIEWRESULTS"];
    [USER_DEFAULTS removeObjectForKey:@"FROMDATE"];
    [USER_DEFAULTS removeObjectForKey:@"TODATE"];

    [self setIsRecordingOn:false];
    
    return true;
}
+ ( void ) setProfix:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"PREFIXDATA"];
    [USER_DEFAULTS synchronize];
}

+ ( NSString *) getProfix
{
    return [USER_DEFAULTS objectForKey:@"PREFIXDATA"];
}
//Register store
+ ( void ) setRegisterPayload:(NSDictionary *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"REGISTERPAYLOAD"];
    [USER_DEFAULTS synchronize];
}
+ ( NSDictionary *) getRegisterPayload
{
    return [USER_DEFAULTS objectForKey:@"REGISTERPAYLOAD"];
}
+ (void)removeRegisterPayload {
    [USER_DEFAULTS removeObjectForKey:@"REGISTERPAYLOAD"];
}
+ (BOOL)removeAllRecords:(BOOL)isWithUserdata {

    if (isWithUserdata) {
        [USER_DEFAULTS removeObjectForKey:@"USERDATA"];
        [USER_DEFAULTS removeObjectForKey:@"ACCESSTOKEN"];
        [USER_DEFAULTS removeObjectForKey:@"PERIPHERAL_ID"];
        [USER_DEFAULTS removeObjectForKey:@"PERIPHERAL_NAME"];
    }
    [USER_DEFAULTS removeObjectForKey:@"SENSERLOCATION_CORDINATE"];
    [USER_DEFAULTS removeObjectForKey:@"SENSERLATITUDE"];
    [USER_DEFAULTS removeObjectForKey:@"SENSERLONGATUDE"];
    [USER_DEFAULTS removeObjectForKey:@"ENVIRONMENTIMAGE"];
    [USER_DEFAULTS removeObjectForKey:@"ENVIRONMENTPHOTO"];
    [USER_DEFAULTS removeObjectForKey:@"RECORDCOUNT"];
    [USER_DEFAULTS removeObjectForKey:@"ATTRIBUTESINDEX"];
    [USER_DEFAULTS removeObjectForKey:@"REVIEWRESULTS"];
//    [USER_DEFAULTS removeObjectForKey:@"FROMDATE"];
    [USER_DEFAULTS removeObjectForKey:@"TODATE"];
    [USER_DEFAULTS removeObjectForKey:@"DEVICEDATA"];
    
    [self setIsRecordingOn:false];
    [self setisEnvironmentSetup:false];
    
    return true;
}
//Remove environment
+ (BOOL)removeEnvironmentRecords {
    
//    [USER_DEFAULTS removeObjectForKey:@"SENSERLATITUDE"];
//    [USER_DEFAULTS removeObjectForKey:@"SENSERLONGATUDE"];
    [USER_DEFAULTS removeObjectForKey:@"ENVIRONMENTIMAGE"];
    [USER_DEFAULTS removeObjectForKey:@"ENVIRONMENTPHOTO"];
    [USER_DEFAULTS removeObjectForKey:@"RECORDCOUNT"];
    [USER_DEFAULTS removeObjectForKey:@"ATTRIBUTESINDEX"];

    [self setIsRecordingOn:false];
    [self setisEnvironmentSetup:false];
    return true;
}
//Logout
+ ( void ) setisLogout:(BOOL) Record
{
    if (Record) {
        [USER_DEFAULTS setObject:[NSNumber numberWithInt:1] forKey:@"ISLOGOUT"];
    }else{
        [USER_DEFAULTS setObject:[NSNumber numberWithInt:0] forKey:@"ISLOGOUT"];
    }
[USER_DEFAULTS synchronize];
}
+ ( BOOL) getisLogout
{
    if ([[USER_DEFAULTS objectForKey:@"ISLOGOUT"] intValue] == 1) {
        return true;
    }else{
        return false;
    }
}
//Timer
+ ( void )setRequestTime :(NSNumber *) Record{
    
    [USER_DEFAULTS setObject:Record forKey:@"REQUESTTIMER"];
    [USER_DEFAULTS synchronize];
}
+ (NSNumber *)getRequestTime {
    return [USER_DEFAULTS objectForKey:@"REQUESTTIMER"];
}

//Row Id
+ ( void ) setRowId:(NSInteger) Record
{
    [USER_DEFAULTS setInteger:Record forKey:@"ROWID"];
    [USER_DEFAULTS synchronize];
}
+ ( NSInteger) getRowId
{
    return [USER_DEFAULTS integerForKey:@"ROWID"];
}
+ ( void ) setIsWriteForOffline:(BOOL) Record
{
    if (Record) {
        [USER_DEFAULTS setObject:[NSNumber numberWithInt:1] forKey:@"ISWROTE"];
    }else{
        [USER_DEFAULTS setObject:[NSNumber numberWithInt:0] forKey:@"ISWROTE"];
    }
    [USER_DEFAULTS synchronize];
}
+ ( BOOL) getIsWriteForOffline
{
    if ([[USER_DEFAULTS objectForKey:@"ISWROTE"] intValue] == 1) {
        return true;
    }else{
        return false;
    }
}
+ ( void ) setIsFirstTimeUser:(BOOL) Record
{
    if (Record) {
        [USER_DEFAULTS setObject:[NSNumber numberWithInt:1] forKey:@"ISFIRSTUSER"];
    }else{
        [USER_DEFAULTS setObject:[NSNumber numberWithInt:0] forKey:@"ISFIRSTUSER"];
    }
    [USER_DEFAULTS synchronize];
}
+ ( BOOL) getIsFirstTimeUser
{
    if ([[USER_DEFAULTS objectForKey:@"ISWROTE"] intValue] == 1) {
        return true;
    }else{
        return false;
    }
}
//LastLogTime
+ ( void ) setLastDisconnectedTime:(NSString *) Record
{
    [USER_DEFAULTS setObject:Record forKey:@"LASTDISCONNECTDATE"];
    [USER_DEFAULTS synchronize];
}
+ ( NSString *) getLastDisconnectedTime
{
    return [USER_DEFAULTS objectForKey:@"LASTDISCONNECTDATE"];
}
+ ( void ) removeLastDisconnectedTime
{
    return [USER_DEFAULTS removeObjectForKey:@"LASTDISCONNECTDATE"];;
}
@end
