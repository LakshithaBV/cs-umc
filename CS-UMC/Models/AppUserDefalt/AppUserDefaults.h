;//
//  AppUserDefaults.h
//  RsturantApp
//
//  Created by Ganidu Ashen on 2/22/13.
//  Copyright (c) 2013 JBMdigital(pvt)Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Constants.h"

@interface AppUserDefaults : NSObject

//ACCESS DETAILS
+ ( void ) setUserdata:(NSDictionary *) Record;
+ ( NSDictionary *) getUserdata;
+ ( NSString *) getAccessTokan;
+ ( BOOL ) getVarified;
+ ( void ) setAccessTokan: (NSString *)Record;

//DEVICE
+ ( BOOL ) setConnectedPeripheral:(CBPeripheral *) Record;
+ ( CBPeripheral * ) getConnectedPeripheral;
+ ( NSString *) getConnectedPeripheralName;
+ ( void ) setConnectedPeripheralName :(NSString *)periparalName;
+ ( NSString *) getConnectedPeripheralID;

//SENSOR LOCATION CORDINATES
+ ( void ) setSelectedLocationCordinate:(NSDictionary *) Recor;
+ ( NSDictionary *) getSelectedLocationCordinate;

//SENSOR LOCATION
+ ( void ) setSelectedLatitude:(CGFloat) Record;
+ (CGFloat) getSelectedLatitude;

+ ( void ) setSelectedLongatude:(CGFloat) Record;
+ (CGFloat) getSelectedLongatude;

//ISENVIRONMENTSETUP
+ ( void ) setisEnvironmentSetup:(BOOL) Record;
+ ( BOOL) getisEnvironmentSetup;

+ ( void ) setENVImage:(NSData *) Record;
+ ( NSData *) getENVImage;

+ ( void ) setENVPhoto:(NSData *) Record;
+ ( NSData *) getENVPhoto;
+ ( void ) setRemovePhoto;

+ ( void ) setRecordSize:(NSInteger) Record;
+ ( NSInteger) getRecordSize;

+ ( void ) setAttributeArray:(NSArray *) Record;
+ ( NSArray *) getAttributeArray;

+ ( void ) setReviewResultArray:(NSArray *) Record;
+ ( NSArray *) getReviewResultArray;

+ ( void ) setIsRecordingOn:(BOOL) Record;
+ ( BOOL ) getIsRecordingOn;

//REVIEWRESULTS
+ ( void ) setFromDate:(NSString *) Record;
+ ( NSString *) getFromDate;

+ ( void ) setToDate:(NSString *) Record;
+ ( NSString *) getToDate;

+ ( void ) setDeviceDataArray:(NSArray *) Record;
+ ( NSArray *) getDeviceDataArray;

+ ( void ) setStationDataArray:(NSArray *) Record;
+ ( NSArray *) getStationDataArray;

//COLORRANGE
+ ( void )setColorRanges :(NSArray *) Record;
+ (NSArray *)getColorRanges;

+ ( void )setFirstAnotationRange :(NSNumber *) Record;
+ (NSNumber *)getFirstAnotationRange;

+ ( void )setSecondAnotationRange :(NSNumber *) Record;
+ (NSNumber *)getSecondAnotationRange;

+ ( void )setAnotationRange :(NSNumber *) Record;
+ (NSNumber *)getAnotationRange;

//REMOVE DATA
+ (BOOL)removeCurrentDevice;

//PREFIX DATA
+ ( void ) setProfix:(NSString *) Record;
+ ( NSString *) getProfix;

+ ( void ) setRegisterPayload:(NSDictionary *) Record;
+ ( NSDictionary *) getRegisterPayload;
+ (void)removeRegisterPayload;

+ (BOOL)removeAllRecords:(BOOL)isWithUserdata;

+ ( void ) setisLogout:(BOOL) Record;
+ ( BOOL) getisLogout;

//Remove environment
+ (BOOL)removeEnvironmentRecords;

//TIMER
+ ( void )setRequestTime :(NSNumber *) Record;
+ (NSNumber *)getRequestTime;

//ROW ID
+ ( void ) setRowId:(NSInteger) Record;
+ ( NSInteger) getRowId;

+ ( void ) setIsWriteForOffline:(BOOL) Record;
+ ( BOOL) getIsWriteForOffline;
//FIRSTTIMEUSERS
+ ( void ) setIsFirstTimeUser:(BOOL) Record;
+ ( BOOL) getIsFirstTimeUser;

//LAST DISCONNECTED DATE

+ ( void ) setLastDisconnectedTime:(NSString *) Record;
+ ( NSString *) getLastDisconnectedTime;
+ ( void ) removeLastDisconnectedTime;
@end
