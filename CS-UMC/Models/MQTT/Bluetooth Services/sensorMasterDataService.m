//
//  sensorMasterDataService.m
//  CS-UMC
//
//  Created by SCIT on 4/24/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "sensorMasterDataService.h"
#import "sensorTagHumidityService.h"
#import "sensorFunctions.h"
#import "masterUUIDList.h"
#import "masterMQTTResourceList.h"

@implementation sensorMasterDataService

+(BOOL) isCorrectService:(CBService *)service {
    
    for (CBCharacteristic *c in service.characteristics) {
        if ([c.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_CURRENT_DATA]) {
            return YES;
        }
    }
    return NO;
}


-(instancetype) initWithService:(CBService *)service {
    self = [super initWithService:service];
    if (self) {
        self.btHandle = [bluetoothHandler sharedInstance];
        self.btHandle.delegate = self;
        self.service = service;
        
        for (CBCharacteristic *c in service.characteristics) {
            if ([c.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_CURRENT_DATA]) {
                currentDataBundle = c;
                [self dataUpdate:c];
            }
            //            else if ([c.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_CURRENT_DATA]) {
            //                [self dataUpdate:c];
            //            }
        }
        //        if (!(self.config && self.data && self.period)) {
        //            NSLog(@"Some characteristics are missing from this service, might not work correctly !");
        //        }
        
        self.tile.origin = CGPointMake(0, 3);
        self.tile.size = CGSizeMake(4, 2);
        self.tile.title.text = @"HHHumidity (HDC1000)";
    }
    return self;
}

-(BOOL) configureService {
    if (currentDataBundle) {
        [self.btHandle readValueFromCharacteristic:currentDataBundle];
    }
    return YES;
}

-(void)didGetNotificaitonOnCharacteristic:(CBCharacteristic *)characteristic{
//    NSLog(@"InMaster Notified for %@ values %@ ",characteristic.UUID.UUIDString,characteristic.value);
    
    if ([characteristic.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_CURRENT_DATA]) {
        [self calcValue:characteristic.value];
    }
}

-(NSString *) calcValue:(NSData *) value {
    char scratchVal[value.length];
    int16_t temp;
    int16_t hum;
    // temperature first
    temp = ((scratchVal[0] & 0xff)| ((scratchVal[1] << 8) & 0xff00));
    temp = (temp*165/65536) - 40;
    
    
    hum = ((scratchVal[2] & 0xff)| ((scratchVal[3] << 8) & 0xff00));
    hum = hum*100 / 65536;
    return [NSString stringWithFormat:@"Temp: %0.1f°C, HUM: %0.1f°C",temp,hum];
}

@end
