//
//  sensorMasterDataService.h
//  CS-UMC
//
//  Created by SCIT on 4/24/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Foundation/Foundation.h>
#import "bleGenericService.h"
#import "bluetoothHandler.h"
#import <CoreBluetooth/CoreBluetooth.h>

@interface sensorMasterDataService : bleGenericService<bluetoothHandlerDelegate>{
    CBCharacteristic * currentDataBundle;
    CBCharacteristic * LoggedDataBundle;
}
@property CGFloat humidity;
@end
