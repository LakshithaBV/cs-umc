//
//  DeviceTracker.m
//  checkBackground
//
//  Created by SCIT on 1/30/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "DeviceTracker.h"

@implementation DeviceTracker
- (id)init {
    if (self==[super init]) {
        //Get the share model and also initialize myLocationArray
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}
-(void)applicationEnterBackground{
    self.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
    [self.bgTask beginNewBackgroundTask];
}

-(void)updateLocation {
    NSLog(@"updateLocation");
//    [self.locationTracker updateLocationToServer];
}
- (void) restartLocationUpdates
{
    NSLog(@"restartLocationUpdates");
}

- (void)startLocationTracking {
    NSLog(@"startLocationTracking");
    NSTimeInterval time = 20.0;
    self.locationUpdateTimer =[NSTimer scheduledTimerWithTimeInterval:time
                                                               target:self
                                                             selector:@selector(updateLocation)
                                                             userInfo:nil
                                                              repeats:YES];
}
//Send the location to Server
- (void)updateLocationToServer {
    NSLog(@"updateLocationToServer");
}

@end
