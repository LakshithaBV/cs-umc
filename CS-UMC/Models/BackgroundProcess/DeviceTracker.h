//
//  DeviceTracker.h
//  checkBackground
//
//  Created by SCIT on 1/30/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BackgroundTaskManager.h"

@interface DeviceTracker : NSObject

@property (nonatomic) BackgroundTaskManager * bgTask;
- (void)startLocationTracking;
- (void)updateLocationToServer;
@property (nonatomic) NSTimer* locationUpdateTimer;
@end
