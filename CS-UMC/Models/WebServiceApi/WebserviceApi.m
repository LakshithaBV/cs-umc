//
//  WebserviceApi.m
//  PosApp
//
//  Created by Lakshitha on 8/12/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import "WebserviceApi.h"
#import "SWNetworking.h"
#import "AppUserDefaults.h"
#import "Constants.h"

@implementation WebserviceApi
@synthesize delegate;
+ (WebserviceApi *)sharedInstance
{
    static WebserviceApi *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[WebserviceApi alloc] init];
    });
    return sharedInstance;
}
#pragma mark -
#pragma mark Register

-(void)Register :(NSDictionary *)params{
    NSString *request = [NSString stringWithFormat:@"%@v1/device/add",SERVICEURL];
    NSLog(@"request %@ Params %@",request,params);
 
    SWPOSTRequest *postRequest = [[SWPOSTRequest alloc]init];
    postRequest.responseDataType = [SWResponseJSONDataType type];
 
    [postRequest startDataTaskWithURL:request parameters:params parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveRegisterinfo:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceiveRegisterinfo:errorMainDic];
    }];
}

#pragma mark -
#pragma mark getEnvironment

-(void)GetEnvironment {
    NSString *request = [NSString stringWithFormat:@"%@v1/device/environment/get",SERVICEURL];
    
    SWGETRequest *getRequest = [[SWGETRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [getRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    getRequest.responseDataType = [SWResponseJSONDataType type];
    
    [getRequest startDataTaskWithURL:request parameters:nil parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveEnvironmentinfo:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceiveEnvironmentinfo:errorMainDic];
    }];
}

#pragma mark -
#pragma mark SetEnvironment
-(void)setEnvironment :(NSDictionary *)params Location:(UIImage *)locationImage Map:(UIImage *)mapImage{
    NSString *request = [NSString stringWithFormat:@"%@v1/device/environment/add",SERVICEURL];
    
    NSMutableArray * dataFiles = [[NSMutableArray alloc]init];
    NSData *mapImageData = UIImagePNGRepresentation(mapImage);
    SWMedia *file2 = [[SWMedia alloc]initWithFileName:@"mapImage.png" key:@"map_image" data:mapImageData];
    [dataFiles addObject:file2];
    
    if (locationImage != nil) {
        NSData *imageData = UIImagePNGRepresentation(locationImage);
        SWMedia *file1 = [[SWMedia alloc]initWithFileName:@"imagefile.png" key:@"location_image" data:imageData];
        [dataFiles addObject:file1];
    }

    SWPOSTRequest *postRequest = [[SWPOSTRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [postRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    postRequest.responseDataType = [SWResponseJSONDataType type];
    
    
    [postRequest startUploadTaskWithURL:request files:dataFiles parameters:params parentView:nil  success:^(NSURLSessionUploadTask *uploadTask, id responseObject) {
        [self.delegate DidReceiveEnvironmentinfo:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
    }];
    [postRequest setUploadProgressBlock:^(long long bytesWritten, long long totalBytesExpectedToWrite) {
        NSLog(@"bytesWritten => %lld and totalBytesExpectedToWrite = %lld", bytesWritten, totalBytesExpectedToWrite);
    }];
}

#pragma mark -
#pragma mark Post SenserData

-(void)sendData :(NSDictionary *)params{
    NSString *request = [NSString stringWithFormat:@"%@v1/device/reading/add",SERVICEURL];
    NSLog(@"request %@ Params %@",request,params);
    
    SWPOSTRequest *postRequest = [[SWPOSTRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [postRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    postRequest.responseDataType = [SWResponseJSONDataType type];
    
    [postRequest startDataTaskWithURL:request parameters:params parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveSendSensorDatainfo:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceiveSendSensorDatainfo:errorMainDic];
    }];
}
#pragma mark -
#pragma mark Check sensor availability in BE

-(void)getIsDeviceAvailable :(NSDictionary *)params{
    NSString *request = [NSString stringWithFormat:@"%@v1/sensor/get",SERVICEURL];
    NSLog(@"request %@ Params %@",request,params);
    
    SWPOSTRequest *postRequest = [[SWPOSTRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [postRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    postRequest.responseDataType = [SWResponseJSONDataType type];
    
    [postRequest startDataTaskWithURL:request parameters:params parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveSensorinfo:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceiveSensorinfo:errorMainDic];
    }];
}

#pragma mark -
#pragma mark Location Attribute

-(void)getLocationAttribute {
    NSString *request = [NSString stringWithFormat:@"%@v1/device/location/attributes/get",SERVICEURL];
    SWGETRequest *getRequest = [[SWGETRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [getRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    getRequest.responseDataType = [SWResponseJSONDataType type];
    
    [getRequest startDataTaskWithURL:request parameters:nil parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveLocationAttributes:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceiveLocationAttributes:errorMainDic];
    }];
}
#pragma mark -
#pragma mark Location Attribute
-(NSString *)getCurrentDate {
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"]; // Date formater
    NSString *date = [dateformate stringFromDate:[NSDate date]];
    return date;
}
//-(void)getHeatMapLocations {
//
//    NSString * dataString =@"2018-02-4"; //[self getCurrentDate];
//    NSLog(@"LogData %@",dataString);
//
//    NSString *request = [NSString stringWithFormat:@"%@v1/device/heatmap/get?date=%@%%2000:00:00",SERVICEURL,dataString];
//    SWGETRequest *getRequest = [[SWGETRequest alloc]init];
//    NSString * accessToken = [AppUserDefaults getAccessTokan];
//    [getRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
//    getRequest.responseDataType = [SWResponseJSONDataType type];
//
//    [getRequest startDataTaskWithURL:request parameters:nil parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
//        NSLog(@"%@", responseObject);
//    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
//        NSLog(@"%@", responseObject);
//        [self.delegate DidReceiveHeatMapLocationData:responseObject];
//    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
//        NSLog(@"%@", error);
//        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
//                                    @"errorcode" : @"404"};
//        NSDictionary * errorMainDic = @{@"error" : errorDic};
//        [self.delegate DidReceiveHeatMapLocationData:errorMainDic];
//    }];
//}
-(void)getHeatMapLocations :(NSDictionary *)params{
    
    NSString *request = [NSString stringWithFormat:@"%@v1/device/heatmap/get",SERVICEURL];
    NSLog(@"request %@ Params %@",request,params);
    
    SWPOSTRequest *postRequest = [[SWPOSTRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [postRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    postRequest.responseDataType = [SWResponseJSONDataType type];
    
    [postRequest startDataTaskWithURL:request parameters:params parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
         [self.delegate DidReceiveHeatMapLocationData:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
         [self.delegate DidReceiveHeatMapLocationData:errorMainDic];
    }];
}
-(void)getSensorStations {

    NSString *request = [NSString stringWithFormat:@"%@v1/device/stations/get",SERVICEURL];
    SWGETRequest *getRequest = [[SWGETRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [getRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    getRequest.responseDataType = [SWResponseJSONDataType type];
    
    [getRequest startDataTaskWithURL:request parameters:nil parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
//        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
//        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveSendSensorDatainfo:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
         [self.delegate DidReceiveSendSensorDatainfo:errorMainDic];
    }];
}
-(void)getSensorStationsData :(NSDictionary *)params{
    NSString *request = [NSString stringWithFormat:@"%@v1/device/station_data/get",SERVICEURL];
    NSLog(@"request %@ Params %@",request,params);
    
    SWPOSTRequest *postRequest = [[SWPOSTRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [postRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    postRequest.responseDataType = [SWResponseJSONDataType type];
    
    [postRequest startDataTaskWithURL:request parameters:params parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveSensorStationsData:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceiveSensorStationsData:errorMainDic];
    }];
}
-(void)getPrefix {
    
    NSString *request = [NSString stringWithFormat:@"%@v1/sensor/prefix",SERVICEURL];
    SWGETRequest *getRequest = [[SWGETRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [getRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    getRequest.responseDataType = [SWResponseJSONDataType type];
    
    [getRequest startDataTaskWithURL:request parameters:nil parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceivePrefixData:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceivePrefixData:errorMainDic];
    }];
}
-(void)getisExperimentExsist  :(NSDictionary *)params{
    
    NSString *request = [NSString stringWithFormat:@"%@v1/sensor/check_exp_exist",SERVICEURL];
    SWPOSTRequest *postRequest = [[SWPOSTRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [postRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    postRequest.responseDataType = [SWResponseJSONDataType type];
    
    [postRequest startDataTaskWithURL:request parameters:params parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveexperimentDataInfo:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceiveexperimentDataInfo:errorMainDic];
    }];
}
-(void)sendBulkData :(NSDictionary *)params{
    NSString *request = [NSString stringWithFormat:@"%@v1/device/reading/bulk_add",SERVICEURL];
    NSLog(@"request %@ Params %@",request,params);
    
    SWPOSTRequest *postRequest = [[SWPOSTRequest alloc]init];
    NSString * accessToken = [AppUserDefaults getAccessTokan];
    [postRequest.request setValue:accessToken forHTTPHeaderField:@"Authorization"];
    postRequest.responseDataType = [SWResponseJSONDataType type];
    
    [postRequest startDataTaskWithURL:request parameters:params parentView:nil cachedData:^(NSCachedURLResponse *response, id responseObject) {
        NSLog(@"%@", responseObject);
    } success:^(NSURLSessionDataTask *uploadTask, id responseObject) {
        NSLog(@"%@", responseObject);
        [self.delegate DidReceiveBulkDataInfo:responseObject];
    } failure:^(NSURLSessionTask *uploadTask, NSError *error) {
        NSLog(@"%@", error);
        NSDictionary * errorDic = @{@"message" : NSLocalizedString(@"server_error", nil),
                                    @"errorcode" : @"404"};
        NSDictionary * errorMainDic = @{@"error" : errorDic};
        [self.delegate DidReceiveBulkDataInfo:errorMainDic];
    }];
}
@end
