//
//  WebserviceApi.h
//  PosApp
//
//  Created by Lakshitha on 8/12/14.
//  Copyright (c) 2014 Lakshitha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"
#import "DBManager.h"
#import "AppUserDefaults.h"

@class DBManager;


@protocol webservicedelegate <NSObject>
@optional

-(void)DidReceiveRegisterinfo :(id)respond;
-(void)DidReceiveEnvironmentinfo :(id)respond;
-(void)DidReceiveSetEnvironmentinfo :(id)respond;
-(void)DidReceiveSendSensorDatainfo :(id)respond;
-(void)DidReceiveSensorinfo :(id)respond;
-(void)DidReceiveLocationAttributes :(id)respond;
-(void)DidReceiveHeatMapLocationData :(id)respond;
-(void)DidReceiveSensorStationsinfo :(id)respond;
-(void)DidReceiveSensorStationsData :(id)respond;
-(void)DidReceivePrefixData :(id)respond;
-(void)DidReceiveBulkDataInfo :(id)respond;
-(void)DidReceiveexperimentDataInfo :(id)respond;


@end

@interface WebserviceApi : NSObject{
    
    MBProgressHUD *HUD;
    DBManager * dbmanager;
    
}
@property ( nonatomic, retain ) id <webservicedelegate> delegate;

+ (WebserviceApi *)sharedInstance;

-(void)Register :(NSDictionary *)params;
-(void)GetEnvironment ;
-(void)setEnvironment :(NSDictionary *)params Location:(UIImage *)locationImage Map:(UIImage *)mapImage;
-(void)sendData :(NSDictionary *)params;
-(void)getIsDeviceAvailable :(NSDictionary *)params;
-(void)getLocationAttribute ;
//-(void)getHeatMapLocations ;
-(void)getHeatMapLocations :(NSDictionary *)params;
-(void)getSensorStations ;
-(void)getSensorStationsData :(NSDictionary *)params;
-(void)getPrefix;
-(void)sendBulkData :(NSDictionary *)params;
-(void)getisExperimentExsist  :(NSDictionary *)params;

@end
