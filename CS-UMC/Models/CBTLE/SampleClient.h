//
//  SampleClient.h
//
//  Created by Tim Burks on 8/10/12.
//  Copyright (c) 2012 Tim Burks. All rights reserved.
//
#import <CoreBluetooth/CoreBluetooth.h>

@interface SampleClient : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>{
    
    
}
@property (nonatomic, strong) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) CBCharacteristic *characteristic;

@property (nonatomic, strong) CBCharacteristic *LEDcharacteristic;
- (void) startScan;
- (void) stopScan;
- (void) disconnect;
@end
