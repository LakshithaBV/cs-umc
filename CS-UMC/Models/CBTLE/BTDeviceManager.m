//
//  BTDeviceManager.m
//  CS-UMC
//
//  Created by SCIT on 1/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "BTDeviceManager.h"
#import "Constants.h"
#import <math.h>

static  int  TIMEINTERVAL_FOR_BACKGROUND  = 1800;
static  int  TIMEINTERVAL            = 10;
static  NSString * TIMEINTERVALHEXA  = @"1E00";
static  NSString * RESETSENSORDATA   = @"0200";
static  NSString * STOPHISTRYDATA    = @"0000";

@implementation BTDeviceManager 

+ (BTDeviceManager *)sharedInstance
{
    static BTDeviceManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[BTDeviceManager alloc] init];
        
    });
   
    return sharedInstance;
}
-(void)startSearching{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resetRequestTimer)
                                                 name:TIMERRESET_NOTITFICATIONS
                                               object:nil];
    NSDictionary *options = @{CBCentralManagerOptionShowPowerAlertKey: @NO};
    self.manager = [[CBCentralManager alloc]initWithDelegate:self queue:nil options:options];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
//    [self connect];
    [self.manager scanForPeripheralsWithServices:nil options:nil];
    NSLog(@"Search start");
}
-(void)applicationEnterBackground{
    
    if (self.bgTask == nil) {
        NSLog(@"start backbround task");
        self.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
        [self.bgTask beginNewBackgroundTask];
    }
}
-(void) centralManagerDidUpdateState:(CBCentralManager *)central {
    if (central.state == CBManagerStatePoweredOn) {
        if (!self.isStatusCheck) {
            [central scanForPeripheralsWithServices:nil options:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber  numberWithBool:YES], CBCentralManagerScanOptionAllowDuplicatesKey, nil]];
        }else{
            [self.delegate getIsBlueToothOn:YES];
        }
    }else{
        [self.delegate getIsBlueToothOn:NO];
    }
}
-(void) centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI {
    
    if (!self.discoveredDevices) {
        self.discoveredDevices = [[NSMutableArray alloc]init];
    }
    for (CBPeripheral *p in self.discoveredDevices) {
        if ([p.identifier isEqual:peripheral.identifier]) return;
    }
    [self.discoveredDevices addObject:peripheral];
    [self.delegate didSelectedDeviceFromSearch:self.discoveredDevices];
    
    NSLog(@"Device Tetected name= %@  ID= %@",peripheral.name,peripheral.identifier.UUIDString);
    
}

-(void)connect{
    self.handler = [bluetoothHandler sharedInstance];
    self.cloudHandle = [MQTTIBMQuickStart sharedInstance];
    self.handler.delegate = self;
    
    NSUUID * uuid = [[NSUUID alloc]initWithUUIDString:[AppUserDefaults getConnectedPeripheralID]];
    self.handler.connectToIdentifier = uuid;
    self.handler.shouldReconnect = YES;
    
    if (self.currentPeripheral!= nil) {
        [self.handler setP:self.currentPeripheral];
        [self.handler setConnectToIdentifier:self.currentPeripheral.identifier];
//        [self deviceReady:YES peripheral:self.currentPeripheral];
    }
}
-(void)disconnect{
    if (![AppUserDefaults getisLogout]) {
        self.handler = [bluetoothHandler sharedInstance];
        self.cloudHandle = [MQTTIBMQuickStart sharedInstance];
        [AppUserDefaults setisLogout:true];
        [self.handler disconnectCurrentDevice];
//        [AppUserDefaults removeAllRecords:false];
        if (!dbManager)dbManager = [[DBManager alloc]init];
        [dbManager deleteAllRecords];
        [[NSNotificationCenter defaultCenter]postNotificationName:LOGOUT_NOTITFICATIONS object:self];
    }
}
-(void)retriveSensorData{
    if (measurements != nil) {
        
        if (!sensorHistoryData)sensorHistoryData = [[NSMutableArray alloc]init];
        [sensorHistoryData removeAllObjects];
        
        [self.handler readValueFromCharacteristic:measurements];
        
        indexValue = 0;
        NSString* str = @"0000";
        NSData* data = [self dataWithHexString:str];
        [self.handler writeValue:data toCharacteristic:savedValue];
        [self.handler readValueFromCharacteristic:requestHistoryValue];
    }else{
        [self resetSensor];
    }
}
//-(void) MQTTTimerTick: (NSTimer *)timer {
//    if (self.MQTTStringTX.length > 2) {
//        self.MQTTStringTX = [self.MQTTStringTX substringToIndex:self.MQTTStringTX.length - 2];
//    }
//    NSLog(@"Posting : %@",self.MQTTStringTX);
//    [self.delegate activeBTStream:self.MQTTDictionaryTX];
//    [self.cloudHandle publishSensorStrings:self.MQTTStringTX];
//}

-(void) deviceReady:(BOOL)ready peripheral:(CBPeripheral *)peripheral {
    [self.delegate getIsBlueToothDeviceConnected:ready :peripheral];
    
    if (ready) {
        for (int ii = 0; ii < self.displayTiles.count; ii++) {
            displayTile *t = [self.displayTiles objectAtIndex:ii];
            [t removeFromSuperview];
        }
        self.services = [[NSMutableArray alloc] init];
        for (CBService *s in peripheral.services) {
            if ([deviceInformationService isCorrectService:s]) {
                deviceInformationService *serv = [[deviceInformationService alloc] initWithService:s];
                [self.services addObject:serv];
                [serv configureService];
                displayTile *t = [serv getViewForPresentation];
                [self.displayTiles addObject:t];
            }
            if ([sensorTagKeyService isCorrectService:s]) {
                sensorTagKeyService *serv = [[sensorTagKeyService alloc] initWithService:s];
                [self.services addObject:serv];
                [serv configureService];
                displayTile *t = [serv getViewForPresentation];
                [self.displayTiles addObject:t];
                
            }
            if ([sensorMasterDataService isCorrectService:s]) {
                NSLog(@"Connected");
                for (CBCharacteristic *c in s.characteristics) {
                    if ([c.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_CURRENT_DATA]) {
                        currentDataBundle = c;
                        if (self.MQTTTimer == nil) {
                            self.MQTTTimer = [NSTimer scheduledTimerWithTimeInterval:TIMEINTERVAL target:self selector:@selector(MQTTTimerTick:) userInfo:nil repeats:YES];
                        }
                    }

//                    if ([AppUserDefaults getLastDisconnectedTime].length > 3) {
                        //History data
                        if ([c.UUID.UUIDString isEqualToString:TI_SENSORTAG_MINUTES_SINCE_LASTLOG]) {
                            measurements = c;
                            
//                             if ([AppUserDefaults getLastDisconnectedTime].length > 3) {
//                                 [self.handler readValueFromCharacteristic:measurements];
//
//                                 indexValue = 0;
//                                 NSString* str = @"0000";
//                                 NSData* data = [self dataWithHexString:str];
//                                 [self.handler writeValue:data toCharacteristic:savedValue];
//                                 [self.handler readValueFromCharacteristic:requestHistoryValue];
//                             }else{
//                                 [self resetSensor];
//                             }
                        }
                        if ([c.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_LOGGED_DATA]) {
                            savedValue = c;
//                            indexValue = 0;
//                            NSString* str = @"0000";
//                            NSData* data = [self dataWithHexString:str];
//                            [self.handler setP:self.currentPeripheral];
//                            [self.handler writeValue:data toCharacteristic:savedValue];
//                            [self.handler readValueFromCharacteristic:requestHistoryValue];
                        }
                        if ([c.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_DATA_REQUEST]) {
                            requestHistoryValue = c;
                        }
                        if ([c.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_HISTORY_DATA]) {
                            historydataDataBundle = c;
                        }
//                    }else{
//                        [self resetSensor];
//                    }
                }
            }
        }
    }
    else {
        
        [self.delegate didDisconnectDevice:peripheral];
        [self disconnect];
    }
}

-(void) MQTTTimerTick: (NSTimer *)timer {
    
    if (currentDataBundle) {
        [self.handler readValueFromCharacteristic:currentDataBundle];
    }
}
-(void)resetRequestTimer{
    if (self.MQTTTimer) {
        [self.MQTTTimer invalidate];
        self.MQTTTimer = nil;
    }
    NSLog(@"Reset requested");
    double requestTime = 0;
    
    BOOL isEnvironmentSetup = [AppUserDefaults getisEnvironmentSetup];
    if (isEnvironmentSetup) {
        requestTime = TIMEINTERVAL; // 300;
        self.MQTTTimer = [NSTimer scheduledTimerWithTimeInterval:requestTime target:self selector:@selector(MQTTTimerTick:) userInfo:nil repeats:YES];
    }else{
        requestTime = TIMEINTERVAL;
        self.MQTTTimer = [NSTimer scheduledTimerWithTimeInterval:requestTime target:self selector:@selector(MQTTTimerTick:) userInfo:nil repeats:YES];
    }
}
-(void)didGetNotificaitonOnCharacteristic:(CBCharacteristic *)characteristic{
    //    NSLog(@"InMaster Notified for %@ values %@ ",characteristic.UUID.UUIDString,characteristic.value);
    if ([characteristic.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_CURRENT_DATA]) {
        [self calcValue:characteristic.value];
    }
    if ([characteristic.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_HISTORY_DATA]) {
        //        NSLog(@"HISTORYDATA VALUE %@",characteristic.value);
    }
    if ([characteristic.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_LOGGED_DATA]) {
        //        NSLog(@"LOGEDDATA VALUE %@",characteristic.value);
    }
    if ([characteristic.UUID.UUIDString isEqualToString:TI_SENSORTAG_MINUTES_SINCE_LASTLOG]) {
                NSLog(@"DELAY %@",characteristic.value);
        timeDelay = [self intFromHexString:[NSString stringWithFormat:@"%@",characteristic.value]];
        if (timeDelay != 0) {
            NSLog(@"DELAY_Minutes %i",timeDelay);
//           NSData * data = [self dataWithHexString:TIMEINTERVALHEXA]; //No histry data on sensor
////            [self.handler setP:self.currentPeripheral];
//            [self.handler writeValue:data toCharacteristic:historydataDataBundle];
//            [self resetSensor];
        }
    }
    if ([characteristic.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_DATA_REQUEST]) {
        if ([self validateDataStream:[NSString stringWithFormat:@"%@",characteristic.value]]) {
            [self performSelector:@selector(writeToNextValue) withObject:self afterDelay:0.1];
            [AppUserDefaults setIsWriteForOffline:true];
            [self calcHistoryValue:characteristic.value];
            NSLog(@"Counting");
        }else{
            [AppUserDefaults setIsWriteForOffline:false];
            NSLog(@"loop ended.");
            [self resetSensor];
            [[NSNotificationCenter defaultCenter]postNotificationName:TEMPRETURE_SAVED_NOTITFICATIONS object:self];
        }
    }
}
-(void)resetSensor{
    NSData * data = [self dataWithHexString:RESETSENSORDATA]; //No histry data on sensor
    if (self.currentPeripheral!= nil) {
        [self.handler writeValue:data toCharacteristic:historydataDataBundle];
        NSLog(@"Reset to write history");
        data = [self dataWithHexString:TIMEINTERVALHEXA]; //No histry data on sensor
        [self.handler writeValue:data toCharacteristic:historydataDataBundle];
    }else{
        NSLog(@"periparal not valid");
    }
    [self.delegate didFinishRetriveData:true :sensorHistoryData];
}
-(void)resetSensorByController{
    NSData * data = [self dataWithHexString:RESETSENSORDATA]; //No histry data on sensor
    if (self.currentPeripheral!= nil) {
        [self.handler writeValue:data toCharacteristic:historydataDataBundle];
        NSLog(@"Reset to write history");
        data = [self dataWithHexString:TIMEINTERVALHEXA]; //No histry data on sensor
        [self.handler writeValue:data toCharacteristic:historydataDataBundle];
    }else{
        NSLog(@"periparal not valid");
    }
}

-(void)didWriteCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    //    NSLog(@"writting value %@ to char %@",characteristic.value,characteristic.UUID.UUIDString);
    
    if ([characteristic.UUID.UUIDString isEqualToString:TI_SENSORTAG_TWO_LOGGED_DATA]) {
        //        NSLog(@"wrote to get history value");
    }
    
    if (error != nil) {
        NSLog(@"error %@",error.localizedDescription);
    }
}
- (int)intFromHexString:(NSString *) hexStr
{
    NSString *stringWithoutChar = [hexStr stringByReplacingOccurrencesOfString:@">" withString:@""];
    stringWithoutChar = [stringWithoutChar stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    stringWithoutChar = [stringWithoutChar substringToIndex:[stringWithoutChar length]-1];
    stringWithoutChar = [stringWithoutChar substringToIndex:[stringWithoutChar length]-1];
    
    NSString * hexString = [NSString stringWithFormat:@"%llu",(UInt64)strtoull([stringWithoutChar UTF8String], NULL, 16)];
    int hexInt = [hexString intValue];
    return hexInt;
}
-(BOOL)validateDataStream :(NSString *)stream{
    
    NSString *stringWithoutChar = [stream stringByReplacingOccurrencesOfString:@">" withString:@""];
    stringWithoutChar = [stringWithoutChar stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    stringWithoutChar = [stringWithoutChar substringToIndex:[stringWithoutChar length]-1];
    stringWithoutChar = [stringWithoutChar substringToIndex:[stringWithoutChar length]-1];
    stringWithoutChar = [[stringWithoutChar componentsSeparatedByString: @" "] firstObject];
    
    NSString * hexString = [NSString stringWithFormat:@"%llu",(UInt64)strtoull([stringWithoutChar UTF8String], NULL, 16)];
    int hexInt = [hexString intValue];
    
    if (hexInt == 0) {
        return false;
    }else{
        return true;
    }
    
}
-(NSString*) NSDataToHex:(NSData*)data
{
    const unsigned char *dbytes = [data bytes];
    NSMutableString *hexStr =
    [NSMutableString stringWithCapacity:[data length]*2];
    int i;
    for (i = 0; i < [data length]; i++) {
        [hexStr appendFormat:@"%02x ", dbytes[i]];
    }
    return [NSString stringWithString: hexStr];
}
-(void)writeToNextValue{
    
    indexValue++;
    NSString * str = [NSString stringWithFormat:@"%02x00",(unsigned int)indexValue];
    
    if (str.length > 4) {
        str  = [str substringToIndex:[str length]-1];
    }
    
    NSData* data = [self dataWithHexString:str];
    NSLog(@"writting %@",str);
//    [self.handler setP:self.currentPeripheral];
    [self.handler writeValue:data toCharacteristic:savedValue];
    [self.handler readValueFromCharacteristic:requestHistoryValue];
    [self.handler readValueFromCharacteristic:measurements]; //Reading
}
- (NSData *)dataWithHexString:(NSString *)hexstring
{
    NSMutableData* data = [NSMutableData data];
    int idx;
    for (idx = 0; idx+2 <= hexstring.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [hexstring substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
        [data appendBytes:&intValue length:1];
    }
    return data;
}
-(NSString *) calcValue:(NSData *) value {
    char scratchVal[value.length];
    double temp;
    double hum;
    
    
    [value getBytes:&scratchVal length:value.length];
    
    //TEMP
    temp = (scratchVal[0] & 0xff) | ((scratchVal[1] << 8) & 0xff00);
    temp = (double)((temp*165/65536) - 40);
    double dbtemp = [[NSString stringWithFormat:@"%0.2f",hum] doubleValue];
    
    //Humi
    hum = ((scratchVal[2] & 0xff)| ((scratchVal[3] << 8) & 0xff00));
    hum = (hum*100 / 65536);
    double dbhum = [[NSString stringWithFormat:@"%0.2f",[self getAbsoluteHumidity:temp :hum]] doubleValue];
    
    NSString * dataString = [NSString stringWithFormat:@"Temp: %0.2f°C, HUM: %0.1f°C",temp,hum];
    NSLog(@"%@",dataString);
    
    //Converter
    NSString * tempString = [NSString stringWithFormat:@"%0.2f",temp];
    float tempDouble = [tempString floatValue];
    
    if (![AppUserDefaults getIsWriteForOffline]) {
        if (!dbManager)dbManager = [[DBManager alloc]init];
        
        NSString * currentDateTime = [self getCurrentDateTime];
        if ((currentDateTime != nil)&&(currentDateTime.length != 0)) {
            [dbManager storeTemprature:tempDouble :dbhum :10.00 :[NSDate date] :currentDateTime];
            [[NSNotificationCenter defaultCenter]postNotificationName:TEMPRETURE_SAVED_NOTITFICATIONS object:self];
        }
    }

    //Send Data
    if ([AppUserDefaults getIsRecordingOn]) {
//        [self sendForBackEnd:temp :hum :0.0 :0.0];
    }
    
    
    return [NSString stringWithFormat:@"Temp: %0.1f°C, HUM: %0.1f°C",temp,dbhum];
}
-(NSString *) calcHistoryValue:(NSData *) value {
    char scratchVal[value.length];
    double temp;
    double hum;
    
    
    [value getBytes:&scratchVal length:value.length];
    
    //TEMP
    temp = (scratchVal[0] & 0xff) | ((scratchVal[1] << 8) & 0xff00);
    temp = (double)((temp*165/65536) - 40);
    double dbtemp = [[NSString stringWithFormat:@"%0.2f",hum] doubleValue];
    
    //Humi
    hum = ((scratchVal[2] & 0xff)| ((scratchVal[3] << 8) & 0xff00));
    hum = (hum*100 / 65536);
    double dbhum = [[NSString stringWithFormat:@"%0.2f",[self getAbsoluteHumidity:temp :hum]] doubleValue];
    
    NSString * dataString = [NSString stringWithFormat:@"Temp: %0.2f°C, HUM: %0.1f°C",temp,hum];
    NSLog(@"History index %@",dataString);
    
    //Converter
    NSString * tempString = [NSString stringWithFormat:@"%0.2f",temp];
    float tempDouble = [tempString floatValue];
    
    [self validateNoOfRecords];  //format if record count more than 96
    
    
    //Save in Array
    NSDictionary * weatherdic = @{@"temperature":[NSString stringWithFormat:@"%0.2f",tempDouble],
                                  @"humidity":[NSString stringWithFormat:@"%0.2f",dbhum],
                                  @"pressure" : @"0.0",
                                  @"light" : @"0.0",
                                  @"dateTime":[self dateTimeInHistory]};
    [sensorHistoryData addObject:weatherdic];
    
    
//    if (!dbManager)dbManager = [[DBManager alloc]init];
//    [dbManager storeTemprature:tempDouble :dbhum :10.00 :[NSDate date] :[self dateTimeInHistory]];
    
    return [NSString stringWithFormat:@"Temp: %0.1f°C, HUM: %0.1f°C",temp,dbhum];
}
-(void)validateNoOfRecords{
    NSString * lastDisconnectedTime = [AppUserDefaults getLastDisconnectedTime];
    if (lastDisconnectedTime != nil) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate * terminatetime = [dateFormatter dateFromString:lastDisconnectedTime];
        NSDate * newDate = [NSDate date];
        NSTimeInterval distanceBetweenDates = [newDate timeIntervalSinceDate:terminatetime];
        NSInteger hoursBetweenminits = distanceBetweenDates/60;
        NSInteger numberofRecords = hoursBetweenminits / 5;
        
        if (numberofRecords > 96) {
            NSLog(@"formatted");
            [AppUserDefaults removeLastDisconnectedTime];
            if (!dbManager)dbManager = [[DBManager alloc]init];
            [dbManager deleteAllRecords];
        }
        NSLog(@"No_of_records %i",(int)numberofRecords);
    }
}
-(NSString *)dateTimeInHistory {
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * currentTime =  [NSDate date];
    NSDate *newDate = [currentTime dateByAddingTimeInterval:-((TIMEINTERVAL_FOR_BACKGROUND*indexValue) + timeDelay)];
    NSLog(@"Date Loged %@",[dateFormatter stringFromDate:newDate]);
    return [dateFormatter stringFromDate:newDate];
}
-(void)applyinitTimeDelay{
    
}
-(float)getAbsoluteHumidity:(float)T :(float)RH{
    float Ar = 6.116441f;
    float m = 7.591386f;
    float Tn = 240.7263f;
    float Tk = 273.15f;
    float C = 2.16679f;
    float c1 = (m * T) / (T + Tn);
    float pow = (float) powf(10, c1);
    float Pws = Ar * pow;
    float Pw = (Pws * RH) / 100;
    float Aa = (C * Pw * 100) / (T + Tk);
    return Aa;
}
//sendto backend

-(void)sendForBackEnd :(float)temperature :(float)humidity :(float)pressure :(float)light{
    
        NSDictionary * requestDic = @{@"temperature":[NSString stringWithFormat:@" %0.1f",temperature],
                                      @"humidity" :[NSString stringWithFormat:@" %0.1f",humidity],
                                      @"pressure" : [NSString stringWithFormat:@" %0.1f",pressure],
                                      @"light": [NSString stringWithFormat:@" %0.1f",light]};
        
        if (!webapi)webapi = [[WebserviceApi alloc]init];
        webapi.delegate = self;
        NSLog(@"dataDic %@",requestDic);
    
        [webapi sendData:requestDic];
}
-(NSString *)getCurrentDateTime{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:[NSDate date]];
}
-(void)DidReceiveSendSensorDatainfo:(id)respond{
    NSLog(@"sendData insomming = %@",respond);
}
-(NSArray *)getStoredWheatherData{
    if (!dbManager)dbManager = [[DBManager alloc]init];
    NSArray * weatherData = [dbManager getTemperatureData];
    NSMutableArray * bundleArray = [[NSMutableArray alloc]init];
    [bundleArray removeAllObjects];
    
    for (Temperature * temperature in weatherData) {
//        NSString * datetime = [NSString stringWithFormat:@"%@ %@",temperature.date,temperature.time];
        NSDictionary * weatherdic = @{@"temperature":[NSString stringWithFormat:@"%0.2f",temperature.temperature.floatValue],
                                      @"humidity":[NSString stringWithFormat:@"%0.2f",temperature.humidity.floatValue],
                                      @"pressure" : @"0.0",
                                      @"light" : @"0.0",
                                      @"dateTime":[self getDateTimeString:temperature.date]};
        [bundleArray addObject:weatherdic];
    }
    
    if (bundleArray.count > 0) {
        return bundleArray;
    }
    return nil;
}
-(NSString *)getDateTimeString :(NSDate *)mydate{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:mydate];
}
-(NSString *)stringFromArray :(NSArray *)stations{
    NSString* json = nil;
    
    NSError* error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:stations options:NSJSONWritingPrettyPrinted error:&error];
    json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return json;
}
-(void)setupMessage :(Temperature *)start End:(Temperature *)end{
    
}
@end
