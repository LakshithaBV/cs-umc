//
//  BTDeviceManager.h
//  CS-UMC
//
//  Created by SCIT on 1/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "MQTTIBMQuickStart.h"
#import "bleGenericService.h"
#import "AppUserDefaults.h"
#import "masterUUIDList.h"

#import "DBManager.h"

#import "sensorTagAmbientTemperatureService.h"
#import "sensorTagAirPressureService.h"
#import "sensorTagHumidityService.h"
#import "sensorTagMovementService.h"
#import "sensorTagLightService.h"
#import "sensorTagKeyService.h"
#import "deviceInformationService.h"
#import "sensorMasterDataService.h"
#import "BackgroundTaskManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "WebserviceApi.h"
#import "Temperature+CoreDataClass.h"

@protocol BTDeviceManagerDelegate <NSObject>

@optional
-(void) getIsBlueToothOn :(BOOL)isAvailable;
-(void) getIsBlueToothDeviceConnected :(BOOL)isAvailable :(CBPeripheral *)device;
-(void) didSelectedDeviceFromSearch :(NSArray *)devices;
-(void) didDisconnectDevice: (CBPeripheral *)device;
//-(void) didgetConnectedPeriparalStatus :(BOOL)isConnected;
-(void) activeBTStream :(NSDictionary *)postString;
-(void) didFinishRetriveData :(BOOL)isSuccess :(NSArray *)historyData;

@end

@interface BTDeviceManager : NSObject <CBCentralManagerDelegate,CBPeripheralDelegate,bluetoothHandlerDelegate,webservicedelegate>{
    
    CBCharacteristic * currentDataBundle;
    CBCharacteristic * LoggedDataBundle;
    CBCharacteristic * historydataDataBundle;
    CBCharacteristic * measurements;
    CBCharacteristic * savedValue;
    CBCharacteristic * requestHistoryValue;
    
    DBManager * dbManager;
    WebserviceApi * webapi;
    
    CBPeripheral * currentPeripheral;
    
    NSString * messageString;
    int indexValue;
    int timeDelay;
    BOOL isAvailabilityCheckDone;
    
    NSMutableArray * temp_Store;
    NSMutableArray * hum_Store;
    
    NSMutableArray * sensorHistoryData;
}
+ (BTDeviceManager *)sharedInstance;
-(void)startSearching;
- (void)checkBluetoothAccess ;
-(void)connect;
-(void)disconnect;
-(NSArray *)getStoredWheatherData;
-(NSString *)stringFromArray :(NSArray *)stations;
-(void)retriveSensorData;
-(void)resetSensorByController;

@property id<BTDeviceManagerDelegate> delegate;
@property (nonatomic, strong) CBCentralManager *manager;
@property (nonatomic, strong) CBPeripheral *currentPeripheral;
@property (nonatomic, strong) CBCharacteristic *characteristic;
@property NSMutableArray *discoveredDevices;
@property NSUUID *currentlySelectedDeviceIdentifier;
@property (nonatomic)BOOL isStatusCheck;

@property bluetoothHandler *handler;
@property MQTTIBMQuickStart *cloudHandle;
@property NSString *MQTTStringLive;
@property NSString *MQTTStringTX;
@property NSMutableDictionary * MQTTDictionaryTX;
@property NSTimer *MQTTTimer;

@property NSMutableArray *services;
@property NSMutableArray *displayTiles;

@property (nonatomic) BackgroundTaskManager * bgTask;
@end
