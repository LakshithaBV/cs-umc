//
//  Temperature+CoreDataClass.h
//  
//
//  Created by SCIT on 8/31/18.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Temperature : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Temperature+CoreDataProperties.h"
