
//
//  AppDelegate.m
//  Simplefire
//
//  Created by SCIT on 6/13/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import "DBManager.h"
#import "Constants.h"
#import "Temperature+CoreDataClass.h"


@implementation DBManager

- (id)init
{
    self = [super init];
    if (self) {
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
        managedObjectContext = [self managedObjectContext];
    }
    return self;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(persistentContainer)]) {
        context = [delegate persistentContainer].viewContext;
    }
    return context;
}
#pragma mark -
#pragma mark TEMPERATURE

-(BOOL)storeTemprature :(float)temperature :(float)humidity :(float)time :(NSDate *)date :(NSString *)currentDateTime{
    
    Temperature * temperature_entity = [NSEntityDescription insertNewObjectForEntityForName:@"Temperature" inManagedObjectContext:managedObjectContext];
    
    if ((currentDateTime.length != 0)&&(currentDateTime != nil)) {
        temperature_entity.date = date;
        temperature_entity.time = [NSNumber numberWithDouble:time];
        temperature_entity.temperature = [NSNumber numberWithDouble:temperature];
        temperature_entity.humidity = [NSNumber numberWithDouble:humidity];
        temperature_entity.dateTime = currentDateTime;
        
        NSError *error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            return false;
        }else {
            //        [[NSNotificationCenter defaultCenter]postNotificationName:TEMPRETURE_SAVED_NOTITFICATIONS object:self];
            return true;
        }
    }else{
        return false;
    }
    

}
-(NSArray *)getTemperatureData {
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Temperature"];
    NSError *error;
    NSArray * mutableFetchResults = [[managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        return nil;
    }else{
        if (mutableFetchResults.count > 0) {
            return mutableFetchResults;
        }
    }
    return nil;
}
-(void)deleteAllRecords
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Temperature"];
    NSError *error;
    NSArray * mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    
    for (NSManagedObject * items in mutableFetchResults) {
        [context deleteObject:items];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}
@end
