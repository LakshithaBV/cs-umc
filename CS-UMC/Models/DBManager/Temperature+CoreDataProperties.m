//
//  Temperature+CoreDataProperties.m
//  
//
//  Created by SCIT on 8/31/18.
//
//

#import "Temperature+CoreDataProperties.h"

@implementation Temperature (CoreDataProperties)

+ (NSFetchRequest<Temperature *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Temperature"];
}

@dynamic date;
@dynamic dateTime;
@dynamic humidity;
@dynamic index;
@dynamic temperature;
@dynamic time;

@end
