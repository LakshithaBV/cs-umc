//
//  AppDelegate.m
//  Simplefire
//
//  Created by SCIT on 6/13/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"



@class AppDelegate;

@interface DBManager : NSObject{
    AppDelegate *appDelegate;
    
    NSManagedObjectContext          *managedObjectContext;
}

-(BOOL)storeTemprature :(float)temperature :(float)humidity :(float)time :(NSDate *)date :(NSString *)currentDateTime;
-(NSArray *)getTemperatureData;
-(void)deleteAllRecords;

@end
