//
//  Temperature+CoreDataProperties.h
//  
//
//  Created by SCIT on 8/31/18.
//
//

#import "Temperature+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Temperature (CoreDataProperties)

+ (NSFetchRequest<Temperature *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSString *dateTime;
@property (nullable, nonatomic, copy) NSNumber *humidity;
@property (nullable, nonatomic, copy) NSNumber *index;
@property (nullable, nonatomic, copy) NSNumber *temperature;
@property (nullable, nonatomic, copy) NSNumber *time;

@end

NS_ASSUME_NONNULL_END
