//
//  MKGradientCircleRenderer.m
//  TestProject_Objc
//
//  Created by SCIT on 12/6/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "MKGradientCircleRenderer.h"

@implementation MKGradientCircleRenderer

- (void)fillPath:(CGPathRef)path inContext:(CGContextRef)context
{
    
    MKCustomCircle * circle = self.overlay;
    
    CGRect rect = CGPathGetBoundingBox(path);
    
    CGContextAddPath(context, path);
    CGContextClip(context);
//    NSLog(@"circleTag = >>>>>>>>>> %@",circle.groupTag);
    
    
    if ([circle.groupTag isEqualToString:@"1"]){
        CGFloat gradientLocations[2] = {1.0f, 0.6f};
        static const CGFloat colors [] = {
            1.0, 1.0, 1.0, 0.10,  //up
            0.0, 0.60, 0.84, 0.50,
           0.28, 0.74, 0.92, 1.0,
           0.42, 0.81, 0.96, 1.0   //down
        };
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, gradientLocations, 4);
        CGColorSpaceRelease(colorSpace);
        CGPoint gradientCenter = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
        CGFloat gradientRadius = MIN(rect.size.width, rect.size.height) / 2;
        
        CGContextDrawRadialGradient(context, gradient, gradientCenter, 0, gradientCenter, gradientRadius, kCGGradientDrawsAfterEndLocation);
        
        CGGradientRelease(gradient);
    }else if ([circle.groupTag isEqualToString:@"2"]){
        CGFloat gradientLocations[2] = {1.0f, 0.6f};
        static const CGFloat colors [] = {
            0.53, 0.62, 0.46, 0.10,  //up
            0.64, 0.76, 0.54, 0.50,
            0.60, 0.71, 0.51, 1.0,
            0.55, 0.87, 0.96, 1.0   //down
        };
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, gradientLocations, 4);
        CGColorSpaceRelease(colorSpace);
        CGPoint gradientCenter = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
        CGFloat gradientRadius = MIN(rect.size.width, rect.size.height) / 2;
        
        CGContextDrawRadialGradient(context, gradient, gradientCenter, 0, gradientCenter, gradientRadius, kCGGradientDrawsAfterEndLocation);
        
        CGGradientRelease(gradient);
    }else if ([circle.groupTag isEqualToString:@"3"]){
        CGFloat gradientLocations[2] = {1.0f, 0.6f};
        static const CGFloat colors [] = {
            1.0, 0.85, 0.32, 0.10,  //up
            1.0, 0.89, 0.38, 0.50,
            0.99, 0.95, 0.51, 1.0,
            0.89, 0.89, 0.52, 1.0   //down
        };
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, gradientLocations, 4);
        CGColorSpaceRelease(colorSpace);
        CGPoint gradientCenter = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
        CGFloat gradientRadius = MIN(rect.size.width, rect.size.height) / 2;
        
        CGContextDrawRadialGradient(context, gradient, gradientCenter, 0, gradientCenter, gradientRadius, kCGGradientDrawsAfterEndLocation);
        
        CGGradientRelease(gradient);
    }else if ([circle.groupTag isEqualToString:@"4"]){
        CGFloat gradientLocations[2] = {1.0f, 0.6f};
        static const CGFloat colors [] = {
            1.0, 0.47, 0.09, 0.10,  //up
            0.99, 0.74, 0.11, 0.50,
            1.0, 0.81, 0.24, 1.0,
            1.0, 0.85, 0.32, 1.0   //down
        };
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, gradientLocations, 4);
        CGColorSpaceRelease(colorSpace);
        CGPoint gradientCenter = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
        CGFloat gradientRadius = MIN(rect.size.width, rect.size.height) / 2;
        
        CGContextDrawRadialGradient(context, gradient, gradientCenter, 0, gradientCenter, gradientRadius, kCGGradientDrawsAfterEndLocation);
        
        CGGradientRelease(gradient);
    }else if ([circle.groupTag isEqualToString:@"5"]){
        CGFloat gradientLocations[2] = {1.0f, 0.6f};
        static const CGFloat colors [] = {
            0.79, 0.13, 0.14, 0.10,  //up
            0.79, 0.13, 0.14, 0.50,
            0.97, 0.44, 0.13, 1.0,
            1.0, 0.47, 0.09, 1.0   //down
        };
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace, colors, gradientLocations, 4);
        CGColorSpaceRelease(colorSpace);
        CGPoint gradientCenter = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
        CGFloat gradientRadius = MIN(rect.size.width, rect.size.height) / 2;
        
        CGContextDrawRadialGradient(context, gradient, gradientCenter, 0, gradientCenter, gradientRadius, kCGGradientDrawsAfterEndLocation);
        
        CGGradientRelease(gradient);
    }
}

@end
