//
//  MKGradientCircleRenderer.h
//  TestProject_Objc
//
//  Created by SCIT on 12/6/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "MKCustomCircle.h"

NS_ASSUME_NONNULL_BEGIN

@interface MKGradientCircleRenderer : MKCircleRenderer

@end

NS_ASSUME_NONNULL_END
