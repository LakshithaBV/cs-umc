//
//  MKCustomCircle.h
//  CS-UMC
//
//  Created by SCIT on 12/6/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MKCustomCircle : MKCircle

@property (nonatomic, copy) NSString *groupTag;

@end


NS_ASSUME_NONNULL_END
