//
//  Constants.h
//  Customer
//
//  Created by Lakshitha on 26/11/2014.
//  Copyright (c) 2014 User. All rights reserved.
//  Final Vertion 2015/1/18

#import <Foundation/Foundation.h>

#ifndef Cashier_Constants_h
#define Cashier_Constants_h


#define kOFFSET_FOR_KEYBOARD 120.0

//colors
#define myColor colorWithRed:210.0/255.0 green:57.0/255.0 blue:48.0/255.0 alpha:1.0
#define menutextColor colorWithRed:149.0/255.0 green:149.0/255.0 blue:149.0/255.0 alpha:1.0

#define MYSENSOR_COLOR colorWithRed:1.00 green:0.00 blue:0.00 alpha:1.0
#define FIRSTSTATION_COLOR colorWithRed:0.38 green:0.81 blue:0.96 alpha:1.0
#define SECONDSTATION_COLOR colorWithRed:1.00 green:0.97 blue:0.00 alpha:1.0


//RegX
#define REGEX_USER_NAME_LIMIT                          @"^.{1,100}$"
#define REGEX_ADDRESS_LIMIT                            @"^.{10,100}$"
#define REGEX_USER_NAME                                @"[A-Za-z0-9 ]*"
#define REGEX_EMAIL                                    @"[A-Z0-9a-z._%+-]{1,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define REGEX_PASSWORD_LIMIT                           @"^.{1,20}$"
#define REGEX_PASSWORD                                 @"[A-Za-z0-9]{6,20}"
#define REGEX_PHONE_DEFAULT                            @"[0-9]{3}\\-[0-9]{3}\\-[0-9]{4}"
//#define REGEX_PHONE_DEFAULT                            @"\\b[\\d]{3}\\-[\\d]{3}\\-[\\d]{4}\\b"

//New Server

#define SERVICEURL                                      @"http://35.166.86.89/api/" //  WebService URL.

//Alerts

#define INVALIDEMAIL                                    @"Invalid email address"  // EMAIL validation
#define NOINTERNET                                      @"No internet access.Check your network and try again."  // Internet validation.
#define NOSENSORDATA                                    @"No data record yet, wait at least 5 min to get a measurement."  // DATA validation.

//NotificationHeaders

#define TEMPRETURE_SAVED_NOTITFICATIONS                 @"Tempreture_notifications"
#define FROMTODATESET_NOTITFICATIONS                    @"FromToDate_notifications"
#define REGISTRATION_CHECK_NOTITFICATIONS               @"registrationcheck_notifications"
#define LOGOUT_NOTITFICATIONS                           @"logout_notifications"
#define TIMERRESET_NOTITFICATIONS                       @"TimerReset_notifications"
#define APPACTIVE_NOTITFICATIONS                       @"TimerReset_notifications"

//Conditions
#define TEMPRETURE                                     @"Tempretures_data"
#define HUMIDITY                                       @"Humidity_data"



#define SAMPLE_SERVICE        @"00000000-0000-0000-0000-000000000001"

#define NOTIFY_CHARACTERISTIC @"00000000-0000-0000-0000-000000000002"
#define WRITE_CHARACTERISTIC  @"00000000-0000-0000-0000-000000000003"

#endif
