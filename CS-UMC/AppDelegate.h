//
//  AppDelegate.h
//  CS-UMC
//
//  Created by SCIT on 1/3/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "WebserviceApi.h"
#import "AppUserDefaults.h"

@import GoogleMaps;
@interface AppDelegate : UIResponder <UIApplicationDelegate>{
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (NSPersistentContainer *)persistentContainer ;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end

