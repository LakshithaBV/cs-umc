//
//  HelpViewController.m
//  CS-UMC
//
//  Created by SCIT on 6/14/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "HelpViewController.h"
#import "SeachDeviceViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoLogout)
                                                 name:LOGOUT_NOTITFICATIONS
                                               object:nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark Auto logout with device disconnect

-(void)autoLogout {
    NSLog(@"HelpViewController");
    [[BTDeviceManager sharedInstance] disconnect];
    SeachDeviceViewController * searchdevice = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
    [self.navigationController pushViewController:searchdevice animated:true];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
@end
