//
//  HelpViewController.h
//  CS-UMC
//
//  Created by SCIT on 6/14/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "MasterViewController.h"

@interface HelpViewController : MasterViewController{
     __weak IBOutlet UIButton *btnMenu;
    
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
}

@end
