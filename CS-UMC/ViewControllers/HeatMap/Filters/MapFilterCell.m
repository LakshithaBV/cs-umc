//
//  MapFilterCell.m
//  CS-UMC
//
//  Created by SCIT on 7/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "MapFilterCell.h"

@implementation MapFilterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected) {
        self.contentView.backgroundColor = UIColor.whiteColor;
    } else {
        self.contentView.backgroundColor = UIColor.whiteColor;
    }
}

@end
