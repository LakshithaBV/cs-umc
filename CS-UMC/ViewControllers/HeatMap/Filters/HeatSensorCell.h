//
//  HeatSensorCell.h
//  CS-UMC
//
//  Created by SCIT on 7/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeatSensorCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *colorIV_1;
@property (weak, nonatomic) IBOutlet UIImageView *colorIV_2;
@property (weak, nonatomic) IBOutlet UILabel *tempretureTV;
@property (weak, nonatomic) IBOutlet UIImageView *checkBoxIV;
@end
