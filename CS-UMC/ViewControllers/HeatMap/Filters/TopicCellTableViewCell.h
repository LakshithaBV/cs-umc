//
//  TopicCellTableViewCell.h
//  CS-UMC
//
//  Created by SCIT on 10/12/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameTL;
@end
