//
//  MapFilterCell.h
//  CS-UMC
//
//  Created by SCIT on 7/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapFilterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameTL;
@property (weak, nonatomic) IBOutlet UIImageView *checkBox;
@end
