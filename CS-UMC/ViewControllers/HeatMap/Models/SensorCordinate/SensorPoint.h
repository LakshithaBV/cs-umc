//
//  SensorPoint.h
//  CS-UMC
//
//  Created by SCIT on 7/9/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SensorPoint : NSObject

@property (nonatomic,retain)NSNumber * deviceId;
@property (nonatomic,retain)NSString * deviceName;
@property (nonatomic,retain)NSNumber * latitude;
@property (nonatomic,retain)NSNumber * longitude;
@property (nonatomic,retain)NSNumber * location_id;
@property (nonatomic,retain)NSString * sensor_id;
@property (nonatomic,retain)NSString * created_at;
@property (nonatomic,retain)NSNumber * sensor_type;
@property (nonatomic,retain)NSNumber * temperature;
@property (nonatomic,retain)NSNumber * humidity;
@property (nonatomic,retain)NSNumber * area_type;
@property (nonatomic,retain)NSNumber * environment_id;

-(SensorPoint *)initWithDataDictionary :(NSDictionary *)dataDic;
@end
