//
//  SensorPoint.m
//  CS-UMC
//
//  Created by SCIT on 7/9/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "SensorPoint.h"

@implementation SensorPoint
@synthesize deviceId;
@synthesize deviceName;
@synthesize latitude;
@synthesize longitude;
@synthesize location_id;
@synthesize sensor_id;
@synthesize created_at;
@synthesize sensor_type;
@synthesize temperature;
@synthesize humidity;
@synthesize area_type;
@synthesize environment_id;

-(SensorPoint *)initWithDataDictionary :(NSDictionary *)dataDic {
    if(self = [super init]){//created_at
        deviceId = [NSNumber numberWithInt:[[dataDic objectForKey:@"device_id"] intValue]];
        sensor_id = [dataDic objectForKey:@"sensor_id"];
        deviceName = [dataDic objectForKey:@"device_name"];
        created_at = [self pinCreatedTime:[dataDic objectForKey:@"created_at"]];
        latitude = [NSNumber numberWithFloat:[[dataDic objectForKey:@"latitude"] floatValue]];
        longitude = [NSNumber numberWithFloat:[[dataDic objectForKey:@"longitude"] floatValue]];
        location_id = [NSNumber numberWithInt:[[dataDic objectForKey:@"location_id"] intValue]];
        sensor_type = [NSNumber numberWithInt:[[dataDic objectForKey:@"sensor_type"] intValue]];
        temperature = [NSNumber numberWithFloat:[[dataDic objectForKey:@"temperature"] floatValue]];
        humidity = [NSNumber numberWithFloat:[[dataDic objectForKey:@"humidity"] floatValue]];
        area_type = [NSNumber numberWithInt:[[dataDic objectForKey:@"area_type_id"] intValue]];
        environment_id = [NSNumber numberWithInt:[[dataDic objectForKey:@"environment_id"] intValue]];
    }
    return self;
}
-(NSString *)pinCreatedTime :(NSString *)createdTime{
   
    if (createdTime != nil) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate * dateTime = [dateFormatter dateFromString:createdTime];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm a"];
        NSString *formattedDate = [dateFormatter stringFromDate:dateTime];
        return formattedDate;
    }
    
    return @"";
}
@end
