//
//  HeatMapAnotationView.h
//  GoogleMap
//
//  Created by SCIT on 7/3/18.
//  Copyright © 2018 Lakshitha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterSubClassView.h"

@interface HeatMapAnotationView : MasterSubClassView

@property (weak, nonatomic) IBOutlet UILabel *stationNameTL;
@property (weak, nonatomic) IBOutlet UILabel *tempretureTL;
@property (weak, nonatomic) IBOutlet UILabel *humidityTL;

@end
