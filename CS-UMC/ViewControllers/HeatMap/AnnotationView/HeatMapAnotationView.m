
//  HeatMapAnotationView.m
//  GoogleMap
//
//  Created by SCIT on 7/3/18.
//  Copyright © 2018 Lakshitha. All rights reserved.
//

#import "HeatMapAnotationView.h"

@implementation HeatMapAnotationView

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self setup];
    }
    return self;
}
- (void)setup {
//    NSLog(@"Subclass");
}
- (IBAction)btnCloseClicks:(id)sender {
    [self removeFromSuperview];
}

@end
