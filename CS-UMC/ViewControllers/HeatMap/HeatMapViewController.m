//
//  HeatMapViewController.m
//  CS-UMC
//
//  Created by SCIT on 5/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "HeatMapViewController.h"
#import "MapFilterCell.h"
#import "SeachDeviceViewController.h"
#import "MKCustomCircle.h"

static NSString *const kTYPE1 = @"Banana";
static NSString *const kTYPE2 = @"Orange";
static CGFloat kDEFAULTCLUSTERSIZE = 0.1;

static  NSString * TOPIC = @"topic";
static  NSString * EVT1 = @"environmentType1";
static  NSString * EVT2 = @"environmentType2";
static  NSString * HEATCELL = @"heatcell";



@implementation HeatMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //Designs
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    [filterView setHidden:true];
    self.mapView.delegate = self;
    self.mapView.clusterSize = kDEFAULTCLUSTERSIZE;
    filterTL.text = @"No filter applied";
    filterMiddleView.layer.cornerRadius = 5.0f;
    filterMiddleView.layer.masksToBounds = true;
    
    filterDisplayView.layer.cornerRadius = 2.0f;
    filterDisplayView.layer.masksToBounds = true;
    
    btnMapRefres.layer.cornerRadius = btnMapRefres.frame.size.width/2;
    btnMapRefres.layer.masksToBounds = true;
    
    [self getAllMapPoints];
    //    [self testWithLocalPoints];
    
    //FilterData
    masterDataArray = [[NSMutableArray alloc]init];
    selectedDataArray = [[NSMutableArray alloc]init];
    
    firstFilterDataArray = [[NSMutableArray alloc]initWithObjects:@"Area Type",@"Environment Type",@"Heat Range", nil];
    environmentArray_1 = [[NSMutableArray alloc]init];
    heatRangeArray = [[NSMutableArray alloc]init];
    
    [self getLocationAttributeData];
    
    //    LatalongBundle * bundles = [[LatalongBundle alloc]init];
    //    NSArray * points =  [bundles genarateLatlongbundle:50];
    //    NSLog(@"pointsArray %@",points);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoLogout)
                                                 name:LOGOUT_NOTITFICATIONS
                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}

// Reload data
- (IBAction)btnLocationRefreshClicks:(id)sender {
    //    [self loadHeatMap];
    [annotationsToAdd removeAllObjects];
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    for (UIView * view in self.mapView.subviews) {
        if ([view isKindOfClass:[SensorPoint class]]) {
            [view removeFromSuperview];
        }
    }
    
    
    //    NSMutableArray * annotationsToRemove = [ self.mapView.annotations mutableCopy ] ;
    //    [ annotationsToRemove removeObject:self.mapView.userLocation ] ;
    //    [ self.mapView removeAnnotations:annotationsToRemove ];
    
}
#pragma mark-
#pragma mark webservice Map Pints Load
-(void)loadHeatMap :(NSDictionary *)params{
    
    [self inithud:@"Appling filter"];
    if (!webApi)webApi = [[WebserviceApi alloc]init];
    webApi.delegate = self;
    if ([self ReachabilityPass]) {
        [webApi getHeatMapLocations:params];
    }else{
        [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
    }
}
-(void)getAllMapPoints{
    [self inithud:@"Loading map data"];
    
    NSDictionary * dataDic = @{@"area_type": @"[]",
                               @"environment_type" : @"[]",
                               @"heat_range" :@"[]"};
    
    if (!webApi)webApi = [[WebserviceApi alloc]init];
    webApi.delegate = self;
    if ([self ReachabilityPass]) {
        [webApi getHeatMapLocations:dataDic];
    }else{
        [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
    }
}
-(void)testWithLocalPoints{
    
    if (!mapPoints)mapPoints = [[NSMutableArray alloc]init];
    [mapPoints removeAllObjects];
    LatalongBundle * bundles = [[LatalongBundle alloc]init];
    NSArray * points =  [bundles genarateLatlongbundle:50];
    
    for (NSDictionary * dataDic in points) {
        SensorPoint * point = [[SensorPoint alloc]initWithDataDictionary:dataDic];
        [mapPoints addObject:point];
    }
    [self loadForCategory];
    [self loadHeatContionAnnotations:mapPoints];
}

-(void)createJsonObjectForRequest{
    //    NSMutableArray * totalpoints = [[NSMutableArray alloc]init];
    NSMutableArray * heatRangesJsonArra = [[NSMutableArray alloc]init];
    
    NSString * areaTypeJson = [self stringFromArray:areaTypesArray];
    NSString * environmentTypeJson = [self stringFromArray:environmentTypesArray];
    
    if (heatRangeArray.count > 0) {
        for (NSDictionary * rangeDic in heatRangeArray) {
            NSString * rangeStringData = [self getHeatRangeStringForJson:[[rangeDic objectForKey:@"index"] intValue]];
            [heatRangesJsonArra addObject:rangeStringData];
        }
    }
    NSString * heatRangeJson = [self stringFromArray:heatRangesJsonArra];
    
    NSDictionary * dataDic = @{@"area_type" : areaTypeJson,
                               @"environment_type" :environmentTypeJson,
                               @"heat_range" :heatRangeJson };
    [self loadHeatMap:dataDic];
}
-(NSString *)stringFromArray :(NSArray *)stations{
    NSString* json = nil;
    if (stations == nil) {
        return @"[]";
    }else{
        NSError* error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:stations options:NSJSONWritingPrettyPrinted error:&error];
        json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        return json;
    }
    
    
}
-(void)DidReceiveHeatMapLocationData:(id)respond {
    [self hudHide];
    
    NSArray * respondArray = [respond objectForKey:@"data"];
    if (respondArray.count > 0) {
        if (!mapPoints)mapPoints = [[NSMutableArray alloc]init];
        [mapPoints removeAllObjects];
        
        for (NSDictionary * dataDic in respondArray) {
            SensorPoint * point = [[SensorPoint alloc]initWithDataDictionary:dataDic];
            [mapPoints addObject:point];
        }
    }else{
        [self ShowAlert:@"Sorry!" message:@"No map points."];
        [mapPoints removeAllObjects];
    }
    //   [self loadForCategory];
    [self loadHeatContionAnnotations:mapPoints];
    NSLog(@"Respond HeatmapData %@",respond);
}
-(void)checkContainer{
    if (!firstRangeAnotations)firstRangeAnotations = [[NSMutableArray alloc]init];
    [firstRangeAnotations removeAllObjects];
    if (!secondRangeAnotations)secondRangeAnotations = [[NSMutableArray alloc]init];
    [secondRangeAnotations removeAllObjects];
    if (!thredRangeAnotations)thredRangeAnotations = [[NSMutableArray alloc]init];
    [thredRangeAnotations removeAllObjects];
    if (!forthRangeAnotations)forthRangeAnotations = [[NSMutableArray alloc]init];
    [forthRangeAnotations removeAllObjects];
    if (!fifthRangeAnotations)fifthRangeAnotations = [[NSMutableArray alloc]init];
    [fifthRangeAnotations removeAllObjects];
    if (!sixthRangeAnotations)sixthRangeAnotations = [[NSMutableArray alloc]init];
    [sixthRangeAnotations removeAllObjects];
}
-(void)loadForCategory{
    [self checkContainer];
    for (SensorPoint * point in mapPoints) {
        float heat = point.temperature.floatValue;
        
        switch ([self validateTempreture:heat]) {
            case 1:
                [firstRangeAnotations addObject:point];
                break;
            case 2:
                [secondRangeAnotations addObject:point];
                break;
            case 3:
                [thredRangeAnotations addObject:point];
                break;
            case 4:
                [forthRangeAnotations addObject:point];
                break;
            case 5:
                [fifthRangeAnotations addObject:point];
                break;
            case 6:
                [sixthRangeAnotations addObject:point];
                break;
                
            default:
                break;
        }
    }
}
-(int)validateTempreture :(float)temp {
    if ((0 < temp)&&(5 > temp)) {
        return 1;
        
    }else if ((5 < temp)&&(15 > temp)) {
        return 2;
        
    }else if ((15 < temp)&&(25 > temp)) {
        return 3;
        
    }else if ((25 < temp)&&(40 > temp)) {
        return 4;
        
    }else if (40 < temp) {
        return 5;
    }
    return 6;
}

//-(void)map
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MKMapViewDelegate
-(void)loadAllAnnotations :(NSArray *)mapPoints{
    NSLog(@"Load_heat_map");
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self addAnotations:mapPoints];
    
    
    if (!allAnnotations)allAnnotations = [[NSMutableArray alloc] init];
    [allAnnotations removeAllObjects];
    
    for (SensorPoint *point in mapPoints) {
        CLLocation * location = [[CLLocation alloc]initWithLatitude:point.latitude.floatValue longitude:point.longitude.floatValue];
        OCMapViewSampleHelpAnnotation * annotation = [[OCMapViewSampleHelpAnnotation alloc] initWithCoordinate:location.coordinate];
        annotation.title = [NSString stringWithFormat:@"%@ (%@)",point.deviceName,point.created_at] ;
        annotation.subtitle = [NSString stringWithFormat:@"%0.2f °C, %0.2f %%",point.temperature.floatValue,point.humidity.floatValue];
        int pointTag = [self validateTempreture:point.temperature.floatValue];
        annotation.groupTag = [NSString stringWithFormat:@"%i",pointTag];
        [allAnnotations addObject:annotation];
    }
    NSLog(@"ANNNNNNNN--> %lu",(unsigned long)self.mapView.displayedAnnotations.count);
}

-(void)loadHeatContionAnnotations :(NSArray *)mapPoints{
    NSLog(@"Load_heat_map");
    [self.mapView removeOverlays:self.mapView.overlays];
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self addAnotations:mapPoints];
    
    
    if(!annotationsToAdd)annotationsToAdd = [[NSMutableSet alloc] init];
    [annotationsToAdd removeAllObjects];
    
    for (SensorPoint *point in mapPoints) {
        CLLocation * location = [[CLLocation alloc]initWithLatitude:point.latitude.floatValue longitude:point.longitude.floatValue];
        OCMapViewSampleHelpAnnotation * annotation = [[OCMapViewSampleHelpAnnotation alloc] initWithCoordinate:location.coordinate];
        annotation.title = [NSString stringWithFormat:@"%@ (%@)",point.deviceName,point.created_at] ;
        annotation.subtitle = [NSString stringWithFormat:@"%0.2f °C, %0.2f %%",point.temperature.floatValue,point.humidity.floatValue];
        int pointTag = [self validateTempreture:point.temperature.floatValue];
        annotation.groupTag = [NSString stringWithFormat:@"%i",pointTag];
        [annotationsToAdd addObject:annotation];
    }
    NSLog(@"ANNNNNNNN--> %lu",(unsigned long)self.mapView.displayedAnnotations.count);
}
- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    MKAnnotationView *annotationView;
    // if it's a cluster
    if ([annotation isKindOfClass:[OCAnnotation class]]) {
        
        OCAnnotation * clusterAnnotation = (OCAnnotation *)annotation;
        
        annotationView = (MKAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"ClusterView"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ClusterView"];
            annotationView.canShowCallout = YES;
            annotationView.centerOffset = CGPointMake(0, -20);
        }
        
        // set title
        //        clusterAnnotation.title = @"Cluster";
        //        clusterAnnotation.subtitle = [NSString stringWithFormat:@"Containing annotations: %zd", [clusterAnnotation.annotationsInCluster count]];
        // set its image
        annotationView.image = [UIImage imageNamed:@"regular.png"];
        
        // change pin image for group
        if (self.mapView.clusterByGroupTag) {
            if ([clusterAnnotation.groupTag isEqualToString:kTYPE1]) {
                //                annotationView.image = [UIImage imageNamed:@"bananas.png"];
                
            }
            else if([clusterAnnotation.groupTag isEqualToString:kTYPE2]){
                //                annotationView.image = [UIImage imageNamed:@"oranges.png"];
            }
            clusterAnnotation.title = clusterAnnotation.groupTag;
        }
    }
    // If it's a single annotation
    else if([annotation isKindOfClass:[OCMapViewSampleHelpAnnotation class]]){
        OCMapViewSampleHelpAnnotation * singleAnnotation = (OCMapViewSampleHelpAnnotation *)annotation;
        annotationView = (MKAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"singleAnnotationView"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:singleAnnotation reuseIdentifier:@"singleAnnotationView"];
            annotationView.canShowCallout = YES;
            annotationView.centerOffset = CGPointMake(0, -20);
        }
    }
    // Error
    else{
        annotationView = (MKPinAnnotationView *)[aMapView dequeueReusableAnnotationViewWithIdentifier:@"errorAnnotationView"];
        if (!annotationView) {
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"errorAnnotationView"];
            annotationView.canShowCallout = YES;
        }
    }
    return annotationView;
}
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    MKGradientCircleRenderer * circleView = [[MKGradientCircleRenderer alloc]initWithOverlay:overlay];
    return circleView;
}
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
    mapRegion = self.mapView.region;
}
-(void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated
{
    [self updateOverlays];
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views;
{
    [self updateOverlays];
}
- (void)updateOverlays
{
    
    if(self.mapView.overlays != nil){
        [self.mapView removeOverlays:self.mapView.overlays];
    }
    for (OCAnnotation *annotation in annotationsToAdd) {
        //        if ([annotation isKindOfClass:[OCAnnotation class]]) {
        //            NSLog(@"anotation Tag %@",annotation.groupTag);
        // static circle size of cluster
        CLLocationDistance clusterRadius = self.mapView.region.span.longitudeDelta * self.mapView.clusterSize * 111000 / 2.0f;
        clusterRadius = clusterRadius * cos([annotation coordinate].latitude * M_PI / 180.0);
        
        MKCustomCircle * circle = [MKCustomCircle circleWithCenterCoordinate:annotation.coordinate radius:clusterRadius];
        circle.groupTag = annotation.groupTag;
        [self.mapView addOverlay:circle];
        //        }
    }
}
-(void)addAnotations:(NSArray *)points{
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    for (SensorPoint * sensorpoint in points) {
        WKTPoint *p = (WKTPoint *)[WKTParser parseGeometry:@"POINT (-73.995 41.145556)"];
        MKPointAnnotation *point = [p toMapPointAnnotation];
        point.coordinate = CLLocationCoordinate2DMake(sensorpoint.latitude.floatValue, sensorpoint.longitude.floatValue);
        point.title = [NSString stringWithFormat:@"%@ (%@)",sensorpoint.deviceName,sensorpoint.created_at] ;
        point.subtitle = [NSString stringWithFormat:@"%0.2f °C, %0.2f %%",sensorpoint.temperature.floatValue,sensorpoint.humidity.floatValue];
        NSLog(@"Adding %@",sensorpoint.deviceName);
        [self.mapView addAnnotation:point];
    }
    
    [self zoomForPoints];
}
#pragma mark - Filter Details

- (IBAction)btnFilterClicks:(id)sender {
    topicTL.text = @"Select Filter";
    
    [filterView setHidden:false];
}
- (IBAction)btnFlterDoneClicks:(id)sender {
    [filterView setHidden:true];
    //    [self refreshMap];
    [self createJsonObjectForRequest];
}

-(void)mapLoaded{
    [self hudHide];
    
}
- (IBAction)btnFilterCancelClicks:(id)sender {
    [filterView setHidden:true];
    filterTL.text = @"No filter applied";
    [self getAllMapPoints];
}

-(void)zoomForPoints{
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in self.mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.8, 0.8);
        zoomRect = MKMapRectUnion(zoomRect, pointRect);
        NSLog(@"point = lat %f,lon %f",annotation.coordinate.latitude,annotation.coordinate.longitude);
    }
    [self.mapView setVisibleMapRect:zoomRect animated:YES];
}

-(void)getLocationAttributeData{
    webApi = [WebserviceApi sharedInstance];
    webApi.delegate = self;
    if ([self ReachabilityPass]) {
        [webApi getLocationAttribute];
    }else{
        [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
    }
}
-(void)DidReceiveLocationAttributes:(id)respond{
    NSLog(@"receivedData %@",respond);
    NSDictionary * dataDic = [respond objectForKey:@"data"];
    
    [environmentArray_1 removeAllObjects];
    environmentArray_1 = [[NSMutableArray alloc]initWithArray:[dataDic objectForKey:@"area_types"]];
    
    [environmentArray_2 removeAllObjects];
    environmentArray_2 = [[NSMutableArray alloc]initWithArray:[dataDic objectForKey:@"environments"]];
    
    [self masterTabelDetails];
}
#pragma mark-
#pragma mark Master Tabel Details
-(void)masterTabelDetails{
    masterDataArray = [[NSMutableArray alloc]init];
    [masterDataArray removeAllObjects];
    //Area types
    NSDictionary * topic_1 = @{@"type" : TOPIC,
                               @"name" : @"Area Types"};
    [masterDataArray addObject:topic_1];
    
    for (NSDictionary * areatype in environmentArray_1) {
        NSMutableDictionary * areaTypeDic = [[NSMutableDictionary alloc]initWithDictionary:areatype];
        [areaTypeDic setObject:EVT1 forKey:@"type"];
        [masterDataArray addObject:areaTypeDic];
    }
    
    //Environment types
    NSDictionary * topic_2 = @{@"type" : TOPIC,
                               @"name" : @"Environment Type"};
    [masterDataArray addObject:topic_2];
    
    for (NSDictionary * areatype in environmentArray_2) {
        NSMutableDictionary * areaTypeDic = [[NSMutableDictionary alloc]initWithDictionary:areatype];
        [areaTypeDic setObject:EVT2 forKey:@"type"];
        [masterDataArray addObject:areaTypeDic];
    }
    //Heat Range
    NSDictionary * topic_3 = @{@"type" : TOPIC,
                               @"name" : @"Heat Range"};
    [masterDataArray addObject:topic_3];
    
    for (int i = 1; i< 6; i++) {
        NSDictionary * heatIndex = @{@"type" : HEATCELL,
                                     @"index": [NSNumber numberWithInt:i],
                                     @"name" : @""};
        [masterDataArray addObject:heatIndex];
        
    }
    [filterTableView reloadData];
}
#pragma mark - Table Data

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return masterDataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * dataDic = [masterDataArray objectAtIndex:indexPath.row];
    
    if ([[dataDic objectForKey:@"type"] isEqualToString:TOPIC]){
        return 35.0;
    }
    return 40.0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary * dataDic = [masterDataArray objectAtIndex:indexPath.row];
    NSNumber * selectednumber = [NSNumber numberWithInteger:indexPath.row];
    if ([[dataDic objectForKey:@"type"] isEqualToString:TOPIC]){
        static NSString *simpleTableIdentifier = @"TopicCellTableViewCell";
        TopicCellTableViewCell *cell = (TopicCellTableViewCell *)[filterTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TopicCellTableViewCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.nameTL.text = [dataDic objectForKey:@"name"];
        
        return cell;
    } if ([[dataDic objectForKey:@"type"] isEqualToString:EVT1]){
        static NSString *simpleTableIdentifier = @"MapFilterCell";
        MapFilterCell *cell = (MapFilterCell *)[filterTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MapFilterCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        NSDictionary * dataDic = [masterDataArray objectAtIndex:indexPath.row];
        cell.nameTL.text = [dataDic objectForKey:@"name"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor whiteColor];
        cell.selectedBackgroundView = selectionColor;
        
        
        
        if ([selectedDataArray containsObject:selectednumber]) {
            cell.checkBox.image = [UIImage imageNamed:@"heat_map_checkbox"];
        }else{
            cell.checkBox.image = [UIImage imageNamed:@"heatmapcheckboxempty"];
        }
        
        return cell;
    }else if ([[dataDic objectForKey:@"type"] isEqualToString:EVT2]){
        static NSString *simpleTableIdentifier = @"MapFilterCell";
        MapFilterCell *cell = (MapFilterCell *)[filterTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MapFilterCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        NSDictionary * dataDic = [masterDataArray objectAtIndex:indexPath.row];
        cell.nameTL.text = [dataDic objectForKey:@"name"];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *selectionColor = [[UIView alloc] init];
        selectionColor.backgroundColor = [UIColor whiteColor];
        cell.selectedBackgroundView = selectionColor;
        
        
        
        if ([selectedDataArray containsObject:selectednumber]) {
            cell.checkBox.image = [UIImage imageNamed:@"heat_map_checkbox"];
        }else{
            cell.checkBox.image = [UIImage imageNamed:@"heatmapcheckboxempty"];
        }
        
        return cell;
    }else {
        static NSString *simpleTableIdentifier = @"HeatSensorCell";
        HeatSensorCell *cell = (HeatSensorCell *)[filterTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HeatSensorCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        int rowindex = [[dataDic objectForKey:@"index"] intValue];
        
        if (rowindex == 1) {
            cell.colorIV_1.image = [UIImage imageNamed:@"blue_1"];
            cell.colorIV_2.image = [UIImage imageNamed:@"blue_2"];
            cell.tempretureTV.text = @"0°C - 5°C";
            cell.checkBoxIV.image = [UIImage imageNamed:@"heatmapcheckboxempty"];
        }else if (rowindex == 2){
            cell.colorIV_1.image = [UIImage imageNamed:@"green_1"];
            cell.colorIV_2.image = [UIImage imageNamed:@"green_2"];
            cell.tempretureTV.text = @"6°C - 15°C";
            cell.checkBoxIV.image = [UIImage imageNamed:@"heatmapcheckboxempty"];
        }else if (rowindex == 3){
            cell.colorIV_1.image = [UIImage imageNamed:@"yellow_1"];
            cell.colorIV_2.image = [UIImage imageNamed:@"yellow_2"];
            cell.tempretureTV.text = @"16°C - 25°C";
            cell.checkBoxIV.image = [UIImage imageNamed:@"heatmapcheckboxempty"];
        }else if (rowindex == 4){
            cell.colorIV_1.image = [UIImage imageNamed:@"orange_1"];
            cell.colorIV_2.image = [UIImage imageNamed:@"orange_2"];
            cell.tempretureTV.text = @"26°C - 40°C";
            cell.checkBoxIV.image = [UIImage imageNamed:@"heatmapcheckboxempty"];
        }else if (rowindex == 5){
            cell.colorIV_1.image = [UIImage imageNamed:@"red_1"];
            cell.colorIV_2.image = [UIImage imageNamed:@"red_2"];
            cell.tempretureTV.text = @"41°C above";
            cell.checkBoxIV.image = [UIImage imageNamed:@"heatmapcheckboxempty"];
        }
        if ([selectedDataArray containsObject:selectednumber]) {
            cell.checkBoxIV.image = [UIImage imageNamed:@"heat_map_checkbox"];
        }
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber * selectednumber = [NSNumber numberWithInteger:indexPath.row];
    
    if ([selectedDataArray containsObject:selectednumber]) {
        [selectedDataArray removeObject:selectednumber];
    }else{
        [selectedDataArray addObject:selectednumber];
    }
    
    [self manageSelectedFilters];
    [filterTableView reloadData];
}
#pragma mark-
#pragma mark Heat Range select process.
-(void)manageSelectedFilters{
    
    [heatRangeArray removeAllObjects];
    NSString * heatRanges = @"";
    filterString = @"";
    if (!filteredPoints)filteredPoints = [[NSMutableArray alloc]init];
    [filteredPoints removeAllObjects];
    if(!areaTypesArray)areaTypesArray = [[NSMutableArray alloc]init];
    [areaTypesArray removeAllObjects];
    if(!environmentTypesArray)environmentTypesArray = [[NSMutableArray alloc]init];
    [environmentTypesArray removeAllObjects];
    
    
    for (NSNumber * selectednumber in selectedDataArray) {
        
        NSDictionary * dataDic = [masterDataArray objectAtIndex:selectednumber.intValue];
        if ([[dataDic objectForKey:@"type"] isEqualToString:EVT1]){
            
            filterString = [NSString stringWithFormat:@"%@ , %@",filterString,[dataDic objectForKey:@"name"]];
            [areaTypesArray addObject:[NSNumber numberWithInt:[[dataDic objectForKey:@"id"] intValue]]];
            
        }else if ([[dataDic objectForKey:@"type"] isEqualToString:EVT2]){
            filterString = [NSString stringWithFormat:@"%@ , %@",filterString,[dataDic objectForKey:@"name"]];
            [environmentTypesArray addObject:[NSNumber numberWithInt:[[dataDic objectForKey:@"id"] intValue]]];
            
        }else if ([[dataDic objectForKey:@"type"] isEqualToString:HEATCELL]){
            [heatRangeArray addObject:dataDic];
            heatRanges = [NSString stringWithFormat:@"%@, %@",heatRanges,[self getHeatRangeString:[[dataDic objectForKey:@"index"] intValue]]];
        }
    }
    NSLog(@"env1 = %@",filteredPoints);
    NSLog(@"heatRangeArray = %@",heatRangeArray);
    filterTL.text = [NSString stringWithFormat:@"Environments : %@. \nHeatRange : %@.",filterString,heatRanges];
    
}
-(void)refreshMap{
    NSMutableArray * filterdata = [[NSMutableArray alloc]init];
    
    for (NSDictionary * filterDic in heatRangeArray) {
        switch ([[filterDic objectForKey:@"index"] intValue]) {
            case 1:
                [filterdata addObjectsFromArray:firstRangeAnotations];
                break;
            case 2:
                [filterdata addObjectsFromArray:secondRangeAnotations];
                break;
            case 3:
                [filterdata addObjectsFromArray:thredRangeAnotations];
                break;
            case 4:
                [filterdata addObjectsFromArray:forthRangeAnotations];
                break;
            case 5:
                [filterdata addObjectsFromArray:fifthRangeAnotations];
                break;
            case 6:
                [filterdata addObjectsFromArray:sixthRangeAnotations];
                break;
                
            default:
                break;
        }
    }
    [self loadHeatContionAnnotations:filterdata];
}


-(NSString *)getHeatRangeString : (int)selectedIndex{
    NSString * rangeString = nil;
    switch (selectedIndex) {
        case 1:
            rangeString = rangeString? [NSString stringWithFormat:@"0°C - 5°C and %@",rangeString]:@"0°C - 5°C";
            break;
        case 2:
            rangeString = rangeString? [NSString stringWithFormat:@"6°C - 15°C and %@",rangeString]:@"6°C - 15°C";
            break;
        case 3:
            rangeString = rangeString? [NSString stringWithFormat:@"16°C - 25°C and %@",rangeString]:@"16°C - 25°C";
            break;
        case 4:
            rangeString = rangeString? [NSString stringWithFormat:@"26°C - 40°C and %@",rangeString]:@"26°C - 40°C";
            break;
        case 5:
            rangeString = rangeString? [NSString stringWithFormat:@"41°C above and %@",rangeString]:@"41°C above";
            break;
            
        default:
            break;
    }
    return rangeString;
}
-(NSString *)getHeatRangeStringForJson : (int)selectedIndex{
    NSString * rangeString = nil;
    switch (selectedIndex) {
        case 1:
            rangeString = [NSString stringWithFormat:@"0 - 5"];
            break;
        case 2:
            rangeString = [NSString stringWithFormat:@"6 - 15"];
            break;
        case 3:
            rangeString = [NSString stringWithFormat:@"16 - 25"];
            break;
        case 4:
            rangeString = [NSString stringWithFormat:@"26 - 40"];
            break;
        case 5:
            rangeString = [NSString stringWithFormat:@"41 - 100"];
            break;
            
        default:
            break;
    }
    return rangeString;
}
#pragma mark -
#pragma mark Auto logout with device disconnect

-(void)autoLogout {
    NSLog(@"Logout_4");
    [[BTDeviceManager sharedInstance] disconnect];
    SeachDeviceViewController * searchdevice = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
    [self.navigationController pushViewController:searchdevice animated:true];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
