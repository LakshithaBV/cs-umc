//
//  HeatMapViewController.h
//  CS-UMC
//
//  Created by SCIT on 5/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "WKTPointM.h"
#import "WKTParser.h"

#import "MasterViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "GMUHeatmapTileLayer.h"
#import "GMUWeightedLatLng.h"
#import "WebserviceApi.h"
#import "HeatMapAnotationView.h"
#import "HeatSensorCell.h"
#import "SensorPoint.h"
#import <MapKit/MapKit.h>
#import "LatalongBundle.h"
#import "CSUMCColor.h"
#import "TopicCellTableViewCell.h"

#import "OCMapView.h"
#import "OCMapViewSampleHelpAnnotation.h"
#import "MKGradientCircleRenderer.h"

@interface HeatMapViewController : MasterViewController <webservicedelegate,GMSMapViewDelegate,UITableViewDelegate,MKMapViewDelegate,UIPopoverControllerDelegate>{
    __weak IBOutlet UIButton *btnMenu;
    
    //    GMSMapView *_mapView;
    WebserviceApi * webApi;
    NSMutableArray * mapPoints;
    
    __weak IBOutlet UIView *mapBackView;
    __weak IBOutlet UIView *btnMapRefres;
    
    
    
    
    //Filters
    int filterIndex;
    int selectedIndex;
    int selectedTempEnvIndex;
    int selectedFinalIndex;
    BOOL isTempretureSelected;
    
    __weak IBOutlet UIView *filterView;
    __weak IBOutlet UIView *filterMiddleView;
    __weak IBOutlet UITableView *filterTableView;
    __weak IBOutlet UILabel *topicTL;
    
    //FilterView Constraint
    __weak IBOutlet NSLayoutConstraint *filterViewHidthCnstraint;
    __weak IBOutlet NSLayoutConstraint *filterViewHightConstraint;
    
    
    //FilterData souce
    NSMutableArray * masterDataArray;
    NSMutableArray * selectedDataArray;
    
    
    NSMutableArray * firstFilterDataArray;
    NSMutableArray * environmentArray_1;
    NSMutableArray * environmentArray_2;
    NSMutableArray * heatRangeArray;
    NSMutableArray * filteredPoints;
    
    NSMutableArray * firstRangeAnotations;
    NSMutableArray * secondRangeAnotations;
    NSMutableArray * thredRangeAnotations;
    NSMutableArray * forthRangeAnotations;
    NSMutableArray * fifthRangeAnotations;
    NSMutableArray * sixthRangeAnotations;
    
    NSMutableArray * areaTypesArray;
    NSMutableArray * environmentTypesArray;
    
    NSMutableArray *allAnnotations;
    NSMutableSet *annotationsToAdd;
    
    int firstPreSelctedIndex;
    int secondPreSelctedIndex;
    int thredPreSelctedIndex;
    
    //Filter
    NSString * filterString;
    __weak IBOutlet UILabel *filterTL;
    __weak IBOutlet UIView *filterDisplayView;
    NSMutableArray * selectedHeatRages;
    NSMutableArray * selectedHeatRagePoints;
    
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
    NSMutableArray * selectedEnvironmentType_1;
    NSMutableArray * selectedEnvironmentType_2;
    
    MKCoordinateRegion mapRegion;
    
}
//Mapkit
@property (weak, nonatomic) IBOutlet OCMapView *mapView;

@end
