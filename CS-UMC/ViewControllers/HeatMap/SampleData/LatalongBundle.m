//
//  LatalongBundle.m
//  CS-UMC
//
//  Created by SCIT on 8/1/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "LatalongBundle.h"

@implementation LatalongBundle

-(NSArray *)genarateLatlongbundle :(int)pointCount {
    int currentPointId = 0;
    NSMutableArray * points = [[NSMutableArray alloc]init];
    while (currentPointId != pointCount) {
        
        //Data
//        NSLog(@"points LatLon = %f,%f",[self randamnumber:-37.7431147 :-37.5553886],[self randamnumber:144.974903 :145.0251154]);
        float latitude = [self randamnumber:6.9931147 :-37.5553886];
        float longatide = [self randamnumber:79.974903 :145.0251154];
        NSString * deviceName = [NSString stringWithFormat:@"DeviceID%i",currentPointId];
        float tempreture = [self randamnumber:0 :60];
        float humidity = [self randamnumber:10 :40];
        int areaType = [self randamInt:0 :2];
        int environmentId = [self randamInt:0 :4];
        
        //Bundle
        NSDictionary * randampoint = @{@"latitude":[NSNumber numberWithFloat:latitude],
                                       @"longitude":[NSNumber numberWithFloat:longatide],
                                       @"device_name":deviceName,
                                       @"area_type_id" : [NSNumber numberWithInt:areaType],
                                       @"environment_id": [NSNumber numberWithInt:environmentId],
                                       @"temperature" :[NSNumber numberWithFloat:tempreture],
                                       @"humidity" :[NSNumber numberWithFloat:humidity]};
        
        currentPointId++;
        [points addObject:randampoint];
    }
    return points;
}
-(float)randamnumber :(float)lowevel :(float)highlevel{
    float rndValue = (((float)arc4random()/0x100000000)*(highlevel-lowevel)+lowevel);
    return rndValue;
}
-(int)randamInt :(int)lowevel :(int)highlevel{
  int rn = (arc4random() % highlevel) + lowevel;
    return rn;
}
@end
