//
//  ConfirmLocationViewController.h
//  CS-UMC
//
//  Created by SCIT on 5/7/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "Constants.h"
#import "AppUserDefaults.h"
#import "WKTPointM.h"
#import "WKTParser.h"
#import "MasterViewController.h"


@interface ConfirmLocationViewController : MasterViewController<CLLocationManagerDelegate,MKMapViewDelegate>{
    CLLocationManager *locationManager;
    __weak IBOutlet MKMapView *sensorLocationMV;
    BOOL isLocated;
    NSDictionary * sensorLocations;
    
    __weak IBOutlet UIButton *btnDone;
    UIActionSheet *actionSheet;
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
}

@end
