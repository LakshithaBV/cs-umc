//
//  ConfirmLocationViewController.m
//  CS-UMC
//
//  Created by SCIT on 5/7/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "ConfirmLocationViewController.h"
#import "SeachDeviceViewController.h"

@interface ConfirmLocationViewController ()

@end

@implementation ConfirmLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isLocated = false;
    sensorLocationMV.showsUserLocation = true;
    btnDone.layer.cornerRadius = 3.0f;
    btnDone.layer.masksToBounds = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoLogout)
                                                 name:LOGOUT_NOTITFICATIONS
                                               object:nil];
    
    // Do any additional setup after loading the view.
    [self loadCurrentPosition];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnBackClicks:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)loadCurrentPosition{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([CLLocationManager locationServicesEnabled]) {
        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [locationManager requestWhenInUseAuthorization];
        }
        [locationManager startUpdatingLocation];
    } else {
        NSLog(@"Location services are not enabled");
    }
    [locationManager startUpdatingLocation];
    CLLocationCoordinate2D mylocation = sensorLocationMV.userLocation.coordinate;
    NSLog(@"Lat %f lon %f",mylocation.latitude,mylocation.longitude);
}
- (void)requestAlwaysAuthorization
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        NSString *title;
        title = (status == kCLAuthorizationStatusDenied) ? @"Location services are off" : @"Background location is not enabled";
        NSString *message = @"To use background location you must turn on 'Always' in the Location Services Settings";
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:@"Settings", nil];
        [alertView show];
    }
    // The user has not enabled any location services. Request background authorization.
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [locationManager requestAlwaysAuthorization];
    }
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
        if (!isLocated) {
            [self updateLocationOnMap:currentLocation.coordinate];
            [locationManager stopUpdatingHeading];
            isLocated = true;
        }
    }
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
}
-(void)updateLocationOnMap:(CLLocationCoordinate2D)center {
    MKCoordinateRegion mapRegion;
    mapRegion.center = center;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [sensorLocationMV setRegion:mapRegion animated: YES];
}
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    MKCoordinateRegion mapRegion;
    mapRegion.center = mapView.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [mapView setRegion:mapRegion animated: YES];
}
-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    
    CLLocationCoordinate2D centre = [mapView centerCoordinate];
    
    CGFloat latitude = centre.latitude;
    CGFloat logatude = centre.longitude;

    [AppUserDefaults setSelectedLatitude:latitude];
    [AppUserDefaults setSelectedLongatude:logatude];
    
    WKTPoint *p = (WKTPoint *)[WKTParser parseGeometry:@"POINT (-73.995 41.145556)"];
    MKPointAnnotation *point = [p toMapPointAnnotation];
    point.coordinate = CLLocationCoordinate2DMake(latitude, logatude);
    point.title = [NSString stringWithFormat:@"%0.2f, %0.2f",latitude,logatude];
    [sensorLocationMV addAnnotation:point];
}
- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    [sensorLocationMV removeAnnotations:sensorLocationMV.annotations];
}
- (IBAction)btnCurrentLocationClicks:(id)sender {
    isLocated = false;
    [self loadCurrentPosition];
}
- (IBAction)btnDoneClicks:(id)sender {
    [self captureScreenShot];
    [self.navigationController popViewControllerAnimated:true];
}
-(void)captureScreenShot{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(self.view.bounds.size);
    }
    
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIImage * cropedImage = [self croppIngimageByImageName:image toRect:CGRectMake(100, 400, 600, 600)];
    
    NSData *imageData = UIImagePNGRepresentation(cropedImage);
    [AppUserDefaults setENVImage:imageData];
    UIGraphicsEndImageContext();
}
- (UIImage *)croppIngimageByImageName:(UIImage *)imageToCrop toRect:(CGRect)rect
{
    //CGRect CropRect = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height+15);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], rect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}
#pragma mark -
#pragma mar3k Auto logout with device disconnect

-(void)autoLogout {
    NSLog(@"Logout_3");
    [[BTDeviceManager sharedInstance] disconnect];
    SeachDeviceViewController * searchdevice = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
    [self.navigationController pushViewController:searchdevice animated:true];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
