//
//  PhotoDetailsViewController.h
//  CS-UMC
//
//  Created by SCIT on 6/20/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUserDefaults.h"
#import "MasterViewController.h"

@interface PhotoDetailsViewController : MasterViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    UIImagePickerController *picker;
    __weak IBOutlet UIImageView *locationPhotos;
    
    __weak IBOutlet UITextView *placeDescriptionTV;
     __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
}
@property(nonatomic,retain)UIImage * dataImage;
@end
