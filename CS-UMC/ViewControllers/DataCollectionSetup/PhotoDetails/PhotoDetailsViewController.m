//
//  PhotoDetailsViewController.m
//  CS-UMC
//
//  Created by SCIT on 6/20/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "PhotoDetailsViewController.h"

@interface PhotoDetailsViewController ()

@end

@implementation PhotoDetailsViewController
@synthesize dataImage;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self placePhotoImage];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    return true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnBackClicks:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

//- (IBAction)btnTakePhotoclicks:(id)sender {
//        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Take a Photo" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//
//        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//            [self cameraAction];
//    //        [self dismissViewControllerAnimated:YES completion:^{
//    //        }];
//        }]];
//
//        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Use Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//                [self openLibrary];
//            });
//
//            [self dismissViewControllerAnimated:YES completion:^{
//            }];
//        }]];
//
//        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//
//            // OK button tapped.
//
//            [self dismissViewControllerAnimated:YES completion:^{
//            }];
//        }]];
//        // Present action sheet.
//        [self presentViewController:actionSheet animated:YES completion:nil];
//}
//-(void)openLibrary{
//    picker = [[UIImagePickerController alloc] init];
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    [self presentViewController:picker animated:YES completion:nil];
//}
//-(void)cameraAction{
//    picker = [[UIImagePickerController alloc] init];
//    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//    picker.delegate = self;
//    picker.allowsEditing = YES;
//    [self presentViewController:picker animated:YES completion:nil];
//}
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
//{
//    NSData *imageData = UIImagePNGRepresentation(image);
//    [AppUserDefaults setENVPhoto:imageData];
//    [self placePhotoImage];
//    [picker dismissViewControllerAnimated:true completion:nil];
//}
- (void) placePhotoImage{
    NSData * imageData = [AppUserDefaults getENVPhoto];
    if (imageData != nil) {
        locationPhotos.image = [UIImage imageWithData:[AppUserDefaults getENVPhoto]];
    }
}
- (IBAction)btnDoneClicks:(id)sender {
     [placeDescriptionTV resignFirstResponder];
    if ([AppUserDefaults getENVPhoto]) {
        [self.navigationController popViewControllerAnimated:true];
    }
}
- (IBAction)btnCancelClicks:(id)sender {
    placeDescriptionTV.text = @"";
    [AppUserDefaults setRemovePhoto];
    [self.navigationController popViewControllerAnimated:true];
    
}
@end
