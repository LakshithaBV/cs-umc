//
//  DataCollectionViewController.m
//  CS-UMC
//
//  Created by SCIT on 5/7/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "DataCollectionViewController.h"
#import "LocationAttributesViewController.h"
#import "ConfirmLocationViewController.h"
#import "PhotoDetailsViewController.h"
#import "SeachDeviceViewController.h"

@interface DataCollectionViewController ()

@end

@implementation DataCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    locationIV.hidden = true;
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoLogout)
                                                 name:LOGOUT_NOTITFICATIONS
                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    NSData * imageData = [AppUserDefaults getENVImage];
    if (imageData != nil){
        CLTopContraint.constant = 20;
        TPTopContraint.constant = 160;
        locationIV.hidden = false;
        locationIV.image = [UIImage imageWithData:imageData];
    }else{
        CLTopContraint.constant = 80;
        TPTopContraint.constant = 80;
        locationIV.hidden = true;
    }
    [recordIndicater setHidden:true];
    
    [self placePhotoImage];
    [self applyDataCollectionStatus];
    

    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
    
}
-(void)applyDataCollectionStatus {
    
    if ([[AppUserDefaults getAttributeArray] count] > 0) {
        attributesDoneIV.image = [UIImage imageNamed:@"checkmark"];
    }else{
//        locationIV.image = [UIImage imageNamed:@""];
    }
    
    if ([AppUserDefaults getENVImage] != nil) {
        locationDoneIV.image = [UIImage imageNamed:@"checkmark"];
    }else{
        locationDoneIV.image = [UIImage imageNamed:@""];
    }
    
    if ([AppUserDefaults getENVPhoto] != nil) {
        photosIV.image = [UIImage imageNamed:@"checkmark"];
    }else{
        photosIV.image = [UIImage imageNamed:@""];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)btnBackClicks:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
#pragma mark - FRONTVIEW
- (IBAction)btnStartedClicks:(id)sender {
    frontView.hidden = YES;
}
#pragma mark - VIEWFUNCTION

- (IBAction)btnLocationAttributes:(id)sender {
    LocationAttributesViewController * locationAttribute = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationAttributesViewController"];
    [self.navigationController pushViewController:locationAttribute animated:YES];
}
- (IBAction)btnConfirmLocationClick:(id)sender {
    ConfirmLocationViewController * locationConfirm = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmLocationViewController"];
    [self.navigationController pushViewController:locationConfirm animated:YES];
}
#pragma mark - TAKEPHOTOCLICK
- (IBAction)btnTakeaPhoto:(id)sender {
    [self TakePhotoclicks];
}

- (void) placePhotoImage{
    NSData * imageData = [AppUserDefaults getENVPhoto];
    if (imageData != nil) {
        takePhotoIV.hidden = false;
        TPBottomConstraint.constant = 160.0;
        takePhotoIV.image = [UIImage imageWithData:[AppUserDefaults getENVPhoto]];
    }else{
        takePhotoIV.hidden = true;
        TPBottomConstraint.constant = 80.0;
    }
}
- (IBAction)btnStartRecording:(id)sender {

    [self requesToRecordData:0];
}
-(void)requesToRecordData :(int)requestType{
    
    CGFloat lat = [AppUserDefaults getSelectedLatitude];
    CGFloat lon = [AppUserDefaults getSelectedLongatude];
    NSArray * attributeArray = [AppUserDefaults getAttributeArray];
    NSInteger areaType_id = [[attributeArray firstObject] integerValue];
    NSInteger environment_id = [[attributeArray objectAtIndex:1] integerValue];
    NSInteger special_feature_id = [[attributeArray objectAtIndex:2] integerValue];
    NSInteger windCondition_id = [[attributeArray lastObject] integerValue];
    NSString * sensor_id = [AppUserDefaults getConnectedPeripheralName];
    
    UIImage * locationImage = locationIV.image;
    UIImage * mapImage = takePhotoIV.image;

    if ((lat != 0) && (lon != 0) && (attributeArray.count > 3) && (sensor_id != nil) && (locationImage != nil)) {
        [recordIndicater setHidden:false];
        [recordIndicater startAnimating];
        NSDictionary * requestDic = @{@"location_name":@"name",
                                      @"longitude" :[NSNumber numberWithFloat:lon],
                                      @"latitude" : [NSNumber numberWithFloat:lat],
                                      @"environment_id": [NSNumber numberWithInteger:environment_id],
                                      @"area_type_id": [NSNumber numberWithInteger:areaType_id],
                                      @"special_feature_id": [NSNumber numberWithInteger:special_feature_id],
                                      @"wind_condition_id": [NSNumber numberWithInteger:windCondition_id],
                                      @"request_type": [NSNumber numberWithInt:requestType],
                                      @"sensor_id": sensor_id };
        
        if (!webapi)webapi = [[WebserviceApi alloc]init];
        webapi.delegate = self;
        NSLog(@"dataDic %@",requestDic);
        if ([self ReachabilityPass]) {
            [webapi setEnvironment:requestDic Location:locationImage Map:mapImage];
        }else{
            [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
        }
    }else {
        [self showAlert:@"Sorry!" :@"Data collection not complete.Please check and try again."];
    }
}
-(void)DidReceiveEnvironmentinfo:(id)respond {
    NSLog(@"request %@",respond);
    [recordIndicater setHidden:true];
    [recordIndicater stopAnimating];
    
    if ([[respond objectForKey:@"code"] intValue] == 1) {
        [AppUserDefaults setIsRecordingOn:true];
        NSString * message = [respond objectForKey:@"message"];
        [AppUserDefaults setisEnvironmentSetup:TRUE];
        int rowid = [[respond objectForKey:@"location_row_id"] intValue];
        [AppUserDefaults setRowId:rowid];
        if ((message == nil) || ([message isEqualToString:@""])) {message = NSLocalizedString(NOINTERNET, @"");}
        [self showAlert:@"CS-UMC" :message];
        [self jumpToHome];
        [self resetSensorAndData];
    }else if ([[respond objectForKey:@"code"] intValue] == 4){
        NSString * message = [respond objectForKey:@"message"];
        if ((message == nil) || ([message isEqualToString:@""])) {message = NSLocalizedString(NOINTERNET, @"");}
        [self showAlertForOveride:@"CS-UMC" :message];
    }else{
        
        NSString * message = [respond objectForKey:@"message"];
        if ((message == nil) || ([message isEqualToString:@""])) {message = NSLocalizedString(NOINTERNET, @"");}
        [self showAlert:@"CS-UMC" :message];
    }
}
-(void)resetSensorAndData{
    //Sensor
    if (!dbManager)dbManager = [[DBManager alloc]init];
    [dbManager deleteAllRecords];
    
    [[BTDeviceManager sharedInstance] resetSensorByController];
    [[BTDeviceManager sharedInstance] setDelegate:self];
    
}
-(void)didFinishRetriveData:(BOOL)isSuccess :(NSArray *)historyData{
    
}
-(void)jumpToHome{
    SWRevealViewController * revealController = self.revealViewController;
    UIViewController * newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    [revealController pushFrontViewController:navigationController animated:YES];
}
-(void)showAlert :(NSString *)title :(NSString *)message {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showAlertForOveride :(NSString *)title :(NSString *)message {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* Overide = [UIAlertAction actionWithTitle:@"Overide" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              [self requesToRecordData:1];
                                                          }];
    UIAlertAction* Cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {
                                                              
                                                          }];
    
    [alert addAction:Overide];
    [alert addAction:Cancel];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)TakePhotoclicks {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Take a Photo" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self cameraAction];
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Use Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self openLibrary];
        });
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}
-(void)openLibrary{
    picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}
-(void)cameraAction{
    picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    NSData *imageData = UIImagePNGRepresentation(image);
    [AppUserDefaults setENVPhoto:imageData];
    [self photoActionView:image];
    [picker dismissViewControllerAnimated:true completion:nil];
}
- (void) photoActionView :(UIImage *)imageData{
    PhotoDetailsViewController * photoDetails = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoDetailsViewController"];
    photoDetails.dataImage = imageData;
    [self.navigationController pushViewController:photoDetails animated:true];
}
#pragma mark -
#pragma mark Auto logout with device disconnect

-(void)autoLogout {
    NSLog(@"Logout_1");
    [[BTDeviceManager sharedInstance] disconnect];
    SeachDeviceViewController * searchdevice = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
    [self.navigationController pushViewController:searchdevice animated:true];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
