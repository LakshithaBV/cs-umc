//
//  DataCollectionViewController.h
//  CS-UMC
//
//  Created by SCIT on 5/7/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "WebserviceApi.h"
#import "HomeViewController.h"
#import "DBManager.h"
#import "BTDeviceManager.h"
#import "MasterViewController.h"

@interface DataCollectionViewController : MasterViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,BTDeviceManagerDelegate,webservicedelegate>{
    
    __weak IBOutlet UIView *frontView;
    __weak IBOutlet UIView *textPanel;
    __weak IBOutlet UIButton *btnMenu;
    
    __weak IBOutlet NSLayoutConstraint *CLTopContraint;
    __weak IBOutlet NSLayoutConstraint *TPTopContraint;
    __weak IBOutlet NSLayoutConstraint *TPBottomConstraint;
    __weak IBOutlet UIImageView *locationIV;
    __weak IBOutlet UIImageView *takePhotoIV;
    UIImagePickerController *picker;
    
    __weak IBOutlet UIImageView *attributesDoneIV;
    __weak IBOutlet UIImageView *locationDoneIV;
    __weak IBOutlet UIImageView *photosIV;
    
    __weak IBOutlet UIActivityIndicatorView *recordIndicater;
    
    WebserviceApi * webapi;
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
    DBManager * dbManager;
    
}

@end
