//
//  SetupDetailsViewController.m
//  CS-UMC
//
//  Created by SCIT on 6/19/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "SetupDetailsViewController.h"
#import "DataCollectionViewController.h"

@interface SetupDetailsViewController ()

@end

@implementation SetupDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    BOOL isDataCollectionDone = [AppUserDefaults getisEnvironmentSetup];
    if (!isDataCollectionDone) {
        btnBack.hidden = true;
        btnMenu.hidden = false;
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
        [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
        [btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        btnBack.hidden = false;
        btnMenu.hidden = true;
    }
    
}
- (IBAction)btnBackClicks:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnGetStartedClicks:(id)sender {
    DataCollectionViewController * dataSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"DataCollectionViewController"];
    [self.navigationController pushViewController:dataSetup animated:true];
}



@end
