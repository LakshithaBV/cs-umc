//
//  SetupDetailsViewController.h
//  CS-UMC
//
//  Created by SCIT on 6/19/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "MasterViewController.h"

@interface SetupDetailsViewController : MasterViewController{
    
     __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIButton *btnBack;
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
}
@end
