//
//  PresetupDetailsViewController.m
//  CS-UMC
//
//  Created by SCIT on 9/10/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "PresetupDetailsViewController.h"
#import "SetupDetailsViewController.h"

@interface PresetupDetailsViewController ()

@end

@implementation PresetupDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}

- (IBAction)btnYesClicks:(id)sender {
    SetupDetailsViewController * dataSetup = [self.storyboard instantiateViewControllerWithIdentifier:@"SetupDetailsViewController"];
    [self.navigationController pushViewController:dataSetup animated:true];
}
- (IBAction)btnReurnClicks:(id)sender {
    SWRevealViewController * revealController = self.revealViewController;
    UIViewController * newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    [revealController pushFrontViewController:navigationController animated:YES];
}


@end
