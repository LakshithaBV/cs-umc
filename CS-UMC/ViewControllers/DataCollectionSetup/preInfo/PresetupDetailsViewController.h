//
//  PresetupDetailsViewController.h
//  CS-UMC
//
//  Created by SCIT on 9/10/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "MasterViewController.h"


@interface PresetupDetailsViewController : MasterViewController{
    
    __weak IBOutlet UIButton *btnMenu;
     __weak IBOutlet NSLayoutConstraint *navigationHeight;
}

@end
