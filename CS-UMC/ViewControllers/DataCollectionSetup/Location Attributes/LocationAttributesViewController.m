//
//  LocationAttributesViewController.m
//  CS-UMC
//
//  Created by SCIT on 5/7/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "LocationAttributesViewController.h"
#import "SeachDeviceViewController.h"

@interface LocationAttributesViewController ()

@end

@implementation LocationAttributesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    masterArray = [[NSMutableArray alloc]init];
    selectedAttributesNames = [[NSMutableArray alloc]init];
    selectedIndex = 0;
    selectedSection = 0;
//    [tableView reloadData];
    
    attributePopup.hidden = true;
    
    areaType_Index = 0;
    envirinments_Index = 0;
    specialFeatures_Index = 0;
    windCondition_Index = 0;
    selectedIndex = 100;
    common_Index = 100;
    
    attributeinnerPopup.layer.cornerRadius = 5.0f;
    attributeinnerPopup.layer.masksToBounds = true;
    
    btnDone.layer.cornerRadius = 4.0f;
    btnDone.layer.masksToBounds = true;
    [self getLocationAttributeData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoLogout)
                                                 name:LOGOUT_NOTITFICATIONS
                                               object:nil];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnLocationClicks:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnAreaClicks:(id)sender {
    NSLog(@"Area");
    selectedSection = 0;
    common_Index = areaType_Index;
    popupTopicTL.text = @"Area";
    [masterArray removeAllObjects];
    [masterArray addObjectsFromArray:areaType];
    
    if (masterArray.count > 0) {
        attributePopup.hidden = false;
        [self manageHeightOfPopup:(int)masterArray.count];
        [tableView reloadData];
    }
}
- (IBAction)btnLocationClick:(id)sender {
    NSLog(@"Location");
    selectedSection = 1;
    selectedIndex = 100;
    common_Index = envirinments_Index;
    popupTopicTL.text = @"Locations";
    [masterArray removeAllObjects];
    [masterArray addObjectsFromArray:envirinments];
    
    if (masterArray.count > 0) {
        attributePopup.hidden = false;
        [self manageHeightOfPopup:(int)masterArray.count];
        [tableView reloadData];
    }
}
- (IBAction)btnSpecialFeaturesClicks:(id)sender {
    NSLog(@"Special");
    selectedSection = 2;
       popupTopicTL.text = @"Special features";
    common_Index = specialFeatures_Index;
    selectedIndex = 100;
    [masterArray removeAllObjects];
    [masterArray addObjectsFromArray:specialFeatures];
    
    if (masterArray.count > 0) {
        attributePopup.hidden = false;
        [self manageHeightOfPopup:(int)masterArray.count];
        [tableView reloadData];
    }
}
- (IBAction)btnWindClicks:(id)sender {
    NSLog(@"Wind");
    popupTopicTL.text = @"Wind Conditions";
    selectedSection = 3;
    selectedIndex = 100;
    common_Index = windCondition_Index;
    [masterArray removeAllObjects];
    [masterArray addObjectsFromArray:windCondition];
    if (masterArray.count > 0) {
        attributePopup.hidden = false;
        [self manageHeightOfPopup:(int)masterArray.count];
        [tableView reloadData];
    }
}
-(void)manageHeightOfPopup :(int)recordCount{
    
    CGFloat minHeight = 300.0f;
    CGFloat maxHeight = (self.view.frame.size.height - 150.0);
    CGFloat recordHeight = (45.0f * recordCount)+110.0f;
    CGFloat realHeight = 0.0f;
    
    if (recordHeight < minHeight) {
        realHeight = minHeight;
    }else if (recordHeight > maxHeight){
        realHeight = maxHeight;
    }else{
        realHeight = recordHeight;
    }
    popupHeightContraint.constant = realHeight;
}
- (IBAction)btnDoneClicks:(id)sender {
    
    if (areaType_Index ==0) {
        [self ShowAlert:@"Sorry!" message:@"Area type not selected."];
    }else if (envirinments_Index ==0){
        [self ShowAlert:@"Sorry!" message:@"Location type not selected."];
    }else if (specialFeatures_Index ==0){
        [self ShowAlert:@"Sorry!" message:@"Special type not selected."];
    }else if (windCondition_Index ==0){
        [self ShowAlert:@"Sorry!" message:@"Wind type not selected."];
    }else{
        NSArray * attributeArray = [[NSArray alloc]initWithObjects:[NSNumber numberWithInteger:areaType_Index],[NSNumber numberWithInteger:envirinments_Index],[NSNumber numberWithInteger:specialFeatures_Index],[NSNumber numberWithInteger:windCondition_Index], nil];
        [AppUserDefaults setAttributeArray:attributeArray];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)getLocationAttributeData{
    webservice = [WebserviceApi sharedInstance];
    webservice.delegate = self;
    if ([self ReachabilityPass]) {
        [webservice getLocationAttribute];
    }else{
        [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
    }
    [self inithud:@"Wait"];
}
-(void)DidReceiveLocationAttributes:(id)respond{
    NSLog(@"receivedData %@",respond);
    [self hudHide];
    NSDictionary * dataDic = [respond objectForKey:@"data"];

    [areaType removeAllObjects];
    areaType = [[NSMutableArray alloc]initWithArray:[dataDic objectForKey:@"area_types"]];

    [envirinments removeAllObjects];
    envirinments = [[NSMutableArray alloc]initWithArray:[dataDic objectForKey:@"environments"]];

    [specialFeatures removeAllObjects];
    specialFeatures = [[NSMutableArray alloc]initWithArray:[dataDic objectForKey:@"special_features"]];

    [windCondition removeAllObjects];
    windCondition = [[NSMutableArray alloc]initWithArray:[dataDic objectForKey:@"wind_conditions"]];
    
    if ([[AppUserDefaults getAttributeArray] count] == 4) {
        [self loadSelectedAttributes];
    }
//    if (![AppUserDefaults getisEnvironmentSetup]) {
        [self setupInitAttributes];
//    }
}
-(void)setupInitAttributes{
    if (areaType.count > 0) {
        NSDictionary * items = [areaType firstObject];
        areaType_Index = [[items objectForKey:@"id"] intValue];
        AreaTypeTL.text = [items objectForKey:@"name"];
    }
    if (envirinments.count > 0) {
        NSDictionary * items = [envirinments firstObject];
        envirinments_Index = [[items objectForKey:@"id"] intValue];
        locationTL.text = [items objectForKey:@"name"];
    }
    if (specialFeatures.count > 0) {
        NSDictionary * items = [specialFeatures firstObject];
        specialFeatures_Index = [[items objectForKey:@"id"] intValue];
        specialFeaturesTL.text = [items objectForKey:@"name"];
    }
    if (windCondition.count > 0) {
        NSDictionary * items = [windCondition firstObject];
        windCondition_Index = [[items objectForKey:@"id"] intValue];
        windConditionTL.text = [items objectForKey:@"name"];
    }
}
-(void)loadSelectedAttributes{
    NSArray * attributes = [AppUserDefaults getAttributeArray];
    areaType_Index = [[attributes firstObject] intValue];
    envirinments_Index = [[attributes objectAtIndex:1] intValue];
    specialFeatures_Index = [[attributes objectAtIndex:2] intValue];
    windCondition_Index = [[attributes objectAtIndex:3] intValue];
    
    //Area type
    for (NSDictionary * dicdata in areaType) {
        if (areaType_Index == [[dicdata objectForKey:@"id"] intValue]) {
            AreaTypeTL.text = [dicdata objectForKey:@"name"];
        }
    }
    
    //Location type
    for (NSDictionary * dicdata in envirinments) {
        if (envirinments_Index == [[dicdata objectForKey:@"id"] intValue]) {
            locationTL.text = [dicdata objectForKey:@"name"];
        }
    }
    
    //specialFeatures
    for (NSDictionary * dicdata in specialFeatures) {
        if (specialFeatures_Index == [[dicdata objectForKey:@"id"] intValue]) {
            specialFeaturesTL.text = [dicdata objectForKey:@"name"];
        }
    }
    
    //WindCondition
    for (NSDictionary * dicdata in windCondition) {
        if (windCondition_Index == [[dicdata objectForKey:@"id"] intValue]) {
            windConditionTL.text = [dicdata objectForKey:@"name"];
        }
    }
    NSLog(@"attributes %@",attributes);
}
//TabelData
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return masterArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"AttributeCell";
    AttributeCell *cell = (AttributeCell *)[theTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AttributeCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    NSDictionary * items = [masterArray objectAtIndex:indexPath.row];
    NSInteger myIndex = [[items objectForKey:@"id"] intValue];
    cell.textTL.text = [items objectForKey:@"name"];
    
    if (myIndex == common_Index) {
        cell.doneIV.hidden = false;
    }else{
        cell.doneIV.hidden = true;
    }
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary * items = [masterArray objectAtIndex:indexPath.row];
    
    switch (selectedSection) {
        case 0:
            areaType_Index = [[items objectForKey:@"id"] intValue];
            AreaTypeTL.text = [items objectForKey:@"name"];
            common_Index = areaType_Index;
            break;
        case 1:
            envirinments_Index = [[items objectForKey:@"id"] intValue];
            locationTL.text = [items objectForKey:@"name"];
            common_Index = envirinments_Index;
            break;
        case 2:
            specialFeatures_Index = [[items objectForKey:@"id"] intValue];
            specialFeaturesTL.text = [items objectForKey:@"name"];
            common_Index = specialFeatures_Index;
            break;
        case 3:
            windCondition_Index = [[items objectForKey:@"id"] intValue];
            windConditionTL.text = [items objectForKey:@"name"];
            common_Index = windCondition_Index;
            break;
            
        default:
            break;
    }
    selectedIndex = indexPath.row;
    [tableView reloadData];
}
- (IBAction)btnCancelClicks:(id)sender {
    attributePopup.hidden = true;
}
- (IBAction)btnSelectDoneClicks:(id)sender {
    attributePopup.hidden = true;
}
#pragma mark -
#pragma mark Auto logout with device disconnect

-(void)autoLogout {
    NSLog(@"Logout_2");
    [[BTDeviceManager sharedInstance] disconnect];
    SeachDeviceViewController * searchdevice = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
    [self.navigationController pushViewController:searchdevice animated:true];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
