//
//  AttributeCell.h
//  CS-UMC
//
//  Created by SCIT on 6/18/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttributeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *textTL;
@property (weak, nonatomic) IBOutlet UIImageView *doneIV;
@property (weak, nonatomic) IBOutlet UIView *backgroundColorView;

@end
