//
//  LocationAttributesViewController.h
//  CS-UMC
//
//  Created by SCIT on 5/7/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AttributeCell.h"
#import "WebserviceApi.h"
#import "MasterViewController.h"

@interface LocationAttributesViewController : MasterViewController<UITableViewDelegate,UITableViewDataSource,webservicedelegate>{
    NSMutableArray * masterArray;
    
    NSMutableArray * areaType;
    NSMutableArray * envirinments;
    NSMutableArray * specialFeatures;
    NSMutableArray * windCondition;
    
    NSMutableArray * selectedAttributesNames;
    
    __weak IBOutlet UILabel *AreaTypeTL;
    __weak IBOutlet UILabel *locationTL;
    __weak IBOutlet UILabel *specialFeaturesTL;
    __weak IBOutlet UILabel *windConditionTL;
    
    NSInteger areaType_Index;
    NSInteger envirinments_Index;
    NSInteger specialFeatures_Index;
    NSInteger windCondition_Index;
    NSInteger common_Index;
    
    
    __weak IBOutlet UITableView *tableView;
    WebserviceApi * webservice;
    
    NSInteger selectedIndex;
    NSInteger selectedSection;
    __weak IBOutlet UIView *attributePopup;
    __weak IBOutlet UIView *attributeinnerPopup;
    __weak IBOutlet UILabel *popupTopicTL;
    
    __weak IBOutlet UIButton *btnDone;
    
    //constraints
    __weak IBOutlet NSLayoutConstraint *popupHeightContraint;
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
}

@end
