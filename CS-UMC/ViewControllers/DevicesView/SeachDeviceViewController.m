//
//  SeachDeviceViewController.m
//  CS-UMC
//
//  Created by SCIT on 1/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "SeachDeviceViewController.h"
#import "BTDeviceManager.h"
#import "DevicesCell.h"
#import "AppLocationmanager.h"
#import "RegistrationViewController.h"
#import "SeachDeviceViewController.h"


@interface SeachDeviceViewController ()

@end

@implementation SeachDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //    [AppUserDefaults setisLogout:true];
    [self.navigationItem setHidesBackButton:YES];
    self.title = @"Devices";
    deviceTable.hidden = true;
    Btmanager = [BTDeviceManager sharedInstance];
    [self inithud:@"Check Bluetooth connection"];
    isDeviceearching = NO;
    isNavigated = NO;
    [self activityIndicaterStart:false];
    firstView.layer.cornerRadius = 3.0f;
    firstView.layer.masksToBounds = YES;
    secondView.layer.cornerRadius = 3.0f;
    secondView.layer.masksToBounds = YES;
    profixData = [AppUserDefaults getProfix];
    isPreConnectedDevice = false;
    isStringMatch = false;
    [self performSelector:@selector(checkBluetoothStatus) withObject:nil afterDelay:0];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(activateAppFromBackground)
                                                 name:APPACTIVE_NOTITFICATIONS
                                               object:nil];
}
-(void)activateAppFromBackground{
    [self btnRefreshClicks:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (IBAction)btnRefreshClicks:(id)sender {
    isDeviceearching = NO;
    [self activityIndicaterStart:true];
    [self performSelector:@selector(checkBluetoothStatus) withObject:nil afterDelay:0];
}
#pragma mark -
#pragma mark Serch Bluetooh

-(void)checkBluetoothStatus{
    [[BTDeviceManager sharedInstance] setDelegate:self];
    [[BTDeviceManager sharedInstance] setIsStatusCheck:YES];
    [[BTDeviceManager sharedInstance] startSearching];
}
-(void)getIsBlueToothOn:(BOOL)isAvailable{
    if (isAvailable) {
        [self hudHide];
        [self performSelector:@selector(startSearshing) withObject:nil afterDelay:0.0];
    }else{
        [self hudHide];
        [self showAlertForBluetooth:@"Turn On Bluetooth to Find Sensors" message:@""];
    }
}
-(void)startSearshing{
    //    [self inithud:@"Searching devices"];
    isDeviceearching = YES;
    [[BTDeviceManager sharedInstance] setDelegate:self];
    [[BTDeviceManager sharedInstance] setIsStatusCheck:NO];
    [[BTDeviceManager sharedInstance] startSearching];
    
}
-(void)didSelectedDeviceFromSearch:(NSArray *)devices {
    //        NSLog(@"Device set %@",devices);
    if (devices.count > 0) {
        deviceTable.hidden = false;
    }
    if (!deviceArray)deviceArray = [[NSMutableArray alloc]init];
    [deviceArray removeAllObjects];
    
    for (CBPeripheral * periparal in devices) {
        if (periparal.name.length > 2) {
            [deviceArray addObject:periparal];
        }
    }
    [deviceTable reloadData];
    [self activityIndicaterStart:false];
}
-(void)activityIndicaterStart :(BOOL)isStarts{
    if (isStarts) {
        btnRefresh.hidden = true;
        searchIndicator.hidden = false;
        [searchIndicator startAnimating];
    }else{
        btnRefresh.hidden = false;
        searchIndicator.hidden = true;
        [searchIndicator stopAnimating];
    }
}
#pragma mark -
#pragma mark Serch Internet

-(void)checkInternetAccess{
    reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable){
        [self showAlertForBluetooth:@"Turn On Internet Connection to Start." message:@""];
    }else{
        //        [self inithud:@""];
        [self performSelector:@selector(getSensorLocation) withObject:nil afterDelay:1];
    }
    [self hudHide];
}

#pragma mark -
#pragma mark Serch Location

-(void)getSensorLocation{
    //    [self showLocationPopup:YES];
    //    [[AppLocationmanager sharedInstance] startUpdateLocation];
}
-(void)showAlertForBluetooth :(NSString *)title message:(NSString *)msg{
    
    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:title
                                                                               message: msg
                                                                        preferredStyle:UIAlertControllerStyleAlert                   ];
    
    //Step 2: Create a UIAlertAction that can be added to the alert
    UIAlertAction* Settings = [UIAlertAction
                               actionWithTitle:@"Settings"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                   if ([[UIApplication sharedApplication] canOpenURL:url]) {
                                       [[UIApplication sharedApplication] openURL:url];
                                   }
                               }];
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //Do some thing here, eg dismiss the alertwindow
                                 [myAlertController dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    
    //Step 3: Add the UIAlertAction ok that we just created to our AlertController
    [myAlertController addAction: Settings];
    [myAlertController addAction: Cancel];
    
    //Step 4: Present the alert to the user
    [self presentViewController:myAlertController animated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnPickLocation:(id)sender {
    [[AppLocationmanager sharedInstance] setDelegate:self];
    [[AppLocationmanager sharedInstance] startUpdateLocation];
}

#pragma mark -
#pragma mark Device Table

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return deviceArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *simpleTableIdentifier = @"DevicesCell";
    DevicesCell *cell = (DevicesCell *)[deviceTable dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DevicesCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CBPeripheral * peripheral = [deviceArray objectAtIndex:indexPath.row];
    
    if (peripheral.name == NULL) {
        cell.deviceNameTL.text = @"UNKNOWN";
    }else{
        cell.deviceNameTL.text = peripheral.name;
    }
    
    cell.deviceId.text = [NSString stringWithFormat:@"%@",peripheral.identifier];
    
    if (Connected_peripheral == peripheral) {
        cell.deviceConnectedIV.hidden = false;
    }else{
        cell.deviceConnectedIV.hidden = true;
    }
    return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CBPeripheral * periparalData  = [deviceArray objectAtIndex:indexPath.row];
    NSString * selectedDevice = periparalData.name;
    NSString * prefix = [AppUserDefaults getProfix];
    if ([self isStringConsist:prefix :selectedDevice]) {
        isStringMatch = true;
        Connected_peripheral = periparalData;
    }else{
        Connected_peripheral = nil;
        [self ShowAlert:@"Oops!" message:@"Device id not match.Please try again."];
    }
    
    [deviceTable reloadData];
}
-(void)didDisconnectDevice:(CBPeripheral *)device{
    if ([deviceArray containsObject:device]) {
        [deviceArray removeObject:device];
        [deviceTable reloadData];
    }
}
-(BOOL)isStringConsist:(NSString *)prefix :(NSString *)string{
    if ([string.lowercaseString rangeOfString:prefix].location == NSNotFound) {
        return false;
    } else {
        return true;
    }
}
-(void)didSelectLocation:(CLLocation *)pickedLocation{
    //    [self showLocationPopup:NO];
    if (!isDeviceearching) {
        NSString * lat =[NSString stringWithFormat:@"%f",pickedLocation.coordinate.latitude];
        NSString * lng =[NSString stringWithFormat:@"%f",pickedLocation.coordinate.longitude];
        
        NSDictionary * locationData = @{@"longitude" :lng,
                                        @"latitude": lat};
        
        [AppUserDefaults setSelectedLocationCordinate:locationData];
        [self startSearshing];
        [self addRightDoneButton];
    }
}
-(void)addRightDoneButton{
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donebuttonaction)];
    self.navigationItem.rightBarButtonItem = anotherButton;
}
- (IBAction)btnPairClicks:(id)sender {
    
    //Conditions
    //Check Connected Device
    isNavigated = NO;
    NSString * connectedName = [AppUserDefaults getConnectedPeripheralName];
    NSString * currentConnected = Connected_peripheral.name;
    if ([connectedName isEqualToString:currentConnected]) {
        isPreConnectedDevice = true;
        [self btConnection];
    }else{
        if (connectedName == nil){
            [self btnConnect];
        }else{
            [self showReRegisterAlert];
        }
        
    }
}
-(void)btConnection{
    if (Connected_peripheral) {
        [[BTDeviceManager sharedInstance] setCurrentPeripheral:Connected_peripheral];
        [[BTDeviceManager sharedInstance] setDelegate:self];
        [[BTDeviceManager sharedInstance] connect];
        [AppUserDefaults setConnectedPeripheral:Connected_peripheral];
        [self inithud:@"Connecting..."];
    }
}
-(void)getIsBlueToothDeviceConnected:(BOOL)isAvailable :(CBPeripheral *)device{
    [self hudHide];
    if ([device.name isEqualToString:Connected_peripheral.name]) {
        if (isAvailable) {
            if (!isNavigated) {
                isNavigated = YES;
                [self navigationHandler];
            }
        }else{
            //            [self ShowAlert:@"Oops!" message:@"Unabel to connect.Please try again later."];
        }
    }else{
        NSLog(@"Device BBBBBBBBBBBBBB");
    }
}
-(void)navigationHandler{
    if (isPreConnectedDevice) { //Old device
        if (isStringMatch) {
            [AppUserDefaults setConnectedPeripheral:Connected_peripheral];
            NSDictionary * oldPlayload = [AppUserDefaults getRegisterPayload];
            
            if (oldPlayload != nil) {
                [self checkEmailVerification];
            }else{
                NSString * accessToken = [AppUserDefaults getAccessTokan];
                if (accessToken!= nil) {
                    [[NSNotificationCenter defaultCenter]postNotificationName:TIMERRESET_NOTITFICATIONS object:self]; //PostForStartAgain
                    [self checkIsExperimentExsist];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        SWRevealViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
//                        [self.navigationController pushViewController:controller animated:YES];
//                    });
                }else{
                    [self navigateToRegister];
                }
            }
        }else{
            [self ShowAlert:@"Oops!" message:@"Sensor not in system."];
        }
    }else{ //New device
        if (isStringMatch) {
            [self navigateToRegister];
        }else{
            [self ShowAlert:@"Oops!" message:@"Sensor not in system."];
        }
    }
}
-(void)removeAllRecordsAndJumpToRegister{
    [AppUserDefaults removeAllRecords:false];
    [AppUserDefaults setConnectedPeripheral:Connected_peripheral];
    [AppUserDefaults removeRegisterPayload];
    [self btConnection];
}
- (void)btnConnect {
    [AppUserDefaults setConnectedPeripheral:Connected_peripheral];
    [[BTDeviceManager sharedInstance] setCurrentPeripheral:Connected_peripheral];
    [[BTDeviceManager sharedInstance] connect];
}
-(void)navigateToRegister{
    RegistrationViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"RegistrationViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}
-(void)navigateToMenu{
    dispatch_async(dispatch_get_main_queue(), ^{
        SWRevealViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    });
}
#pragma mark -
#pragma mark Email Verification

-(void)checkEmailVerification{
    [self inithud:@"Wait"];
    NSDictionary * oldPlayload = [AppUserDefaults getRegisterPayload];
    WebserviceApi * webcall = [WebserviceApi sharedInstance];
    webcall.delegate = self;
    
    if ([self ReachabilityPass]) {
        [webcall Register:oldPlayload];
    }else{
        [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
    }
}
-(void)DidReceiveRegisterinfo:(id)respond{
    [self hudHide];
    if ([[respond objectForKey:@"code"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
        if ([respond objectForKey:@"data"]!=nil) {
            NSLog(@"Logdata %@",[respond objectForKey:@"data"]);
        }
    }else if ([[respond objectForKey:@"code"] isEqualToNumber:[NSNumber numberWithInt:4]]){
        if ([respond objectForKey:@"data"]!=nil) {
            
            BOOL isVerify = [[[respond objectForKey:@"data"] objectForKey:@"verified"] boolValue];
            if (isVerify) {
                [AppUserDefaults setAccessTokan:[respond objectForKey:@"token"]];
                [AppUserDefaults setConnectedPeripheral:Connected_peripheral];
                dispatch_async(dispatch_get_main_queue(), ^{
                    SWRevealViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                    [self.navigationController pushViewController:controller animated:YES];
                });
                BOOL isEnvSet = [[respond objectForKey:@"env_exist"] boolValue];
                [AppUserDefaults setisEnvironmentSetup:isEnvSet];
                [AppUserDefaults removeRegisterPayload];
                int locationRowId = [[respond objectForKey:@"location_row_id"] intValue];
                [AppUserDefaults setRowId:locationRowId];
            }else{
                [self showRegisterAlert];
            }
            NSLog(@"Logdata %@",[respond objectForKey:@"data"]);
        }
        
    }else{
        [self ShowAlert:@"Sorry!" message:[respond objectForKey:@"message"]];
    }
}
-(void)showRegisterAlert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Email not verified"
                                                                   message:@"Please check you email and click the link to verify the email address."
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Ok"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [self checkEmailVerification];
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"New user ?"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [AppUserDefaults setConnectedPeripheral:Connected_peripheral];
                                                               [AppUserDefaults removeRegisterPayload];
                                                               [self navigateToRegister];
                                                           }]; // 3
    
    [alert addAction:firstAction]; // 4
    [alert addAction:secondAction]; // 5
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showReRegisterAlert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"New sensor found!"
                                                                   message:@"Do you want to register again ?"
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"YES"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              [self removeAllRecordsAndJumpToRegister];
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"NO"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               Connected_peripheral = nil;
                                                               [deviceTable reloadData];
                                                           }];
    
    [alert addAction:firstAction];
    [alert addAction:secondAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark -
#pragma mark experimentExisit

-(void)checkIsExperimentExsist {
    
    NSString * deviceId = Connected_peripheral.name;
    NSDictionary * dataDic = @{@"sensor_name":deviceId};
    
    if (deviceId != nil) {
        WebserviceApi * webcall = [WebserviceApi sharedInstance];
        webcall.delegate = self;
        
        if ([self ReachabilityPass]) {
            [webcall getisExperimentExsist:dataDic];
        }else{
            [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
        }
    }
}
-(void)DidReceiveexperimentDataInfo:(id)respond{
    NSLog(@"experiment_details %@",respond);
    if ([[respond objectForKey:@"code"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
        BOOL isExsist = [[respond objectForKey:@"exp_exist"] boolValue];
        [AppUserDefaults setisEnvironmentSetup:isExsist];
        [self navigateToMenu];
    }else{
         [self ShowAlert:@"Sorry!" message:[respond objectForKey:@"message"]];
    }
}
@end
