//
//  DevicesCell.h
//  CS-UMC
//
//  Created by SCIT on 1/8/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DevicesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceNameTL;
@property (weak, nonatomic) IBOutlet UILabel *deviceId;
@property (weak, nonatomic) IBOutlet UIImageView *deviceConnectedIV;

@end
