//
//  SeachDeviceViewController.h
//  CS-UMC
//
//  Created by SCIT on 1/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
#import "BTDeviceManager.h"
#import "Reachability.h"
#import "AppLocationmanager.h"
#import "AppUserDefaults.h"
#import "SensorLocationViewController.h"
#import "WebserviceApi.h"

@interface SeachDeviceViewController : MasterViewController <BTDeviceManagerDelegate,UITableViewDelegate,LocationManagerDelegate,webservicedelegate,UITextFieldDelegate>{
    
    __weak IBOutlet UIActivityIndicatorView *searchIndicator;
    __weak IBOutlet UILabel *searchText;
    NSMutableArray * deviceArray;
    __weak IBOutlet UITableView *deviceTable;
    __weak IBOutlet UIButton *btnRefresh;
    CBPeripheral * Connected_peripheral;
    BTDeviceManager * Btmanager;
    Reachability* reachability;
    BOOL isDeviceearching;
    WebserviceApi * webservice;
    NSString * profixData;
    
    BOOL isNavigated;
    
    __weak IBOutlet NSLayoutConstraint *locationPopupTopConstrain;
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
    __weak IBOutlet UIView *firstView;
    __weak IBOutlet UIView *secondView;
    
    BOOL isPreConnectedDevice;
    BOOL isStringMatch;
}

@end
