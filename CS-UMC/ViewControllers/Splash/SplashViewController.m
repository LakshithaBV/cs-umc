//
//  SplashViewController.m
//  CS-UMC
//
//  Created by SCIT on 1/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "SplashViewController.h"
#import "SeachDeviceViewController.h"
#import "SeachDeviceViewController.h"
#import "RegistrationViewController.h"
#import "InfoViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    [self performSelector:@selector(jumpToSearchDevice) withObject:nil afterDelay:3];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)jumpToSearchDevice{
    [self getPrefix];
}
-(void)jumpTOMenu{
    dispatch_async(dispatch_get_main_queue(), ^{
        SWRevealViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    });
}
-(void)getPrefix {
    
    if ([self isReachabilityPass]) {
        if (!webApi)webApi = [[WebserviceApi alloc]init];
        webApi.delegate = self;
        [webApi getPrefix];
    }else{
        NSString * oldProfix = [AppUserDefaults getProfix];
        if (oldProfix == nil) {
            [self showRegisterAlert:@"Oops!" :NSLocalizedString(NOINTERNET, @"")];
        }else{
            [self goOnwords];
        }
    }
}
-(void)goOnwords{
    if ([AppUserDefaults getAccessTokan] != nil) {
        SeachDeviceViewController * serchdev = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
        [self.navigationController pushViewController:serchdev animated:YES];
    }else{
        InfoViewController * infoView = [self.storyboard instantiateViewControllerWithIdentifier:@"InfoViewController"];
        [self.navigationController pushViewController:infoView animated:YES];
    }
}
-(void)DidReceivePrefixData:(id)respond{
    NSLog(@"respond %@",respond);
    if ([[respond objectForKey:@"code"] intValue] == 1) {
        NSString * newPrefix = [[respond objectForKey:@"data"] lowercaseString];
        [AppUserDefaults setProfix:newPrefix];
        [self goOnwords];
    }else{
        NSString * message = [respond objectForKey:@"message"];
        if ((message == nil) || ([message isEqualToString:@""])) {message = NSLocalizedString(NOINTERNET, @"");}
        [self showRegisterAlert:@"CS-UMC" :message];
    }
}
-(void)showRegisterAlert:(NSString *)title :(NSString *)message{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Ok"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                          }];
    
    [alert addAction:firstAction]; // 4
    [self presentViewController:alert animated:YES completion:nil];
}
-(BOOL)isReachabilityPass{
    
    Reachability * networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus != NotReachable) {
        return true;
    }
    return false;
}
@end
