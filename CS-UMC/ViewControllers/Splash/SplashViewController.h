//
//  SplashViewController.h
//  CS-UMC
//
//  Created by SCIT on 1/5/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
#import "SeachDeviceViewController.h"
#import "AppUserDefaults.h"
#import "WebserviceApi.h"

@interface SplashViewController : UIViewController<webservicedelegate>{
    WebserviceApi * webApi;
    
}

@end
