//
//  MenuViewController.m
//  CS-UMC
//
//  Created by SCIT on 1/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.navigationController.navigationItem setHidesBackButton:YES];
    isEnvironmentSetup = [AppUserDefaults getisEnvironmentSetup];
    fromDate = [AppUserDefaults getFromDate];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    isEnvironmentSetup = [AppUserDefaults getisEnvironmentSetup];
    fromDate = [AppUserDefaults getFromDate];
    NSLog(@"Appera");
    [menuTable reloadData];
    
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
//    [self showBlueView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    switch ( indexPath.row )
    {
        case 0:
            CellIdentifier = @"Home";
            break;
            
        case 1:
            CellIdentifier = @"Details";
            break;
            
        case 2:
            CellIdentifier = @"Review";
            break;
            
        case 3:
            CellIdentifier = @"HeatMap";
            break;
            
        case 4:
            CellIdentifier = @"Help";
            break;
            
        case 5:
            CellIdentifier = @"Exit";
            break;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath: indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (isEnvironmentSetup) {
        UIView * view = [cell viewWithTag:2];
        view.hidden = true;
        if (indexPath.row == 2) {
            if (([fromDate isKindOfClass:[NSNull class]])||(fromDate == nil)) {
                view.hidden = false;
            }else{
                view.hidden = true;
            }
        }
    }else{
        UIView * view = [cell viewWithTag:2];
        view.hidden = false;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *newFrontController = nil;
    SWRevealViewController * revealController = self.revealViewController;
    
//    BOOL isEnvironmentSetup = [AppUserDefaults getIsRecordingOn];
    
    if (indexPath.row == 0)
    {
        newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    }
    else if (indexPath.row == 1)
    {
        BOOL isDataSetup = [AppUserDefaults getisEnvironmentSetup];
        if (isDataSetup) {
            [self showDataCollectionAlert];
        }else{
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"SetupDetailsViewController"];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
            [revealController pushFrontViewController:navigationController animated:YES];
        }
        
    }
    else if (indexPath.row == 2)
    {
        if (isEnvironmentSetup) {
            if (([fromDate isKindOfClass:[NSNull class]])||(fromDate == nil)) {
                //Message
            }else{
                newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"WeatherStationsViewController"];
                UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                [revealController pushFrontViewController:navigationController animated:YES];
            }
        }
    }
    else if (indexPath.row == 3)
    {
//        if (isEnvironmentSetup) {
            newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HeatMapViewController"];
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
            [revealController pushFrontViewController:navigationController animated:YES];
//        }
        
    }else if (indexPath.row == 4)
    {
        newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpViewController"];
    }
    else if (indexPath.row == 5)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self showExitAlert];
        });
    }
}
-(void)showBlueView{

    if (isEnvironmentSetup) {
        reviewResultBlueView.hidden = true;
        heatmapBlueView.hidden = true;
    }else{
        reviewResultBlueView.hidden = false;
        heatmapBlueView.hidden = false;
    }

}
-(void)showDataCollectionAlert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Info"
                                                                   message:@"A data collection set up exists for this device. Do you want to sync it?"
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Ok"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Override"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               UIViewController *newFrontController = nil;
                                                               SWRevealViewController * revealController = self.revealViewController;
                                                               newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"PresetupDetailsViewController"];
                                                               UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
                                                               [revealController pushFrontViewController:navigationController animated:YES];
                                                           }]; // 3
    
    [alert addAction:firstAction]; // 4
    [alert addAction:secondAction]; // 5
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)showExitAlert{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Exit from app ?"
                                                                   message:@"" //By exiting the application,your data will be erased.
                                                            preferredStyle:UIAlertControllerStyleAlert]; // 1
    UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                              
                                                          }];
    UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"Exit"
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               [self exitfromApp];
                                                           }]; // 3
    
    [alert addAction:firstAction]; // 4
    [alert addAction:secondAction]; // 5
    
    [self presentViewController:alert animated:YES completion:nil];
}
-(void)exitfromApp{
    [AppUserDefaults removeAllRecords:false];
    [[BTDeviceManager sharedInstance] disconnect];
    SplashViewController * splash = [self.storyboard instantiateViewControllerWithIdentifier:@"SplashViewController"];
    [self.navigationController pushViewController:splash animated:YES];
}
@end
