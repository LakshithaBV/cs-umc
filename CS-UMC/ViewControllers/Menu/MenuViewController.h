//
//  MenuViewController.h
//  CS-UMC
//
//  Created by SCIT on 1/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "SplashViewController.h"
#import "BTDeviceManager.h"
#import "WeatherStationsViewController.h"

@interface MenuViewController : MasterViewController{
    
    __weak IBOutlet UIView *reviewResultBlueView;
    __weak IBOutlet UIView *heatmapBlueView;
    BOOL isEnvironmentSetup;
    NSString * fromDate;
    
    __weak IBOutlet UITableView *menuTable;
    
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
}

@end
