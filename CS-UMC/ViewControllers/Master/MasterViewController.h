//
//  MasterViewController.h
//  Simplefire
//
//  Created by SCIT on 6/20/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "MBProgressHUD.h"
#import "BTDeviceManager.h"

@interface MasterViewController : UIViewController{
    
    DBManager * dbmanager;
    MBProgressHUD * HUD;
    
}
-(DBManager *)getDBmanager;

//Internet access
-(BOOL)ReachabilityPass;

//Alerts
-(void)ShowAlertviewInternetUnavailable;
-(void)ShowAlert :(NSString *)title message:(NSString *)msg;

//Progress views
-(NSString *)getPresentDateTime;
-(void)inithud :(NSString *)message;
-(void)initMessageOnlyhud :(NSString *)message;
-(void)hudHide;
-(void)jumpToviewController :(UIViewController *)masterViewController :(NSString *)identifire;
-(BOOL)isiphoneWithSafeArea;

@end
