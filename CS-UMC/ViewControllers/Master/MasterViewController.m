//
//  MasterViewController.m
//  Simplefire
//
//  Created by SCIT on 6/20/16.
//  Copyright © 2016 SCIT. All rights reserved.
//

#import "MasterViewController.h"
#import "DBManager.h"
#import "Reachability.h"

@interface MasterViewController ()

@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma Mark -
#pragma Reachability


//-(BOOL)prefersStatusBarHidden { return YES; }

-(BOOL)ReachabilityPass{
    
    Reachability * networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus != NotReachable) {
        return true;
    }
    [self ShowAlertviewInternetUnavailable];
    return false;
}
//-(void) playSound {
//    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"sound" ofType:@"mp3"];
//    SystemSoundID soundID;
//    AudioServicesCreateSystemSoundID((__bridge CFURLRef)[NSURL fileURLWithPath: soundPath], &soundID);
//    AudioServicesPlaySystemSound (soundID);
//}


- (UIInterfaceOrientation) preferredInterfaceOrientationForPresentation {
    // Return the orientation you'd prefer - this is what it launches to. The
    // user can still rotate. You don't have to implement this method, in which
    // case it launches in the current orientation
    return UIInterfaceOrientationPortrait;
}
-(void)ShowAlertviewInternetUnavailable{[self ShowAlert:@"Network unavailable" message:@"Please check your internet access."];}

-(void)ShowAlert :(NSString *)title message:(NSString *)msg{
    
    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:title
                                                                               message: msg
                                                                        preferredStyle:UIAlertControllerStyleAlert                   ];
    
    //Step 2: Create a UIAlertAction that can be added to the alert
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here, eg dismiss the alertwindow
                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    //Step 3: Add the UIAlertAction ok that we just created to our AlertController
    [myAlertController addAction: ok];
    
    //Step 4: Present the alert to the user
    [self presentViewController:myAlertController animated:YES completion:nil];
}


-(DBManager *)getDBmanager{
    if (!dbmanager) dbmanager = [[DBManager alloc]init];
    return dbmanager;
}

#pragma Mark -
#pragma timestamp

-(NSString *)getPresentDateTime{
    
    NSDateFormatter *dateTimeFormat = [[NSDateFormatter alloc] init];
    [dateTimeFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *now = [[NSDate alloc] init];
    NSString *theDateTime = [dateTimeFormat stringFromDate:now];
    
    dateTimeFormat = nil;
    now = nil;
    
    return theDateTime;
}

#pragma Mark -
#pragma ProgresView
-(void)inithud :(NSString *)message{
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.label.text = message;
}
-(void)initMessageOnlyhud :(NSString *)message{
    
    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    HUD.mode = MBProgressHUDModeText;
    HUD.offset = CGPointMake(0.f, MBProgressMaxOffset);
    HUD.minSize = CGSizeMake(150.f, 100.f);
    HUD.detailsLabel.text = message;
    [HUD hideAnimated:YES afterDelay:3.f];
}
- (void)hudHide{
    [HUD hideAnimated:YES];
    HUD = nil;
}
-(void)jumpToviewController :(UIViewController *)masterViewController :(NSString *)identifire{
    masterViewController = [masterViewController.storyboard instantiateViewControllerWithIdentifier:identifire];
    [self.navigationController pushViewController:masterViewController animated:true];
    
}
-(BOOL)isiphoneWithSafeArea{
    BOOL isPhonewithSafeArea = false;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
            case 1136:
                break;
            case 1334:
                printf("iPhone 6/6S/7/8");
                break;
            case 1920:
                printf("iPhone 6+/6S+/7+/8+");
                break;
            case 2436:
                printf("iPhone X");
                isPhonewithSafeArea = true;
                break;
            default:
                printf("unknown");
        }
    }
    return isPhonewithSafeArea;
}

@end
