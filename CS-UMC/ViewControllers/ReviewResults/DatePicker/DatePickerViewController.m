//
//  DatePickerViewController.m
//  CS-UMC
//
//  Created by SCIT on 7/24/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "DatePickerViewController.h"

@interface DatePickerViewController ()

@end

@implementation DatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedIndex = 0;
    
    btnDone.layer.cornerRadius = 5.0;
    btnDone.layer.masksToBounds = true;
    
    btnCancel.layer.cornerRadius = 5.0;
    btnCancel.layer.masksToBounds = true;
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnViewDoneClicks:(id)sender {
    
    if ((fromDateTF.text!= nil) &&(toDateTF.text!= nil)) {
//        [AppUserDefaults setFromDate:fromDateTF.text];
        [AppUserDefaults setToDate:toDateTF.text];
        [[NSNotificationCenter defaultCenter]postNotificationName:FROMTODATESET_NOTITFICATIONS object:self];
        [self dismissViewControllerAnimated:true completion:nil];
    }else{
        [self ShowAlert:@"Sorry" message:@"From or To date not set."];
    }
}
- (IBAction)btnPickerDoneClicks:(id)sender {
    [self pickerAnimationsOn:false];
}
- (IBAction)btnCancelClicks:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [textField resignFirstResponder];
    if (textField.tag == 1) {
        if (selectedIndex != 1) {
             [self pickerAnimationsOn:false];
        }
         [self pickerAnimationsOn:true];
        selectedIndex = 1;
//        datePickerCotroller.maximumDate = toDate;
        popupLabel.text = @"From date :";
        
    }else if (textField.tag == 2){
//        datePickerCotroller.minimumDate = fromDate;
        if (selectedIndex != 2) {
            [self pickerAnimationsOn:false];
        }
        [self pickerAnimationsOn:true];
        selectedIndex = 2;
        
        popupLabel.text = @"To date :";
    }
}
-(void)pickerAnimationsOn :(BOOL)isShow{
    if (isShow) {
        [self.view layoutIfNeeded];
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             datepickerTopConstrain.constant = 399.0;
                             [self.view layoutIfNeeded];
                         }];
    }else{
        [self.view layoutIfNeeded];
        
        [UIView animateWithDuration:0.5
                         animations:^{
                             datepickerTopConstrain.constant = 1000.0;
                             [self.view layoutIfNeeded];
                         }];
    }
}

- (IBAction)datePickerControllerAction:(id)sender {
    NSDate * dateTime = datePickerCotroller.date;
    
   NSDateFormatter * dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"YYYY-MM-dd"];
    
    switch (selectedIndex) {
        case 1:
            fromDateTF.text = [dateFormat stringFromDate:dateTime];
            fromDate = dateTime;
            break;
            
        case 2:
            toDateTF.text = [dateFormat stringFromDate:dateTime];
            toDate = dateTime;
            break;
            
        default:
            break;
    }
}
@end
