//
//  DatePickerViewController.h
//  CS-UMC
//
//  Created by SCIT on 7/24/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUserDefaults.h"
#import "MasterViewController.h"
#import "WebserviceApi.h"

@interface DatePickerViewController : MasterViewController<UITextFieldDelegate,UIPickerViewDataSource,webservicedelegate>{
    
    __weak IBOutlet UITextField *fromDateTF;
    __weak IBOutlet UITextField *toDateTF;
    __weak IBOutlet NSLayoutConstraint * datepickerTopConstrain;
    __weak IBOutlet UIDatePicker *datePickerCotroller;
    
    __weak IBOutlet UIButton *btnDone;
    __weak IBOutlet UIButton *btnCancel;
    
    WebserviceApi * webApis;
    __weak IBOutlet UILabel *popupLabel;
    
    
    
    int selectedIndex;
    NSDate * fromDate;
    NSDate * toDate;
    
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
}
@property (nonatomic,retain)NSString * firstStation;
@property (nonatomic,retain)NSString * secondStation;


@end
