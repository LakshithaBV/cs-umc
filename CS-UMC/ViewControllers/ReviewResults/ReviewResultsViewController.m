//
//  ReviewResultsViewController.m
//  CS-UMC
//
//  Created by SCIT on 5/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "ReviewResultsViewController.h"

@interface ReviewResultsViewController ()

@end

@implementation ReviewResultsViewController
@synthesize deviceData,stationData,stationData_2;

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    zoomScale = 1;
    isTempSelected = true;
    //    [self loadHumidityChart];
    [self filterData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (IBAction)btnZoomClicks:(id)sender {
    zoomScale = zoomScale - (0.5);
    if (zoomScale < 0.5)zoomScale = 0.5;
    
    if (isTempSelected) {
        [self loadTemperatureChart];
    }else{
        [self loadHumidityChart];
    }
}
- (IBAction)btnZoomOutClicks:(id)sender {
    zoomScale = zoomScale + (0.5);
    if (zoomScale > 1.5)zoomScale = 1.5;
    
    if (isTempSelected) {
        [self loadTemperatureChart];
    }else{
        [self loadHumidityChart];
    }
}
- (IBAction)btnBaclClicks:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)filterData{
    [self clearUpContainer];
    if (stationData.count > 0) {
        NSArray * subStations = [stationData objectForKey:@"subStations"];
        NSArray * SecondsubStations = [stationData_2 objectForKey:@"subStations"];
        if (subStations.count > 0) {
            
            if (subStations.count == 1) {
                sensor_3ColorLine.hidden = true;
                sensor_3TL.hidden = true;
                firstStationName = [[subStations firstObject] objectForKey:@"subStationName"];
                
                for (NSDictionary * firstStationDic in [[subStations firstObject]objectForKey:@"data"]) {
                    WSPoint * wheterStationPoint = [[WSPoint alloc]initWithDataDictionary:firstStationDic Station:firstStationName];
                    [firstStationData addObject:wheterStationPoint];
                }
                
                if (SecondsubStations.count >0) {
                    secondStationName = [[SecondsubStations firstObject] objectForKey:@"subStationName"];
                    sensor_3ColorLine.hidden = false;
                    sensor_3TL.hidden = false;
                    for (NSDictionary * firstStationDic in [[SecondsubStations firstObject]objectForKey:@"data"]) {
                        WSPoint * wheterStationPoint = [[WSPoint alloc]initWithDataDictionary:firstStationDic Station:secondStationName];
                        [secondStationData addObject:wheterStationPoint];
                    }
                }
    
            }else if (subStations.count == 2){
                sensor_3ColorLine.hidden = false;
                sensor_3TL.hidden = false;
                
                firstStationName = [[subStations firstObject] objectForKey:@"subStationName"];
                secondStationName = [[subStations lastObject] objectForKey:@"subStationName"];
                
                for (NSDictionary * firstStationDic in [[subStations firstObject]objectForKey:@"data"]) {
                    WSPoint * wheterStationPoint = [[WSPoint alloc]initWithDataDictionary:firstStationDic Station:firstStationName];
                    [firstStationData addObject:wheterStationPoint];
                }
                for (NSDictionary * firstStationDic in [[subStations lastObject]objectForKey:@"data"]) {
                    WSPoint * wheterStationPoint = [[WSPoint alloc]initWithDataDictionary:firstStationDic Station:secondStationName];
                    [secondStationData addObject:wheterStationPoint];
                }
            }
            sensor_2TL.text = firstStationName;
            sensor_3TL.text = secondStationName;
        }
    }
    if (deviceData.count > 0){
        //        NSLog(@"filtered device data %@",deviceData);
        for (NSDictionary * firstStationDic in deviceData) {
            WSPoint * wheterStationPoint = [[WSPoint alloc]initWithDataDictionary:firstStationDic Station:@"Current_Station"];
            [myStationData addObject:wheterStationPoint];
        }
    }else{
        sensor_1TL.hidden = true;
        sensor_1ColorLine.hidden = true;
    }
    
    [self loadTemperatureChart];
}

-(void)stopSpring{
    [self hudHide];
}
-(void)manageIndecater :(int)line1_count :(int)line2_count :(int)line3_count{
    
    //Red line
    if (line3_count > 0) {
        sensor_1TL.hidden = false;
        sensor_1ColorLine.hidden = false;
        
        //SetLatLong
        
        NSString * lat = [NSString stringWithFormat:@"%0.2f",[AppUserDefaults getSelectedLatitude]];
        NSString * lon = [NSString stringWithFormat:@"%0.2f",[AppUserDefaults getSelectedLongatude]];
        
        NSString * textString = [NSString stringWithFormat:@"My Sensor (Location :%@,%@)",lat,lon];
        sensor_1TL.text = textString;
    }else{
        sensor_1TL.hidden = true;
        sensor_1ColorLine.hidden = true;
    }
    //blue line
    if (line1_count > 0) {
        sensor_2TL.hidden = false;
        sensor_2ColorLine.hidden = false;
    }else{
        sensor_2TL.hidden = true;
        sensor_2ColorLine.hidden = true;
    }
    //Yello line
    if (line2_count > 0) {
        sensor_3TL.hidden = false;
        sensor_3ColorLine.hidden = false;
    }else{
        sensor_3TL.hidden = true;
        sensor_3ColorLine.hidden = true;
    }
    
}
-(void)clearUpContainer {
    
    //New
    if (!firstStationData)firstStationData = [[NSMutableArray alloc]init];
    [firstStationData removeAllObjects];
    
    if (!secondStationData)secondStationData = [[NSMutableArray alloc]init];
    [secondStationData removeAllObjects];
    
    if (!myStationData)myStationData = [[NSMutableArray alloc]init];
    [myStationData removeAllObjects];
    
    //Old
    if (!firstStation_humidity)firstStation_humidity = [[NSMutableArray alloc]init];
    [firstStation_humidity removeAllObjects];
    if (!firstStation_Temperature)firstStation_Temperature = [[NSMutableArray alloc]init];
    [firstStation_Temperature removeAllObjects];
    
    if (!secondStation_humidity)secondStation_humidity = [[NSMutableArray alloc]init];
    [secondStation_humidity removeAllObjects];
    if (!secondStation_Temperature)secondStation_Temperature = [[NSMutableArray alloc]init];
    [secondStation_Temperature removeAllObjects];
    
    if (!myStation_humidity)myStation_humidity = [[NSMutableArray alloc]init];
    [myStation_humidity removeAllObjects];
    if (!myStation_Temperature)myStation_Temperature = [[NSMutableArray alloc]init];
    [myStation_Temperature removeAllObjects];
    
    if (!station_1TimeBaseArray)station_1TimeBaseArray = [[NSMutableArray alloc]init];
    [station_1TimeBaseArray removeAllObjects];
    if (!station_2TimeBaseArray)station_2TimeBaseArray = [[NSMutableArray alloc]init];
    [station_2TimeBaseArray removeAllObjects];
    if (!station_3TimeBaseArray)station_3TimeBaseArray = [[NSMutableArray alloc]init];
    [station_3TimeBaseArray removeAllObjects];
    
    if (!main_TimeBaseArray)main_TimeBaseArray = [[NSMutableArray alloc]init];
    [main_TimeBaseArray removeAllObjects];
    
}
-(void)loadHumidityChart{
    isTempSelected = false;;
    statusTL.text = @"Humidity (%)";
    [self inithud:@"Wait"];
    [self performSelector:@selector(stopSpring) withObject:self afterDelay:1];
    
    [mainGraph removeFromSuperview];
    chartManager = [[ChatManager alloc] init];
    [mainGraph setFrame:CGRectMake(0, 0, 363, 338)];
    mainGraph = [chartManager getTotalChart:zoomScale Type:HUMIDITY Line1:firstStationData Line2:secondStationData MySensor:myStationData];
    [btnType setTitle:@"HUMIDITY" forState:UIControlStateNormal];
    [self manageIndecater:(int)firstStationData.count :(int)secondStationData.count :(int)myStationData.count];
    [graphView addSubview:mainGraph];
}
-(void)loadTemperatureChart{
    isTempSelected = true;
    statusTL.text = @"Temperature (*C)";
    [self inithud:@"Wait"];
    [self performSelector:@selector(stopSpring) withObject:self afterDelay:0.2];
    
    [mainGraph removeFromSuperview];
    chartManager = [[ChatManager alloc] init];
    [mainGraph setFrame:CGRectMake(0, 0, 363, 338)];
    mainGraph = [chartManager getTotalChart:zoomScale Type:TEMPRETURE Line1:firstStationData Line2:secondStationData MySensor:myStationData];
    [btnType setTitle:@"HUMIDITY" forState:UIControlStateNormal];
    [self manageIndecater:(int)firstStationData.count :(int)secondStationData.count :(int)myStationData.count];
    [graphView addSubview:mainGraph];
}

-(void)manageChartWidth :(int)pointCount {
    int recordCount = pointCount;
    
    if (recordCount > 10) {
        NSLog(@"Overlap");
        int overloadedCount = (recordCount - 10);
        CGRect chartFrame = chartView.frame;
        chartFrame.size.width = (chartFrame.size.width + overloadedCount * 5);
        [chartView setFrame:chartFrame];
        chartScroller.contentSize = chartView.frame.size;
        [self scrollToRight];
    }
}
-(void)scrollToRight{
    float w = chartScroller.frame.size.width;
    float h = chartScroller.frame.size.height;
    float newPosition = chartScroller.contentOffset.x+w;
    CGRect toVisible = CGRectMake(newPosition, 0, w, h);
    [chartScroller scrollRectToVisible:toVisible animated:YES];
}
#pragma mark -- AAChartView delegate
- (void)AAChartViewDidFinishLoad {
    NSLog(@"🔥🔥🔥🔥🔥 AAChartView content did finish load!!!");
}
- (IBAction)btnSaveImageClicks:(id)sender {
    UIGraphicsBeginImageContextWithOptions(graphView.bounds.size, graphView.opaque, 0.0);
    [graphView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    
    //    NSData *imageData = UIImagePNGRepresentation(image);
    //
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //
    //    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",@"cached"]];
    //
    //    NSLog(@"pre writing to file");
    //    if (![imageData writeToFile:imagePath atomically:YES])
    //    {
    //        NSLog(@"Failed to cache image data to disk");
    //    }
    //    else
    //    {
    //        NSLog(@"the cachedImagedPath is %@",imagePath);
    //    }
}
- (IBAction)btnStypeClicks:(id)sender {
    if ([btnType.titleLabel.text isEqualToString:@"HUMIDITY"]) {
        [self loadHumidityChart];
        [btnType setTitle:@"TEMPERATURE" forState:UIControlStateNormal];
    }else{
        [self loadTemperatureChart];
        [btnType setTitle:@"HUMIDITY" forState:UIControlStateNormal];
    }
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return chartView;
}
-(void)scrollViewDidZoom:(UIScrollView *)scrollView{
    
}
@end
