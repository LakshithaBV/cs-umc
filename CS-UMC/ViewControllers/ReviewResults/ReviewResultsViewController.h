//
//  ReviewResultsViewController.h
//  CS-UMC
//
//  Created by SCIT on 5/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "ChatManager.h"
#import "AAChartView.h"
#import "LMLineGraphView.h"
#import "Constants.h"
#import "MasterViewController.h"
#import "WSPoint.h"

@interface ReviewResultsViewController : MasterViewController<AAChartViewDidFinishLoadDelegate,UIScrollViewDelegate>{
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UIView *chartViewer;
    
    ChatManager * chartManager;
    AAChartView * chartView;
    LMLineGraphView * mainGraph;
    __weak IBOutlet UIView *graphView;
    
    float zoomScale;
    
    BOOL isTempSelected;
    
    __weak IBOutlet UILabel *statusTL;
    
    __weak IBOutlet UILabel *sensor_1TL;
    __weak IBOutlet UILabel *sensor_2TL;
    __weak IBOutlet UILabel *sensor_3TL;
    
    __weak IBOutlet UIView *sensor_1ColorLine;
    __weak IBOutlet UIView *sensor_2ColorLine;
    __weak IBOutlet UIView *sensor_3ColorLine;
    
    __weak IBOutlet UIScrollView *chartScroller;
    __weak IBOutlet UIButton *btnType;
    
    NSString * firstStationName;
    NSMutableArray * firstStationData;
    NSMutableArray * firstStation_humidity;
    NSMutableArray * firstStation_Temperature;
    
    NSString * secondStationName;
    NSMutableArray * secondStationData;
    NSMutableArray * secondStation_humidity;
    NSMutableArray * secondStation_Temperature;
    
    NSMutableArray * myStationData;
    NSMutableArray * myStation_humidity;
    NSMutableArray * myStation_Temperature;
    
    
    NSMutableArray * station_1TimeBaseArray;
    NSMutableArray * station_2TimeBaseArray;
    NSMutableArray * station_3TimeBaseArray;
    NSMutableArray * main_TimeBaseArray;
    
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
}
@property (nonatomic,retain)NSDictionary * deviceData;
@property (nonatomic,retain)NSDictionary * stationData;
@property (nonatomic,retain)NSDictionary * stationData_2;
@end
