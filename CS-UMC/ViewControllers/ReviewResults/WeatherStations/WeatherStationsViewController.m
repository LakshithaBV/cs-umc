//
//  WeatherStationsViewController.m
//  CS-UMC
//
//  Created by SCIT on 6/22/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "WeatherStationsViewController.h"
#import "TopicTableViewCell.h"
#import "StationsTableViewCell.h"
#import "WeatherStationsViewController.h"

#define TOPIC                          @"topic_row";
#define DETAIL                         @"detail_row";

@interface WeatherStationsViewController ()

@end

@implementation WeatherStationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isFiltered = false;
    isDateTimePicked = true;
    isTyping = false;
    isStationsTabels = false;
    isStateViewActive = false;
    whetherStationsView.hidden = true;
    
    whetherStationInnerView.layer.cornerRadius = 5.0f;
    whetherStationInnerView.layer.masksToBounds = true;
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
    if (!selectedIndexArray)selectedIndexArray = [[NSMutableArray alloc]init];
    [selectedIndexArray removeAllObjects];
//    [selectedIndexArray addObjectsFromArray:[AppUserDefaults getReviewResultArray]];
    [self requestForLoadStations];
    
    
//    [self ShowAlert:@"Sorry!" message:@"Section under construction."];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(dateTimePassed)
//                                                 name:FROMTODATESET_NOTITFICATIONS
//                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)requestForLoadStations {
    [self inithud:@"Wait"];
    if (!webApis)webApis = [[WebserviceApi alloc]init];
    webApis.delegate = self;
    if ([self ReachabilityPass]) {
        [webApis getSensorStations];
    }else{
        [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
    }
    
}
-(void)DidReceiveSendSensorDatainfo:(id)respond{
    [self hudHide];
    if (!statesArray)statesArray = [[NSMutableArray alloc]init];
    [statesArray removeAllObjects];
    if (!stationListArray)stationListArray = [[NSMutableArray alloc]init];
    [stationListArray removeAllObjects];
    
    if (!referanceStationArray)referanceStationArray = [[NSMutableArray alloc]init];
    [referanceStationArray removeAllObjects];

    if (!recipes)recipes = [[NSMutableArray alloc]init];
    [recipes removeAllObjects];
    
    if (!allRefStationsName)allRefStationsName = [[NSMutableArray alloc]init];
    [allRefStationsName removeAllObjects];
    
    //Whether stations
    for (NSDictionary * dataDic in [respond objectForKey:@"data_stations"]) {
        NSString * stationName = [dataDic objectForKey:@"stationName"];
        NSArray * locations = [dataDic objectForKey:@"subStations"];
       NSMutableArray * stationArray = [[NSMutableArray alloc]init];
        [stationArray removeAllObjects];
        //Wheter statons
        NSDictionary * dataStations = @{@"Type" :@"topic",
                                        @"name" :stationName};
        [whetherSataionsArray addObject:dataStations];
        [statesArray addObject:dataStations];
        
        for (NSString * selectedLocation in locations) {
            NSDictionary * datalocation = @{@"Type" :@"details",
                                            @"name" :selectedLocation};
            [stationArray addObject:datalocation];
        }
        [stationListArray addObject:stationArray];
    }
    
    //Referance stations
    for (NSDictionary * dataDic in [respond objectForKey:@"data_ref_stations"]) {
        NSString * stationName = [dataDic objectForKey:@"stationName"];
        NSArray * locations = [dataDic objectForKey:@"subStations"];
        //Wheter statons
        NSDictionary * dataStations = @{@"Type" :@"topic",
                                        @"name" :stationName};
        [referanceStationArray addObject:dataStations];
        
        for (NSString * selectedLocation in locations) {
            NSDictionary * datalocation = @{@"Type" :@"details",
                                            @"name" :selectedLocation};
            [referanceStationArray addObject:datalocation];
            [allRefStationsName addObject:selectedLocation];
        }
    }
    
    [self setStation:4];
}

- (IBAction)btnSelectStateClicks:(id)sender {
    if (statesArray.count > 0) {
        isStateViewActive = true;
        whetherStationsView.hidden = false;
        [statusTabel reloadData];
    }else{
        [self ShowAlert:@"Sorry!" message:@"No states found!"];
    }
}
-(void)setStation :(int)statusIndex{
    
    //Set ButtonTitle
    NSDictionary * selectedDic = [statesArray objectAtIndex:statusIndex];
    NSString * selectedState = [selectedDic objectForKey:@"name"];
    [btnSelectStatus setTitle:selectedState forState:UIControlStateNormal];
    
    //Set stations List
    if (!whetherSataionsArray)whetherSataionsArray = [[NSMutableArray alloc]init];
    [whetherSataionsArray removeAllObjects];
    
    //Add refStations
    NSDictionary * datalocation = @{@"Type" :@"section",
                                    @"name" :@"Reference Stations"};
    [whetherSataionsArray addObject:datalocation];
    [whetherSataionsArray addObjectsFromArray:referanceStationArray];
    
    //SubStations

    NSDictionary * datalocation_2 = @{@"Type" :@"section",
                                    @"name" :@"Weather Stations"};
    [whetherSataionsArray addObject:datalocation_2];
    if (stationListArray.count > 0) {
        if ([[stationListArray objectAtIndex:statusIndex] count] > 0) {
            [whetherSataionsArray addObjectsFromArray:[stationListArray objectAtIndex:statusIndex]];
        }else{
            [self ShowAlert:@"Sorry!" message:@"No stations for this state."];
        }
    }
    

    [WeatherStationTable reloadData];
}
//TabelData
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (isStateViewActive) {
         return statesArray.count;
    }else{
        if (isFiltered) {
            return filteredStations.count;
        }else{
            return whetherSataionsArray.count;
        }
    }
}
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (isStateViewActive) {
        static NSString *simpleTableIdentifier = @"StateCell";
        StateCell *cell = (StateCell *)[theTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StateCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
       NSDictionary * selectedDic = [statesArray objectAtIndex:indexPath.row];
        cell.stateNameTL.text = [selectedDic objectForKey:@"name"];
        return cell;
    }else{
        NSDictionary * selectedDic = nil;
        if (isFiltered) {
            selectedDic = [filteredStations objectAtIndex:indexPath.row];
        }else{
            selectedDic = [whetherSataionsArray objectAtIndex:indexPath.row];
        }
        
        
        if ([[selectedDic objectForKey:@"Type"] isEqualToString:@"section"]) {
            static NSString *simpleTableIdentifier = @"SectionCell";
            SectionCell *cell = (SectionCell *)[theTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SectionCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.sectionNameTL.text = [selectedDic objectForKey:@"name"];
            
            return cell;
        }else if ([[selectedDic objectForKey:@"Type"] isEqualToString:@"topic"]) {
            static NSString *simpleTableIdentifier = @"TopicTableViewCell";
            TopicTableViewCell *cell = (TopicTableViewCell *)[theTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TopicTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            
            NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
            cell.textLabel.attributedText = [[NSAttributedString alloc] initWithString:[selectedDic objectForKey:@"name"]
                                                                            attributes:underlineAttribute];
            
            
            return cell;
        }else{
            static NSString *simpleTableIdentifier = @"StationsTableViewCell";
            StationsTableViewCell *cell = (StationsTableViewCell *)[theTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"StationsTableViewCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.delegate = self;
            cell.tag = indexPath.row;
            NSString * sationName = [selectedDic objectForKey:@"name"];
            cell.textLabel.text = sationName;
            
            if ([selectedIndexArray containsObject:sationName]) // YES
            {
                [cell selectCheckBox:true];
            }else{
                [cell selectCheckBox:false];
            }
            
            
            return cell;
        }
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (isStateViewActive) {
        isStateViewActive = false;
        whetherStationsView.hidden = true;
        [self setStation:(int)indexPath.row];
    }
}
- (IBAction)btnDoneClicks:(id)sender {
    
    if (isDateTimePicked) {
//        DatePickerViewController * dateController = [self.storyboard instantiateViewControllerWithIdentifier:@"DatePickerViewController"];
//        [self presentViewController:dateController animated:true completion:nil];
        [self dateTimePassed];
    }else{
        [self ShowAlert:@"Sorry!" message:@"Weather Sations not selected"];
    }
}
-(void)didMarkCheckboxAtIndex:(NSString *)subStationName IsCheck:(BOOL)ischeck{
    
    if ([selectedIndexArray containsObject:subStationName]) {
        [selectedIndexArray removeObject:subStationName];
        [WeatherStationTable reloadData];
    }else{
        if (selectedIndexArray.count == 2) {
            [selectedIndexArray removeObjectAtIndex:0];
        }
        [selectedIndexArray addObject:subStationName];
        [WeatherStationTable reloadData];
    }
    
    [AppUserDefaults setReviewResultArray:selectedIndexArray];
    
    
    if (selectedIndexArray.count == 0) {
        isDateTimePicked = false;
    }else{
        isDateTimePicked = true;
    }
}
//-(void)didMarkCheckboxAtIndex:(NSInteger)tag IsCheck:(BOOL)ischeck {
//
//    if ([selectedIndexArray containsObject:[NSNumber numberWithInteger:tag]]) {
//        [selectedIndexArray removeObject:[NSNumber numberWithInteger:tag]];
//        [WeatherStationTable reloadData];
//    }else{
//        if (selectedIndexArray.count == 2) {
//            [selectedIndexArray removeObjectAtIndex:0];
//        }
//        [selectedIndexArray addObject:[NSNumber numberWithInteger:tag]];
//        [WeatherStationTable reloadData];
//    }
//
//    [AppUserDefaults setReviewResultArray:selectedIndexArray];
//
//
//    if (selectedIndexArray.count == 0) {
//        isDateTimePicked = false;
//    }else{
//        isDateTimePicked = true;
//    }
//}
#pragma mark - SearchView
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(nonnull NSString *)searchText{
    if (searchText.length == 0) {
        isFiltered = false;
        [reviewSearch endEditing:YES];
    }
    else {
        isFiltered = true;
        filteredStations = [[NSMutableArray alloc]init];
        
        for (NSDictionary * searchDic in whetherSataionsArray) {
            if ([[searchDic objectForKey:@"Type"] isEqualToString:@"section"]) {
                [filteredStations addObject:searchDic];
            }else if ([[searchDic objectForKey:@"Type"] isEqualToString:@"topic"]) {
                [filteredStations addObject:searchDic];
            }else{
                if ([[searchDic objectForKey:@"name"] rangeOfString:searchText options:NSCaseInsensitiveSearch].location == NSNotFound) {
//                    NSLog(@"string does not contain bla");
                } else {
                     [filteredStations addObject:searchDic];
                }
            }
        }
    }
    [WeatherStationTable reloadData];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [reviewSearch endEditing:YES];
}

#pragma mark - RequestForGetStationValues
-(void)dateTimePassed{
//    NSLog(@"dateTimePassed");
    NSString * fromDate = [AppUserDefaults getFromDate];
    NSString * todate = [NSString stringWithFormat:@"%@",[self getPresentDateTime]];
    NSMutableArray * dataArray = [[NSMutableArray alloc]init];
    NSMutableArray * refStationsArray = [[NSMutableArray alloc]init];
    //RefStations
    for (NSString * selectedStation in selectedIndexArray) {
        if ([allRefStationsName containsObject:selectedStation]) {
            [refStationsArray addObject:selectedStation];
//            [selectedIndexArray removeObject:selectedRefStation];
        }else{
            [dataArray addObject:selectedStation];
        }
    }
    if (refStationsArray.count == 0) {
        [refStationsArray addObject:@""];
    }
    if (dataArray.count == 0) {
        [dataArray addObject:@""];
    }
    NSString * refStationsString = [self stringFromArray:refStationsArray];
    
    //mainStatins
//    switch (selectedIndexArray.count) {
//        case 0:
//            [dataArray addObject:@""];
//            break;
//        case 1:
//            [dataArray addObject:[selectedIndexArray firstObject]];
//            break;
//        case 2:
//            [dataArray addObject:[selectedIndexArray firstObject]];
//            [dataArray addObject:[selectedIndexArray lastObject]];
//            break;
//
//        default:
//            break;
//    }
    NSString * arrayString = [self stringFromArray:dataArray];
    
    if (([fromDate isKindOfClass:[NSNull class]])||(fromDate == nil)) {
        //Message
    }else{
        
        NSDictionary * requestData  = @{@"stations" :arrayString,
                                        @"ref_stations":refStationsString,
                                        @"date_from":fromDate,
                                        @"timezone" :[self getCurrntTimeZone],
                                        @"date_to":todate};
        NSLog(@"sending %@",requestData);
        
        [self inithud:@"Wait"];
        if (!webApis)webApis = [[WebserviceApi alloc]init];
        webApis.delegate = self;
        if ([self ReachabilityPass]) {
            [webApis getSensorStationsData:requestData];
        }else{
            [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
        }
    }
}
-(NSString *)getCurrntTimeZone {
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSLog(@"TimeZone %@",destinationTimeZone.name);
    NSString * timeZoneString = destinationTimeZone.name;
    if (timeZoneString != nil) {
        return timeZoneString;
    }else{
        return @"Asia/Colombo";
    }
}
//-(void)requestForGetStationData:(NSArray *)indexes {
//
//    if (indexes.count == 2) {
//        NSDictionary * firstStationDic = [whetherSataionsArray objectAtIndex:[[indexes firstObject] intValue]] ;
//        NSDictionary * secondStationDic = [whetherSataionsArray objectAtIndex:[[indexes lastObject] intValue]];
//
//        firstStation = [firstStationDic objectForKey:@"name"];
//        secondStaion = [secondStationDic objectForKey:@"name"];
//    }else{
//        NSDictionary * firstStationDic = [whetherSataionsArray objectAtIndex:[[indexes firstObject] intValue]] ;
//        firstStation = [firstStationDic objectForKey:@"name"];
//    }
//}
-(NSString *)stringFromArray :(NSArray *)stations{
    NSString* json = nil;
    
    NSError* error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:stations options:NSJSONWritingPrettyPrinted error:&error];
    json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return json;
}
-(void)DidReceiveSensorStationsData:(id)respond {
    [self hudHide];
    
    if ([[respond objectForKey:@"code"] intValue] != 1 ) {
        [self ShowAlert:@"Sorry!" message:@"No data availabel."];
    }else{
        NSDictionary * dataDic = [respond objectForKey:@"data"];
        
        NSDictionary * deviceData = [dataDic objectForKey:@"device_data"];
        NSDictionary * stationData = [[dataDic objectForKey:@"station_data"] firstObject];
        NSDictionary * stationData_2 = nil;
        if ([[dataDic objectForKey:@"station_data"] count] > 1) {
           stationData_2 = [[dataDic objectForKey:@"station_data"] lastObject];
        }
        
        if ((deviceData.count > 0) || (stationData.count >0)) {
            ReviewResultsViewController  * reviewGrafh = [self.storyboard instantiateViewControllerWithIdentifier:@"ReviewResultsViewController"];
            reviewGrafh.deviceData = deviceData;
            reviewGrafh.stationData = stationData;
            reviewGrafh.stationData_2 = stationData_2;
            [self.navigationController pushViewController:reviewGrafh animated:true];
        }
    }
}
#pragma mark - inner view functions

- (IBAction)btninnerviewCancelClicks:(id)sender {
    whetherStationsView.hidden = true;
    isStateViewActive = false;
}
@end
