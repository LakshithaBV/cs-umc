//
//  StationsTableViewCell.h
//  CS-UMC
//
//  Created by SCIT on 6/22/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StationsCellDelegate <NSObject>
@optional
- (void)didMarkCheckboxAtIndex :(NSString *)subStationName IsCheck:(BOOL)ischeck;
@end

@interface StationsTableViewCell : UITableViewCell{
    
    __weak IBOutlet UIImageView *checkBoxIV;
    BOOL isCheck;
}
@property (nonatomic, weak) id <StationsCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (nonatomic,assign)NSNumber * myTag;

-(void)selectCheckBox :(BOOL)isCheck ;
@end
