//
//  TopicTableViewCell.h
//  CS-UMC
//
//  Created by SCIT on 6/22/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicTableViewCell : UITableViewCell {
    
}
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
