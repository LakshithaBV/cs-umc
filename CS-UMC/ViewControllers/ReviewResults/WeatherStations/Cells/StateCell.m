//
//  StateCell.m
//  CS-UMC
//
//  Created by SCIT on 9/19/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "StateCell.h"

@implementation StateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
