//
//  SectionCell.h
//  CS-UMC
//
//  Created by SCIT on 9/21/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *sectionNameTL;
@end
