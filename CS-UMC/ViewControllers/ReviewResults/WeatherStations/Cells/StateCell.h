//
//  StateCell.h
//  CS-UMC
//
//  Created by SCIT on 9/19/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StateCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *stateNameTL;

@end
