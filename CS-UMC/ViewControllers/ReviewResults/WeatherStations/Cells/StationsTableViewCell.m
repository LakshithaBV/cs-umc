//
//  StationsTableViewCell.m
//  CS-UMC
//
//  Created by SCIT on 6/22/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "StationsTableViewCell.h"

@implementation StationsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    isCheck = false;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnCheckBoxClicks:(id)sender {
    
    if (isCheck) {
//        checkBoxIV.image = [UIImage imageNamed:@"RR_check_box_empty"];
        isCheck = false;
    }else{
//        checkBoxIV.image = [UIImage imageNamed:@"RR_check_box"];
        isCheck = true;
    }
    [self.delegate didMarkCheckboxAtIndex:self.textLabel.text IsCheck:isCheck];
}
-(void)selectCheckBox :(BOOL)isCheck {
    
    if (isCheck) {
        checkBoxIV.image = [UIImage imageNamed:@"RR_check_box"];
    }else{
        checkBoxIV.image = [UIImage imageNamed:@"RR_check_box_empty"];
    }
}

@end
