//
//  WeatherStationsViewController.h
//  CS-UMC
//
//  Created by SCIT on 6/22/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "StationsTableViewCell.h"
#import "AppUserDefaults.h"
#import "ReviewResultsViewController.h"
#import "WebserviceApi.h"
#import "MasterViewController.h"
#import "DatePickerViewController.h"
#import "SCUMCButton.h"
#import "StateCell.h"
#import "SectionCell.h"
@interface WeatherStationsViewController : MasterViewController<UITableViewDelegate,UITableViewDataSource,StationsCellDelegate,webservicedelegate,UISearchBarDelegate>{
    NSMutableArray * whetherSataionsArray;
    __weak IBOutlet UITableView *WeatherStationTable;
    __weak IBOutlet UITableView *statusTabel;
    __weak IBOutlet UIButton *btnMenu;
    NSMutableArray * selectedIndexArray;
    
    WebserviceApi * webApis;
    __weak IBOutlet UISearchBar *reviewSearch;
    NSMutableArray *recipes;
    NSArray *searchResults;
    NSMutableArray *selectedStations;
    NSMutableArray *filteredStations;
    BOOL isStationsTabels;
    BOOL isFiltered;
    BOOL isDateTimePicked;
    BOOL isTyping;
    BOOL isStateViewActive;
    
    NSString * firstStation;
    NSString * secondStaion;
    
    NSMutableArray *statesArray;
    NSMutableArray *stationListArray;
    NSMutableArray *referanceStationArray;
    NSMutableArray *allRefStationsName;
    
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
    __weak IBOutlet SCUMCButton *btnSelectStatus;
    __weak IBOutlet UIView *whetherStationsView;
    __weak IBOutlet UIView *whetherStationInnerView;
    
    
}

@end
