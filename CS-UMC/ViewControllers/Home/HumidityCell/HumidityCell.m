//
//  HumidityCell.m
//  CS-UMC
//
//  Created by SCIT on 5/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "HumidityCell.h"
#import "ChatManager.h"
#import "Constants.h"
#import "AAChartModel.h"
#import "AppUserDefaults.h"

@implementation HumidityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(temperatureNotifications)
                                                 name:TEMPRETURE_SAVED_NOTITFICATIONS
                                               object:nil];
    
    [self loadChartData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)loadChartData{
    
    chartManager = [[ChatManager alloc] init];
    chartView = [chartManager getHymidityChart];
    
    CGFloat chartViewWidth  = self.frame.size.width - 47;
    
    if (chartView.chartModel.pointCount > 10) {
        int overloadedCount = (int)(chartView.chartModel.pointCount - 10);
        chartViewWidth = (chartViewWidth + overloadedCount * 10);
        
    }
    chartView.frame = CGRectMake(0,0, chartViewWidth +50, self.frame.size.height +50);
    [chartView setDelegate:self];
    chartView.scrollEnabled = NO;
    chartView.backgroundColor = UIColor.clearColor;
    chartScroller.contentSize = chartView.frame.size;
    [self manageChartWidth:(int)chartView.chartModel.pointCount];
    [chartScroller addSubview:chartView];
//    [self scrollToRight];
    
    
}
-(void)temperatureNotifications {
    NSLog(@"Refresh Data");
    AAChartModel * chartModel = [[ChatManager sharedInstance] updateHymidityChartWithData];
    chartModel.symbol = AAChartSymbolTypeCircle;
    [self manageChartWidth:(int)chartModel.pointCount];
    [chartView aa_refreshChartWithChartModel:chartModel];
}
-(void)manageChartWidth :(int)pointCount {
    int recordCount = pointCount;
    
    if (recordCount > 10) {
        NSLog(@"Overlap");
        int overloadedCount = (recordCount - 10);
        CGRect chartFrame = chartView.frame;
        chartFrame.size.width = (chartFrame.size.width + (recordCount * 2));
        [chartView setFrame:chartFrame];
        chartScroller.contentSize = chartView.frame.size;
//        [self scrollToRight];
    }
}
-(void)scrollToRight{
    float w = chartScroller.frame.size.width;
    float h = chartScroller.frame.size.height;
    float newPosition = chartScroller.contentOffset.x+w;
    CGRect toVisible = CGRectMake(newPosition, 0, w, h);
    
    [chartScroller scrollRectToVisible:toVisible animated:YES];
}
#pragma mark -- AAChartView delegate
- (void)AAChartViewDidFinishLoad {
    NSLog(@"🔥🔥🔥🔥🔥 AAChartView content did finish load!!!");
}
@end
