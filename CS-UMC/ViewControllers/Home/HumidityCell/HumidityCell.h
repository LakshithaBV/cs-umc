//
//  HumidityCell.h
//  CS-UMC
//
//  Created by SCIT on 5/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatManager.h"
#import "AAChartView.h"

@interface HumidityCell : UITableViewCell <AAChartViewDidFinishLoadDelegate>{
    ChatManager * chartManager;
    AAChartView * chartView;
    __weak IBOutlet UIScrollView *chartScroller;
}

@end
