//
//  HomeViewController.h
//  CS-UMC
//
//  Created by SCIT on 1/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "BTDeviceManager.h"
#import "WebserviceApi.h"
#import "MasterViewController.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "AfterCell.h"
#import "BeforeCell.h"
#import "WebserviceApi.h"
#import "DBManager.h"


@interface HomeViewController : MasterViewController<BTDeviceManagerDelegate,webservicedelegate,UITableViewDelegate,UITableViewDataSource,AfterCellDelegate,BeforeCellDelegate>{
    
    __weak IBOutlet UIBarButtonItem *menubutton;
    __weak IBOutlet UIButton *btnMenu;
    
    __weak IBOutlet UIView *fontView;
    __weak IBOutlet UITextView *sensdataShowTF;
    
    
    __weak IBOutlet UILabel *deviceStatusTL;
    NSString * feedString;
    BTDeviceManager * devicemanager;
    DBManager * dbManager;
    
    CGFloat firstViewHeight;
    
    
    __weak IBOutlet UITableView *tableHome;
    __weak IBOutlet UILabel *BTDeviceNameTL;
    
    
    BOOL isDeviceSetup;
    BOOL isDataRecording;
    __weak IBOutlet UIView *manageDataView;
    __weak IBOutlet UIView *manageDataInnerView;
    
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
    WebserviceApi * webapi;
    
    NSDate * startDate;
    NSDate * endDate;
}

@end
