//
//  HomeViewController.m
//  CS-UMC
//
//  Created by SCIT on 1/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "HomeViewController.h"
#import "BeforeCell.h"
#import "AirTeperatureCell.h"
#import "HumidityCell.h"
#import "ChatManager.h"
#import "AppUserDefaults.h"
#import "SeachDeviceViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
     [btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    feedString = [[NSString alloc]init];
    deviceStatusTL.backgroundColor = [UIColor redColor];
    BTDeviceNameTL.text = [NSString stringWithFormat:@"Device %@", [AppUserDefaults getConnectedPeripheralName]];
    
    isDeviceSetup = [AppUserDefaults getisEnvironmentSetup];
    isDataRecording = [AppUserDefaults getIsRecordingOn];
    firstViewHeight = 160.0f;
    [tableHome reloadData];
    manageDataView.hidden = true;
    
//    [self btnConnect:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoLogout)
                                                 name:LOGOUT_NOTITFICATIONS
                                               object:nil];
//    [AppUserDefaults setisLogout:false];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [tableHome reloadData];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnMenuButtonClicks:(id)sender {
    SWRevealViewController *revealController = [self revealViewController];
//    [revealController bacb]
    [revealController revealToggle:nil];
}
- (IBAction)btnConnect:(id)sender {
    
    devicemanager = [BTDeviceManager sharedInstance];
    devicemanager.delegate = self;
    deviceStatusTL.text = @"Searching...";
    deviceStatusTL.backgroundColor = [UIColor yellowColor];
    [devicemanager connect];
}

-(void)scrollTextViewToBottom{
    if(sensdataShowTF.text.length > 0 ) {
        NSRange bottom = NSMakeRange(sensdataShowTF.text.length -1, 1);
        [sensdataShowTF scrollRangeToVisible:bottom];
    }
}
-(void)didSelectedDeviceFromSearch:(NSArray *)devices{
    
}

#pragma mark MARKS
#pragma mark -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return firstViewHeight;
    }else {
        CGFloat viewHeight = self.view.frame.size.height - (firstViewHeight +115);
        return viewHeight/2;
    }
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        if (isDeviceSetup) {
            
            static NSString *simpleTableIdentifier = @"AfterCell";
            AfterCell *cell = (AfterCell *)[tableHome dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AfterCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.backBroundTextView.layer.cornerRadius = 2.0;
            cell.backBroundTextView.layer.masksToBounds = true;
            
            cell.btnRetrivedData.layer.cornerRadius = 2.0;
            cell.btnRetrivedData.layer.masksToBounds = true;
            cell.delegate = self;
            return cell;
        }else{
            static NSString *simpleTableIdentifier = @"BeforeCell";
            BeforeCell *cell = (BeforeCell *)[tableHome dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            if (cell == nil)
            {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BeforeCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
            }
            cell.delegate = self;
            return cell;
        }
    }else if (indexPath.row == 1){
        static NSString *simpleTableIdentifier = @"AirTeperatureCell";
        AirTeperatureCell *cell = (AirTeperatureCell *)[tableHome dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AirTeperatureCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        return cell;
    }else {

        static NSString *simpleTableIdentifier = @"HumidityCell";
        HumidityCell *cell = (HumidityCell *)[tableHome dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"HumidityCell" owner:self options:nil];
            cell = [nib objectAtIndex:0];
        }
        
        return cell;
        
    }
}
#pragma mark -
#pragma mark Auto logout with device disconnect

-(void)autoLogout {
    SeachDeviceViewController * searchdevice = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
    [self.navigationController pushViewController:searchdevice animated:true];
}
#pragma mark -
#pragma mark Retrive Data Action

-(void)didSelectRetriveData {
    manageDataView.hidden = false;
}
- (IBAction)btnSetupandcollectdataClicks:(id)sender {
    manageDataView.hidden = true;
    [self retrivingData];
}
- (IBAction)btnStopanddiscarddataClicks:(id)sender {
    [self showDiscardDataAlert];
}
- (IBAction)btnDoNothingClicks:(id)sender {
    manageDataView.hidden = true;
}
-(void)retrivingData{
    BTDeviceManager * device_manager = [BTDeviceManager sharedInstance];
    device_manager.delegate = self;
    [device_manager retriveSensorData];
    [self inithud:@"retrieve data from Sensor."];
}
-(void)didFinishRetriveData:(BOOL)isSuccess :(NSArray *)historyData{
    if (isSuccess) {
        [self hudHide];
        [self sendBulkData:historyData];
        NSLog(@"Log Array %@",historyData);
    }
}
-(void)sendBulkData :(NSArray *)data_array{
    if (data_array.count >0) {
        [self setFromTime:data_array];
        NSString * dataString = [[BTDeviceManager sharedInstance] stringFromArray:data_array];
        if (dataString.length > 0) {
            [self inithud:@"sending data.."];
            if (!webapi)webapi = [[WebserviceApi alloc]init];
            webapi.delegate = self;
            int rowId = (int)[AppUserDefaults getRowId];
            NSString * rowidString = [NSString stringWithFormat:@"%i",rowId];
            NSDictionary * requestDic = @{@"dataset":dataString,
                                          @"location_row_id":rowidString};
            NSLog(@"dataDic %@",requestDic);
            if ([self ReachabilityPass]) {
                [webapi sendBulkData:requestDic];
            }else{
                [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
            }
        }else{
            [self ShowAlert:@"Sorry!" message:NSLocalizedString(NOSENSORDATA, @"")];
        }
    }else{
        [self ShowAlert:@"Sorry!" message:NSLocalizedString(NOSENSORDATA, @"")];
    }
}
-(void)setFromTime :(NSArray *)fromArray{
    NSDictionary * dataDic = [fromArray lastObject];
    NSString * lastRecDate = [dataDic objectForKey:@"dateTime"];
    [AppUserDefaults setFromDate:lastRecDate];
}
-(NSString *)getDateTimeString :(NSDate *)mydate{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:mydate];
}
-(void)DidReceiveBulkDataInfo:(id)respond{
    [self hudHide];
    NSLog(@"respond = %@",respond);
    if ([[respond objectForKey:@"code"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
        [self initMessageOnlyhud:@"Data submitted successfully"];
        if (!dbManager)dbManager = [[DBManager alloc]init];
        [dbManager deleteAllRecords];
        [AppUserDefaults removeEnvironmentRecords];
//        [AppUserDefaults setFromDate:[self getDateTimeString:startDate]]; //from Date to review Results.
        [self navigateToReviewResults];
//        [self showAlert];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(NSDate *)stringToDate :(NSString *)mydate{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter dateFromString:mydate];
}

-(void)navigateToReviewResults{
    SWRevealViewController * revealController = self.revealViewController;
    UIViewController * newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"WeatherStationsViewController"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    [revealController pushFrontViewController:navigationController animated:YES];
}
-(void)didClickSetupdateNow{
    SWRevealViewController * revealController = self.revealViewController;
    UIViewController * newFrontController = [self.storyboard instantiateViewControllerWithIdentifier:@"SetupDetailsViewController"];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:newFrontController];
    navigationController.navigationBarHidden = true;
    [revealController pushFrontViewController:navigationController animated:YES];
}
//Alerts
#pragma mark -
#pragma mark Alerts
-(void)showAlert{
    NSDate* date1 = startDate;
    NSDate* date2 = endDate;
    NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date1];
    double secondsInAnHour = 3600;
    NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
    
    div_t h = div(distanceBetweenDates, 3600);
    int hours = h.quot;
    
    div_t m = div(h.rem, 60);
    int minutes = m.quot;
    int seconds = m.rem;
    
    NSString * timeString = @"";
    
    if (hours != 0) {
        timeString = [NSString stringWithFormat:@"%dhrs ",hours];
    }
    if (minutes != 0){
        timeString = [NSString stringWithFormat:@"%@ %dmin ",timeString,minutes];
    }
    if (seconds != 0){
        timeString = [NSString stringWithFormat:@"%@ %dsec ",timeString,seconds];
    }
    
    //    NSLog(@"%d:%d:%d", hours, minutes, seconds);
    NSString * title = @"Done!";
    NSString * message = [NSString stringWithFormat:@"%@ of data have been collected from the sensor",timeString];
    [self showLocalAlert:title message:message];
}
-(void)showLocalAlert :(NSString *)title message:(NSString *)msg{
    
    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:title
                                                                               message: msg
                                                                        preferredStyle:UIAlertControllerStyleAlert                   ];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"OK"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [self navigateToReviewResults];
                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    [myAlertController addAction: ok];
    [self presentViewController:myAlertController animated:YES completion:nil];
}
-(void)showDiscardDataAlert{
    
    UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:@"Info"
                                                                               message: @"Are you sure to discard data ?"
                                                                        preferredStyle:UIAlertControllerStyleAlert                   ];
    
    //Step 2: Create a UIAlertAction that can be added to the alert
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"YES"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here, eg dismiss the alertwindow
                             
                             [self removeAndDiscradData];
                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* no = [UIAlertAction
                         actionWithTitle:@"NO"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here, eg dismiss the alertwindow
                             [myAlertController dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    
    //Step 3: Add the UIAlertAction ok that we just created to our AlertController
    [myAlertController addAction: ok];
    [myAlertController addAction: no];
    
    //Step 4: Present the alert to the user
    [self presentViewController:myAlertController animated:YES completion:nil];
}
-(void)removeAndDiscradData{
    manageDataView.hidden = true;
    if (!dbManager)dbManager = [[DBManager alloc]init];
    [dbManager deleteAllRecords];
    [AppUserDefaults removeEnvironmentRecords];
    [tableHome reloadData];
}
@end
