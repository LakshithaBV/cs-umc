//
//  AfterCell.m
//  CS-UMC
//
//  Created by SCIT on 5/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "AfterCell.h"

@implementation AfterCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self messageProcess];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messageProcess)
                                                 name:TEMPRETURE_SAVED_NOTITFICATIONS
                                               object:nil];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnRetriveDataClicks:(id)sender {
    [self.delegate didSelectRetriveData];
}
-(void)messageProcess {
    
    DBManager * dbm = [[DBManager alloc]init];
    NSArray * recordsArr = [dbm getTemperatureData];
    NSString * message = [NSString stringWithFormat:@"(one measurement every 5 mins) The sensor has recorded %i measurements.You should aim for at lease 8 hours.",(int)[recordsArr count]];
    self.messageTL.text = message;
}
@end
