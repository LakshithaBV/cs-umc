//
//  AfterCell.h
//  CS-UMC
//
//  Created by SCIT on 5/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@protocol AfterCellDelegate <NSObject>

@optional
-(void) didSelectRetriveData;
@end

@interface AfterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *backBroundTextView;
@property (weak, nonatomic) IBOutlet UIButton *btnRetrivedData;
@property (weak, nonatomic) IBOutlet UILabel *messageTL;

@property id<AfterCellDelegate> delegate;
@end
