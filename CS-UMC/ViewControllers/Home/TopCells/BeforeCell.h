//
//  BeforeCell.h
//  CS-UMC
//
//  Created by SCIT on 5/4/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BeforeCellDelegate <NSObject>

@optional
-(void) didClickSetupdateNow;
@end


@interface BeforeCell : UITableViewCell
@property id<BeforeCellDelegate> delegate;
@end
