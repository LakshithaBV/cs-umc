//
//  InfoViewController.m
//  CS-UMC
//
//  Created by SCIT on 4/26/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "InfoViewController.h"
#import "SeachDeviceViewController.h"

@interface InfoViewController ()

@end

@implementation InfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnOkClicks:(id)sender {
    SeachDeviceViewController * serchDevicesView = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
    [self.navigationController pushViewController:serchDevicesView animated:YES];
}

-(void)jumpTOMenu{
    dispatch_async(dispatch_get_main_queue(), ^{
        SWRevealViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    });
}

@end
