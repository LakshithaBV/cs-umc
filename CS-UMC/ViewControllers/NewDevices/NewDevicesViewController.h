//
//  NewDevicesViewController.h
//  CS-UMC
//
//  Created by SCIT on 5/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "BTDeviceManager.h"
#import "Reachability.h"
#import "AppLocationmanager.h"
#import "AppUserDefaults.h"
#import "SensorLocationViewController.h"
#import "WebserviceApi.h"
#import "BTDeviceManager.h"
#import "DevicesCell.h"
#import "AppLocationmanager.h"
#import "RegistrationViewController.h"
#import "DBManager.h"

@interface NewDevicesViewController : MasterViewController{
    __weak IBOutlet UIButton *btnMenu;
    __weak IBOutlet UILabel *DeviceNameTL;
    
    __weak IBOutlet UIButton *btnYes;
    __weak IBOutlet UIButton *btnBo;
    
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
}

@end
