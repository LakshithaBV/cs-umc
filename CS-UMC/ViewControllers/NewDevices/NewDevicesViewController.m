//
//  NewDevicesViewController.m
//  CS-UMC
//
//  Created by SCIT on 5/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "NewDevicesViewController.h"
#import "HomeViewController.h"
#import "BTDeviceManager.h"
#import "SeachDeviceViewController.h"

@interface NewDevicesViewController ()

@end

@implementation NewDevicesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    DeviceNameTL.text = [AppUserDefaults getConnectedPeripheralName];
    btnYes.layer.cornerRadius = 5.0f;
    btnYes.layer.masksToBounds = true;
    
    btnBo.layer.cornerRadius = 5.0f;
    btnBo.layer.masksToBounds = true;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnYesClicks:(id)sender {
    NSLog(@"Logout_5");
    [[BTDeviceManager sharedInstance] disconnect];
    SeachDeviceViewController * searchview = [self.storyboard instantiateViewControllerWithIdentifier:@"SeachDeviceViewController"];
    [self.navigationController pushViewController:searchview animated:YES];
}
- (IBAction)btnNoClicks:(id)sender {
    HomeViewController * home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:home animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:true];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
