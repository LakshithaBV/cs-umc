//
//  RegistrationViewController.h
//  CS-UMC
//
//  Created by SCIT on 1/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MasterViewController.h"
#import "AppUserDefaults.h"
#import "SWRevealViewController.h"
#import "SensorLocationViewController.h"
#import "SeachDeviceViewController.h"
#import "WebserviceApi.h"

@interface RegistrationViewController : MasterViewController<webservicedelegate>{
    
    __weak IBOutlet UILabel *deviceIdTL;
    
    __weak IBOutlet UITextField *nameTF;
    __weak IBOutlet UITextField *emailTF;
    __weak IBOutlet UITextField *phoneTF;
    __weak IBOutlet UIButton *btnCheck;
    
    BOOL isCheck;
    
    __weak IBOutlet UILabel *emailCheckTL;
    NSDictionary * payload;
    __weak IBOutlet NSLayoutConstraint *navigationHeight;
    
}

@end
