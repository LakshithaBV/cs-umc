//
//  RegistrationViewController.m
//  CS-UMC
//
//  Created by SCIT on 1/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "RegistrationViewController.h"
#import "SWRevealViewController.h"
@interface RegistrationViewController ()

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    deviceIdTL.text = [NSString stringWithFormat:@"Device %@",[AppUserDefaults getConnectedPeripheralName]];
    [self.navigationItem setHidesBackButton:YES];
    isCheck = NO;
    emailCheckTL.hidden = true;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkRegistrationAgain)
                                                 name:REGISTRATION_CHECK_NOTITFICATIONS
                                               object:nil];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:true];
    if([self isiphoneWithSafeArea]){
        navigationHeight.constant = 88.0;
    }
}
- (IBAction)btnBackClicks:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}
-(void)checkRegistrationAgain{
    
    NSDictionary * mypayload = [AppUserDefaults getRegisterPayload];
    
    if (mypayload != nil) {
        [self inithud:@"Verifying..."];
        WebserviceApi * webcall = [WebserviceApi sharedInstance];
        webcall.delegate = self;
        if ([self ReachabilityPass]) {
           [webcall Register:mypayload];
        }else{
            [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
        }
    }
    NSLog(@"reg test");
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnRegisterClicks:(id)sender {
    
    NSString * name = nameTF.text;
    if (name.length == 0)name = @" ";
    emailCheckTL.hidden = true;

    NSString * email = emailTF.text;
    NSString * phone = phoneTF.text;
    if (phone.length == 0)phone = @" ";
    NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString * sensorname = [AppUserDefaults getConnectedPeripheralName];
    
    if ((email.length > 0) && (sensorname.length > 5)) {
        if ([self validateEmail:email]) {
            
            if (isCheck) {
                //Server call
                
                WebserviceApi * webcall = [WebserviceApi sharedInstance];
                webcall.delegate = self;
                
                payload =  @{@"name" :name,
                             @"email" :email,
                             @"device_id" : Identifier,
                             @"type" :@"IOS",
                             @"sensor_name" :sensorname,
                             };
                
                if ([self ReachabilityPass]) {
                    [webcall Register:payload];
                }else{
                    [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
                }
            }else{
                [self ShowAlert:@"Sorry!" message:@"Please agree T&C."];
            }
        }else{
            [self ShowAlert:@"Oops!" message:@"Email Addess Invalid."];
        }
    }else{
        [self ShowAlert:@"Oops!" message:@"Fill All Fields"];
    }
}
-(void)DidReceiveRegisterinfo:(id)respond{
    NSLog(@"respond %@",respond);
    [self hudHide];
    
    if ([[respond objectForKey:@"code"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
        if ([respond objectForKey:@"data"]!=nil) {
            [AppUserDefaults setUserdata:[respond objectForKey:@"data"]];
            BOOL isVerified = [AppUserDefaults getVarified];
            if (isVerified) {
                emailCheckTL.hidden = true;
                [AppUserDefaults removeRegisterPayload];
                [self jumpToMenu];
                
                BOOL isEnvSet = [[respond objectForKey:@"env_exist"] boolValue];
                [AppUserDefaults setisEnvironmentSetup:isEnvSet];
                int locationRowId = [[respond objectForKey:@"location_row_id"] intValue];
                [AppUserDefaults setRowId:locationRowId];
            }else{
                if (payload!= nil) {
                    [AppUserDefaults setRegisterPayload:payload];
                }
                emailCheckTL.hidden = false;
            }
        }
    }else if ([[respond objectForKey:@"code"] isEqualToNumber:[NSNumber numberWithInt:4]]){
         if ([respond objectForKey:@"data"]!=nil) {
           BOOL isVerified =  [[[respond objectForKey:@"data"] objectForKey:@"verified"] boolValue];
             if (isVerified) {
                 emailCheckTL.hidden = true;
                 [AppUserDefaults setAccessTokan:[respond objectForKey:@"token"]];
                 [self jumpToMenu];
                 
                 BOOL isEnvSet = [[respond objectForKey:@"env_exist"] boolValue];
                 [AppUserDefaults setisEnvironmentSetup:isEnvSet];
                 int locationRowId = [[respond objectForKey:@"location_row_id"] intValue];
                 [AppUserDefaults setRowId:locationRowId];
             }else{
                 if (payload!= nil) {
                     [AppUserDefaults setRegisterPayload:payload];
                 }
                 emailCheckTL.hidden = false;
             }
         }

    }else{
        [self ShowAlert:@"Sorry!" message:[respond objectForKey:@"message"]];
    }
}
-(BOOL)validateEmail :(NSString *)email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
-(void)jumpToMenu{
    NSString * successmsg = [NSString stringWithFormat:@"You are registered with sensor %@",[AppUserDefaults getConnectedPeripheralName]];
    [self initMessageOnlyhud:successmsg];
        dispatch_async(dispatch_get_main_queue(), ^{
            SWRevealViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        });
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [nameTF resignFirstResponder];
    [emailTF resignFirstResponder];
    [phoneTF resignFirstResponder];
}
- (IBAction)btnCheckBoxClicks:(id)sender {
    if (isCheck) {
        [btnCheck setImage:[UIImage imageNamed:@"RR_check_box_empty"] forState:UIControlStateNormal];
        isCheck = NO;
    }else{
        [btnCheck setImage:[UIImage imageNamed:@"RR_check_box"] forState:UIControlStateNormal];
        isCheck = YES;
    }
}
//Keybord validation
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag == 1) {
        textField.returnKeyType = UIReturnKeyNext;
    }else if (textField.tag == 2){
        textField.returnKeyType = UIReturnKeyDone;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    if (aTextField.tag == 1) {
        [emailTF becomeFirstResponder];
    }else if (aTextField.tag == 2){
        [nameTF resignFirstResponder];
        [emailTF resignFirstResponder];
    }
    return YES;
}
@end
