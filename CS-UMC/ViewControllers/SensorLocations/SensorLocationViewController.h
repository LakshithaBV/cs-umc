//
//  SensorLocationViewController.h
//  CS-UMC
//
//  Created by SCIT on 1/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationCollectionViewCell.h"
#import "WebserviceApi.h"
#import "MasterViewController.h"

@interface SensorLocationViewController : MasterViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,webservicedelegate,UICollectionViewDelegate>{
    
    __weak IBOutlet UICollectionView *locationCollectionView;
    
    UIImagePickerController *  mediaPicker;
    NSMutableArray * locationsArray;
    int selectedRow;
    NSDictionary * selectedEnvironment;
    
}

@end
