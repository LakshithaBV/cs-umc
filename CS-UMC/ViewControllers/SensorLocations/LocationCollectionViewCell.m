//
//  LocationCollectionViewCell.m
//  CS-UMC
//
//  Created by SCIT on 1/16/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "LocationCollectionViewCell.h"

@implementation LocationCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)loadFromData :(NSDictionary *)dataDic{
    locationNameTL.text = [dataDic objectForKey:@"env_name"];
    NSString * imageURL = [dataDic objectForKey:@"env_image"];
    [locationImage loadWithURLString:imageURL complete:^(UIImage *image) {
        
    }];
}
@end
