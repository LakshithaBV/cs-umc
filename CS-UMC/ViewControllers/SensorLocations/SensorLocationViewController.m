//
//  SensorLocationViewController.m
//  CS-UMC
//
//  Created by SCIT on 1/15/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "SensorLocationViewController.h"
#import "SWRevealViewController.h"

@interface SensorLocationViewController ()

@end

@implementation SensorLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setHidesBackButton:YES];
    selectedRow = 100;
    [locationCollectionView registerNib:[UINib nibWithNibName:@"LocationCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"CELL"];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self requestEnvironmnts];
}
-(void)requestEnvironmnts{
    WebserviceApi * webapi = [WebserviceApi sharedInstance];
    webapi.delegate = self;
    if ([self ReachabilityPass]) {
        [webapi GetEnvironment];
    }else{
        [self ShowAlert:@"Oops!" message:NSLocalizedString(NOINTERNET, @"")];
    }
    
}
-(void)DidReceiveEnvironmentinfo:(id)respond{
    NSLog(@"EnV data %@",respond);
    if ([respond objectForKey:@"data"]) {
        NSArray * locations = [respond objectForKey:@"data"];
        
        if (locations.count > 0) {
            if (!locationsArray)locationsArray = [[NSMutableArray alloc]init];
            [locationsArray removeAllObjects];
            [locationsArray addObjectsFromArray:locations];
            [locationCollectionView reloadData];
        }else{
            [self ShowAlert:@"Oops!" message:@"No locations found."];
        }
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnStartClicks:(id)sender {
    [self jumpTOMenu];
    
//    if ([AppUserDefaults getSelectedLocation]!=NULL) {
//        [self cameraAction];
//    }else{
//        [self ShowAlert:@"Oops!" message:@"Please select a location."];
//    }
}
#pragma mark -
#pragma mark collectonview data

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return locationsArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(160.f, 160.f);
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LocationCollectionViewCell *cell = [locationCollectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    NSDictionary * environmentdata = [locationsArray objectAtIndex:indexPath.row];

    if (selectedRow == indexPath.row) {
        cell.backgroungImage.backgroundColor = [UIColor redColor];
    }else{
       cell.backgroungImage.backgroundColor = [UIColor clearColor];
    }

    [cell loadFromData:environmentdata];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    selectedRow = (int)indexPath.row;
    selectedEnvironment = [locationsArray objectAtIndex:indexPath.row];
//    [AppUserDefaults setSelectedLocation:selectedEnvironment];
    [locationCollectionView reloadData];
}
#pragma mark -
#pragma mark Camera/PhotoLibrary
-(void)cameraAction{
    mediaPicker = [[UIImagePickerController alloc] init];
    [mediaPicker setDelegate:self];
    mediaPicker.allowsEditing = YES;

    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:@"one"
                                                              style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                  NSLog(@"You pressed button one");
                                                                  mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                                  [self presentViewController:mediaPicker animated:NO completion:nil];
                                                              }]; // 2
        UIAlertAction *secondAction = [UIAlertAction actionWithTitle:@"two"
                                                               style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                                   NSLog(@"You pressed button two");
                                                                   mediaPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                                   [self presentViewController:mediaPicker animated:NO completion:nil];
                                                               }]; // 3
        
        [actionSheet addAction:firstAction]; // 4
        [actionSheet addAction:secondAction]; // 5
        [self presentViewController:actionSheet animated:YES completion:nil];
        
        /*
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            
            // Cancel button tappped.
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            
            // Distructive button tapped.
            [self dismissViewControllerAnimated:YES completion:^{
                mediaPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self presentViewController:mediaPicker animated:NO completion:nil];
                }];
            }];
        }]];
        
        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose Existing" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            // OK button tapped.
            
            [self dismissViewControllerAnimated:YES completion:^{
                mediaPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    [self presentViewController:mediaPicker animated:NO completion:nil];
                }];
            }];
        }]];
        // Present action sheet.
        [self presentViewController:actionSheet animated:YES completion:nil];
       */
    }else{
     
        
    }
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self sendLocationData:chosenImage];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)sendLocationData :(UIImage *)selectedImage{
    NSMutableDictionary * locationdata = [[NSMutableDictionary alloc]initWithDictionary:[AppUserDefaults getSelectedLocationCordinate]];
    
    NSString * senserId = [AppUserDefaults getConnectedPeripheralID];
//    NSDictionary * selectedLocation = [AppUserDefaults getSelectedLocation];
//    
//    [locationdata setObject:[selectedLocation objectForKey:@"id"] forKey:@"env_type"];
//    [locationdata setObject:senserId forKey:@"sensor_id"];
//    NSData *imageData = UIImagePNGRepresentation(selectedImage);
//    [locationdata setObject:imageData forKey:@"image"];
//    
//    WebserviceApi * webapi = [WebserviceApi sharedInstance];
//    webapi.delegate = self;
//    [webapi setEnvironment:locationdata :selectedImage];
}
-(void)DidReceiveSetEnvironmentinfo:(id)respond{
    
    if ([[respond objectForKey:@"code"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
        [AppUserDefaults setisEnvironmentSetup:YES];
        [self jumpTOMenu];
    }else{
        [self ShowAlert:@"Sorry!" message:[respond objectForKey:@"message"]];
    }
    
    NSLog(@"setdata %@",respond);
    
}
-(void)jumpTOMenu{
        dispatch_async(dispatch_get_main_queue(), ^{
            SWRevealViewController * controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
            [self.navigationController pushViewController:controller animated:YES];
        });
}
@end
