//
//  LocationCollectionViewCell.h
//  CS-UMC
//
//  Created by SCIT on 1/16/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWNetworking.h"

@interface LocationCollectionViewCell : UICollectionViewCell{
    
    __weak IBOutlet UIImageView *locationImage;
    __weak IBOutlet UILabel *locationNameTL;
}

-(void)loadFromData :(NSDictionary *)dataDic;
@property (weak, nonatomic) IBOutlet UIView *backgroungImage;

@end
