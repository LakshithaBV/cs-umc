//
//  AppDelegate.m
//  CS-UMC
//
//  Created by SCIT on 1/3/18.
//  Copyright © 2018 SCIT. All rights reserved.
//

#import "AppDelegate.h"
#import "SensorDataUpdateService.h"
#import "BTDeviceManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:@"AIzaSyA6-mw-i3UYMcKvfjpQ8O6Ux7hipkPwdZs"];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [AppUserDefaults removeCurrentDevice];
    [AppUserDefaults setisLogout:false];
    [[BTDeviceManager sharedInstance] disconnect];
    exit(0);
}
- (void)applicationWillEnterForeground:(UIApplication *)application {
    [self saveFromTime];
    [[NSNotificationCenter defaultCenter]postNotificationName:REGISTRATION_CHECK_NOTITFICATIONS object:self];
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [[NSNotificationCenter defaultCenter]postNotificationName:APPACTIVE_NOTITFICATIONS object:self];
}
-(void)applicationWillTerminate:(UIApplication *)application{
    [self saveFromTime];
}
-(void)saveFromTime{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString * dateString = [dateFormatter stringFromDate:[NSDate date]];
    [AppUserDefaults setLastDisconnectedTime:dateString];
    
    NSLog(@"Logged date %@",dateString);
}
#pragma mark - Core Data stack
@synthesize persistentContainer = _persistentContainer;
- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"CSUMC"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    return _persistentContainer;
}

#pragma mark - Core Data Saving support
- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}


@end
